Impressum
=========

Informationspflicht laut §5 E-Commerce Gesetz, §14 Unternehmensgesetzbuch, §63 Gewerbeordnung und Offenlegungspflicht laut §25 Mediengesetz.

Oliver Holzer

Gabrielerstrasse 43,  
2340 Mödling,  
Österreich

**Tel.:** 06606856910  
**E-Mail:** [office@exclamation-mark.com](mailto:office@exclamation-mark.com)

Quelle: Erstellt mit dem [Impressum Generator](https://www.firmenwebseiten.at/impressum-generator/ "Impressum Generator von firmenwebseiten.at") von firmenwebseiten.at

Urheberrechtshinweis
--------------------

Alle Inhalte dieser Webseite (Bilder, Fotos, Texte, Videos) unterliegen dem Urheberrecht. Falls notwendig, werden wir die unerlaubte Nutzung von Teilen der Inhalte unserer Seite rechtlich verfolgen.

EU-Streitschlichtung
--------------------

Gemäß Verordnung über Online-Streitbeilegung in Verbraucherangelegenheiten (ODR-Verordnung) möchten wir Sie über die Online-Streitbeilegungsplattform (OS-Plattform) informieren.  
Verbraucher haben die Möglichkeit, Beschwerden an die Online Streitbeilegungsplattform der Europäischen Kommission unter [http://ec.europa.eu/odr?tid=221148768](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&lng=DE) zu richten. Die dafür notwendigen Kontaktdaten finden Sie oberhalb in unserem Impressum.

Wir möchten Sie jedoch darauf hinweisen, dass wir nicht bereit oder verpflichtet sind, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.

Bildernachweis
--------------

Die Bilder, Fotos und Grafiken auf dieser Webseite sind urheberrechtlich geschützt.

Die Bilderrechte liegen bei den folgenden Fotografen und Unternehmen:

*   Fotograf Mustermann

Haftung für Inhalte dieser Website
----------------------------------

Wir entwickeln die Inhalte dieser Webseite ständig weiter und bemühen uns korrekte und aktuelle Informationen bereitzustellen.  Leider können wir keine Haftung für die Korrektheit aller Inhalte auf dieser Website übernehmen, speziell für jene, die seitens Dritter bereitgestellt wurden. Als Diensteanbieter sind wir nicht verpflichtet, die von ihnen übermittelten oder gespeicherten Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.

Unsere Verpflichtungen zur Entfernung von Informationen oder zur Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen aufgrund von gerichtlichen oder behördlichen Anordnungen bleiben auch im Falle unserer Nichtverantwortlichkeit davon unberührt.

Sollten Ihnen problematische oder rechtswidrige Inhalte auffallen, bitte wir Sie uns umgehend zu kontaktieren, damit wir die rechtswidrigen Inhalte entfernen können. Sie finden die Kontaktdaten im Impressum.

Haftung für Links auf dieser Webseite
-------------------------------------

Unsere Webseite enthält Links zu anderen Webseiten für deren Inhalt wir nicht verantwortlich sind. Haftung für verlinkte Websites besteht für uns nicht, da wir keine Kenntnis rechtswidriger Tätigkeiten hatten und haben, uns solche Rechtswidrigkeiten auch bisher nicht aufgefallen sind und wir Links sofort entfernen würden, wenn uns Rechtswidrigkeiten bekannt werden.

Wenn Ihnen rechtswidrige Links auf unserer Website auffallen, bitte wir Sie uns zu kontaktieren. Sie finden die Kontaktdaten im Impressum.

Datenschutzerklärung
====================

Einleitung und Überblick
------------------------

Wir haben diese Datenschutzerklärung (Fassung 07.10.2021-221148768) verfasst, um Ihnen gemäß der Vorgaben der [Datenschutz-Grundverordnung (EU) 2016/679](https://eur-lex.europa.eu/legal-content/DE/ALL/?uri=celex%3A32016R0679&tid=221148768) und anwendbaren nationalen Gesetzen zu erklären, welche personenbezogenen Daten (kurz Daten) wir als Verantwortliche – und die von uns beauftragten Auftragsverarbeiter (z. B. Provider) – verarbeiten, zukünftig verarbeiten werden und welche rechtmäßigen Möglichkeiten Sie haben. Die verwendeten Begriffe sind geschlechtsneutral zu verstehen.  
**Kurz gesagt:** Wir informieren Sie umfassend über Daten, die wir über Sie verarbeiten.

Datenschutzerklärungen klingen für gewöhnlich sehr technisch und verwenden juristische Fachbegriffe. Diese Datenschutzerklärung soll Ihnen hingegen die wichtigsten Dinge so einfach und transparent wie möglich beschreiben. Soweit es der Transparenz förderlich ist, werden technische **Begriffe leserfreundlich erklärt**, **Links** zu weiterführenden Informationen geboten und **Grafiken** zum Einsatz gebracht. Wir informieren damit in klarer und einfacher Sprache, dass wir im Rahmen unserer Geschäftstätigkeiten nur dann personenbezogene Daten verarbeiten, wenn eine entsprechende gesetzliche Grundlage gegeben ist. Das ist sicher nicht möglich, wenn man möglichst knappe, unklare und juristisch-technische Erklärungen abgibt, so wie sie im Internet oft Standard sind, wenn es um Datenschutz geht. Ich hoffe, Sie finden die folgenden Erläuterungen interessant und informativ und vielleicht ist die eine oder andere Information dabei, die Sie noch nicht kannten.  
Wenn trotzdem Fragen bleiben, möchten wir Sie bitten, sich an die unten bzw. im Impressum genannte verantwortliche Stelle zu wenden, den vorhandenen Links zu folgen und sich weitere Informationen auf Drittseiten anzusehen. Unsere Kontaktdaten finden Sie selbstverständlich auch im Impressum.

Rechtsgrundlagen
----------------

In der folgenden Datenschutzerklärung geben wir Ihnen transparente Informationen zu den rechtlichen Grundsätzen und Vorschriften, also den Rechtsgrundlagen der Datenschutz-Grundverordnung, die uns ermöglichen, personenbezogene Daten zu verarbeiten.  
Was das EU-Recht betrifft, beziehen wir uns auf die VERORDNUNG (EU) 2016/679 DES EUROPÄISCHEN PARLAMENTS UND DES RATES vom 27. April 2016. Diese Datenschutz-Grundverordnung der EU können Sie selbstverständlich online auf EUR-Lex, dem Zugang zum EU-Recht, unter [https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=celex%3A32016R0679](https://eur-lex.europa.eu/legal-content/DE/ALL/?uri=celex%3A32016R0679&tid=221148768) nachlesen.

Wir verarbeiten Ihre Daten nur, wenn mindestens eine der folgenden Bedingungen zutrifft:

1.  **Einwilligung** (Artikel 6 Absatz 1 lit. a DSGVO): Sie haben uns Ihre Einwilligung gegeben, Daten zu einem bestimmten Zweck zu verarbeiten. Ein Beispiel wäre die Speicherung Ihrer eingegebenen Daten eines Kontaktformulars.
2.  **Vertrag** (Artikel 6 Absatz 1 lit. b DSGVO): Um einen Vertrag oder vorvertragliche Verpflichtungen mit Ihnen zu erfüllen, verarbeiten wir Ihre Daten. Wenn wir zum Beispiel einen Kaufvertrag mit Ihnen abschließen, benötigen wir vorab personenbezogene Informationen.
3.  **Rechtliche Verpflichtung** (Artikel 6 Absatz 1 lit. c DSGVO): Wenn wir einer rechtlichen Verpflichtung unterliegen, verarbeiten wir Ihre Daten. Zum Beispiel sind wir gesetzlich verpflichtet Rechnungen für die Buchhaltung aufzuheben. Diese enthalten in der Regel personenbezogene Daten.
4.  **Berechtigte Interessen** (Artikel 6 Absatz 1 lit. f DSGVO): Im Falle berechtigter Interessen, die Ihre Grundrechte nicht einschränken, behalten wir uns die Verarbeitung personenbezogener Daten vor. Wir müssen zum Beispiel gewisse Daten verarbeiten, um unsere Website sicher und wirtschaftlich effizient betreiben zu können. Diese Verarbeitung ist somit ein berechtigtes Interesse.

Weitere Bedingungen wie die Wahrnehmung von Aufnahmen im öffentlichen Interesse und Ausübung öffentlicher Gewalt sowie dem Schutz lebenswichtiger Interessen treten bei uns in der Regel nicht auf. Soweit eine solche Rechtsgrundlage doch einschlägig sein sollte, wird diese an der entsprechenden Stelle ausgewiesen.

Zusätzlich zu der EU-Verordnung gelten auch noch nationale Gesetze:

*   In **Österreich** ist dies das Bundesgesetz zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten (**Datenschutzgesetz**), kurz **DSG**.
*   In **Deutschland** gilt das **Bundesdatenschutzgesetz**, kurz **BDSG**.

Sofern weitere regionale oder nationale Gesetze zur Anwendung kommen, informieren wir Sie in den folgenden Abschnitten darüber.

TLS-Verschlüsselung mit https
-----------------------------

TLS, Verschlüsselung und https klingen sehr technisch und sind es auch. Wir verwenden HTTPS (das Hypertext Transfer Protocol Secure steht für „sicheres Hypertext-Übertragungsprotokoll“), um Daten abhörsicher im Internet zu übertragen.  
Das bedeutet, dass die komplette Übertragung aller Daten von Ihrem Browser zu unserem Webserver abgesichert ist – niemand kann “mithören”.

Damit haben wir eine zusätzliche Sicherheitsschicht eingeführt und erfüllen Datenschutz durch Technikgestaltung [Artikel 25 Absatz 1 DSGVO](https://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:32016R0679&from=DE&tid=221148768)). Durch den Einsatz von TLS (Transport Layer Security), einem Verschlüsselungsprotokoll zur sicheren Datenübertragung im Internet, können wir den Schutz vertraulicher Daten sicherstellen.  
Sie erkennen die Benutzung dieser Absicherung der Datenübertragung am kleinen Schlosssymbol ![](https://www.adsimple.at/wp-content/uploads/2018/03/schlosssymbol-https.svg) links oben im Browser, links von der Internetadresse (z. B. beispielseite.de) und der Verwendung des Schemas https (anstatt http) als Teil unserer Internetadresse.  
Wenn Sie mehr zum Thema Verschlüsselung wissen möchten, empfehlen wir die Google Suche nach “Hypertext Transfer Protocol Secure wiki” um gute Links zu weiterführenden Informationen zu erhalten.

Kontaktdaten des Verantwortlichen
---------------------------------

Sollten Sie Fragen zum Datenschutz haben, finden Sie nachfolgend die Kontaktdaten der verantwortlichen Person bzw. Stelle:  
Musterfirma GmbH  
Musterstraße 1, 4711 Musterort  
Vertretungsberechtigt: Markus Mustermann  
E-Mail: [office@musterfirma.com](mailto:office@musterfirma.com)  
Telefon: +49 47114 654165  
Impressum: [https://www.testfirma.de/impressum](http://www.testfirma.de/impressum)

Rechte laut Datenschutzgrundverordnung
--------------------------------------

Laut Artikel 13 DSGVO stehen Ihnen die folgenden Rechte zu, damit es zu einer fairen und transparenten Verarbeitung von Daten kommt:

*   Sie haben laut Artikel 15 DSGVO ein Auskunftsrecht darüber, ob wir Daten von Ihnen verarbeiten. Sollte das zutreffen, haben Sie Recht darauf eine Kopie der Daten zu erhalten und die folgenden Informationen zu erfahren:
    *   zu welchem Zweck wir die Verarbeitung durchführen;
    *   die Kategorien, also die Arten von Daten, die verarbeitet werden;
    *   wer diese Daten erhält und wenn die Daten an Drittländer übermittelt werden, wie die Sicherheit garantiert werden kann;
    *   wie lange die Daten gespeichert werden;
    *   das Bestehen des Rechts auf Berichtigung, Löschung oder Einschränkung der Verarbeitung und dem Widerspruchsrecht gegen die Verarbeitung;
    *   dass Sie sich bei einer Aufsichtsbehörde beschweren können (Links zu diesen Behörden finden Sie weiter unten);
    *   die Herkunft der Daten, wenn wir sie nicht bei Ihnen erhoben haben;
    *   ob Profiling durchgeführt wird, ob also Daten automatisch ausgewertet werden, um zu einem persönlichen Profil von Ihnen zu gelangen.
*   Sie haben laut Artikel 16 DSGVO ein Recht auf Berichtigung der Daten, was bedeutet, dass wir Daten richtig stellen müssen, falls Sie Fehler finden.
*   Sie haben laut Artikel 17 DSGVO das Recht auf Löschung („Recht auf Vergessenwerden“), was konkret bedeutet, dass Sie die Löschung Ihrer Daten verlangen dürfen.
*   Sie haben laut Artikel 18 DSGVO das Recht auf Einschränkung der Verarbeitung, was bedeutet, dass wir die Daten nur mehr speichern dürfen aber nicht weiter verwenden.
*   Sie haben laut Artikel 19 DSGVO das Recht auf Datenübertragbarkeit, was bedeutet, dass wir Ihnen auf Anfrage Ihre Daten in einem gängigen Format zur Verfügung stellen.
*   Sie haben laut Artikel 21 DSGVO ein Widerspruchsrecht, welches nach Durchsetzung eine Änderung der Verarbeitung mit sich bringt.
    *   Wenn die Verarbeitung Ihrer Daten auf Artikel 6 Abs. 1 lit. e (öffentliches Interesse, Ausübung öffentlicher Gewalt) oder Artikel 6 Abs. 1 lit. f (berechtigtes Interesse) basiert, können Sie gegen die Verarbeitung Widerspruch einlegen. Wir prüfen danach so rasch wie möglich, ob wir diesem Widerspruch rechtlich nachkommen können.
    *   Werden Daten verwendet, um Direktwerbung zu betreiben, können Sie jederzeit gegen diese Art der Datenverarbeitung widersprechen. Wir dürfen Ihre Daten danach nicht mehr für Direktmarketing verwenden.
    *   Werden Daten verwendet, um Profiling zu betreiben, können Sie jederzeit gegen diese Art der Datenverarbeitung widersprechen. Wir dürfen Ihre Daten danach nicht mehr für Profiling verwenden.
*   Sie haben laut Artikel 22 DSGVO unter Umständen das Recht, nicht einer ausschließlich auf einer automatisierten Verarbeitung (zum Beispiel Profiling) beruhenden Entscheidung unterworfen zu werden.

Wenn Sie glauben, dass die Verarbeitung Ihrer Daten gegen das Datenschutzrecht verstößt oder Ihre datenschutzrechtlichen Ansprüche in sonst einer Weise verletzt worden sind, können Sie sich bei der Aufsichtsbehörde beschweren. Diese ist für Österreich die Datenschutzbehörde, deren Website Sie unter [https://www.dsb.gv.at/](https://www.dsb.gv.at/?tid=221148768) finden und für Deutschland können Sie sich an die [Bundesbeauftragte für den Datenschutz und die Informationsfreiheit (BfDI)](https://www.bfdi.bund.de) wenden.

**Kurz gesagt:** Sie haben Rechte – zögern Sie nicht, die oben gelistete verantwortliche Stelle bei uns zu kontaktieren!

Google Analytics IP-Anonymisierung
----------------------------------

Wir haben auf dieser Webseite die IP-Adressen-Anonymisierung von Google Analytics implementiert. Diese Funktion wurde von Google entwickelt, damit diese Webseite die geltenden Datenschutzbestimmungen und Empfehlungen der lokalen Datenschutzbehörden einhalten kann, wenn diese eine Speicherung der vollständigen IP-Adresse untersagen. Die Anonymisierung bzw. Maskierung der IP findet statt, sobald die IP-Adressen im Google Analytics-Datenerfassungsnetzwerk eintreffen und bevor eine Speicherung oder Verarbeitung der Daten stattfindet.

Mehr Informationen zur IP-Anonymisierung finden Sie auf [https://support.google.com/analytics/answer/2763052?hl=de](https://support.google.com/analytics/answer/2763052?hl=de).

Google Analytics Deaktivierungslink
-----------------------------------

Wenn Sie auf folgenden **Deaktivierungslink** klicken, können Sie verhindern, dass Google weitere Besuche auf dieser Webseite erfasst. Achtung: Das Löschen von Cookies, die Nutzung des Inkognito/Privatmodus ihres Browsers, oder die Nutzung eines anderen Browsers führt dazu, dass wieder Daten erhoben werden.

[Google Analytics deaktivieren](javascript:gaOptout())

Kommunikation
-------------

**Kommunikation Zusammenfassung**  
👥 Betroffene: Alle, die mit uns per Telefon, E-Mail oder Online-Formular kommunizieren  
📓 Verarbeitete Daten: z. B. Telefonnummer, Name, E-Mail-Adresse, eingegebene Formulardaten. Mehr Details dazu finden Sie bei der jeweils eingesetzten Kontaktart  
🤝 Zweck: Abwicklung der Kommunikation mit Kunden, Geschäftspartnern usw.  
📅 Speicherdauer: Dauer des Geschäftsfalls und der gesetzlichen Vorschriften  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit. b DSGVO (Vertrag), Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)

Wenn Sie mit uns Kontakt aufnehmen und per Telefon, E-Mail oder Online-Formular kommunizieren, kann es zur Verarbeitung personenbezogener Daten kommen.

Die Daten werden für die Abwicklung und Bearbeitung Ihrer Frage und des damit zusammenhängenden Geschäftsvorgangs verarbeitet. Die Daten während ebensolange gespeichert bzw. so lange es das Gesetz vorschreibt.

### Betroffene Personen

Von den genannten Vorgängen sind alle betroffen, die über die von uns bereit gestellten Kommunikationswege den Kontakt zu uns suchen.

### Telefon

Wenn Sie uns anrufen, werden die Anrufdaten auf dem jeweiligen Endgerät und beim eingesetzten Telekommunikationsanbieter pseudonymisiert gespeichert. Außerdem können Daten wie Name und Telefonnummer im Anschluss per E-Mail versendet und zur Anfragebeantwortung gespeichert werden. Die Daten werden gelöscht, sobald der Geschäftsfall beendet wurde und es gesetzliche Vorgaben erlauben.

### E-Mail

Wenn Sie mit uns per E-Mail kommunizieren, werden Daten gegebenenfalls auf dem jeweiligen Endgerät (Computer, Laptop, Smartphone,…) gespeichert und es kommt zur Speicherung von Daten auf dem E-Mail-Server. Die Daten werden gelöscht, sobald der Geschäftsfall beendet wurde und es gesetzliche Vorgaben erlauben.

### Online Formulare

Wenn Sie mit uns mittels Online-Formular kommunizieren, werden Daten auf unserem Webserver gespeichert und gegebenenfalls an eine E-Mail-Adresse von uns weitergeleitet. Die Daten werden gelöscht, sobald der Geschäftsfall beendet wurde und es gesetzliche Vorgaben erlauben.

### Rechtsgrundlagen

Die Verarbeitung der Daten basiert auf den folgenden Rechtsgrundlagen:

*   Art. 6 Abs. 1 lit. a DSGVO (Einwilligung): Sie geben uns die Einwilligung Ihre Daten zu speichern und weiter für den Geschäftsfall betreffende Zwecke zu verwenden;
*   Art. 6 Abs. 1 lit. b DSGVO (Vertrag): Es besteht die Notwendigkeit für die Erfüllung eines Vertrags mit Ihnen oder einem Auftragsverarbeiter wie z. B. dem Telefonanbieter oder wir müssen die Daten für vorvertragliche Tätigkeiten, wie z. B. die Vorbereitung eines Angebots, verarbeiten;
*   Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen): Wir wollen Kundenanfragen und geschäftliche Kommunikation in einem professionellen Rahmen betreiben. Dazu sind gewisse technische Einrichtungen wie z. B. E-Mail-Programme, Exchange-Server und Mobilfunkbetreiber notwendig, um die Kommunikation effizient betreiben zu können.

Google Analytics Berichte zu demografischen Merkmalen und Interessen
--------------------------------------------------------------------

Wir haben in Google Analytics die Funktionen für Werbeberichte eingeschaltet. Die Berichte zu demografischen Merkmalen und Interessen enthalten Angaben zu Alter, Geschlecht und Interessen. Damit können wir uns – ohne diese Daten einzelnen Personen zuordnen zu können – ein besseres Bild von unseren Nutzern machen. Mehr über die Werbefunktionen erfahren Sie [auf https://support.google.com/analytics/answer/3450482?hl=de\_AT&utm\_id=ad](https://support.google.com/analytics/answer/3450482?hl=de_AT&utm_id=ad).

Sie können die Nutzung der Aktivitäten und Informationen Ihres Google Kontos unter “Einstellungen für Werbung” auf [https://adssettings.google.com/authenticated](https://adssettings.google.com/authenticated) per Checkbox beenden.

Google Analytics Zusatz zur Datenverarbeitung
---------------------------------------------

Wir haben mit Google einen Direktkundenvertrag zur Verwendung von Google Analytics abgeschlossen, indem wir den “Zusatz zur Datenverarbeitung” in Google Analytics akzeptiert haben.

Mehr über den Zusatz zur Datenverarbeitung für Google Analytics finden Sie hier: [https://support.google.com/analytics/answer/3379636?hl=de&utm\_id=ad](https://support.google.com/analytics/answer/3379636?hl=de&utm_id=ad)

Datenübertragung in Drittländer
-------------------------------

Wir übertragen oder verarbeiten Daten nur dann in Länder außerhalb der EU (Drittländer), wenn Sie dieser Verarbeitung zustimmen, dies gesetzlich vorgeschrieben ist oder vertraglich notwendig und in jedem Fall nur soweit dies generell erlaubt ist. Ihre Zustimmung ist in den meisten Fällen der wichtigste Grund, dass wir Daten in Drittländern verarbeiten lassen. Die Verarbeitung personenbezogener Daten in Drittländern wie den USA, wo viele Softwarehersteller Dienstleistungen anbieten und Ihre Serverstandorte haben, kann bedeuten, dass personenbezogene Daten auf unerwartete Weise verarbeitet und gespeichert werden. Nach Möglichkeit versuchen wir Serverstandorte innerhalb der EU zu nutzen, sofern das angeboten wird.

Wir informieren Sie an den passenden Stellen dieser Datenschutzerklärung genauer über Datenübertragung in Drittländer, sofern diese zutrifft.

Sicherheit der Datenverarbeitung
--------------------------------

Um personenbezogene Daten zu schützen, haben wir sowohl technische als auch organisatorische Maßnahmen umgesetzt. Wo es uns möglich ist, verschlüsseln oder pseudonymisieren wir personenbezogene Daten. Dadurch machen wir es im Rahmen unserer Möglichkeiten so schwer wie möglich, dass Dritte aus unseren Daten auf persönliche Informationen schließen können.

Art. 25 DSGVO spricht hier von “Datenschutz durch Technikgestaltung und durch datenschutzfreundliche Voreinstellungen” und meint damit, dass man sowohl bei Software (z. B. Formularen) also auch Hardware (z. B. Zugang zum Serverraum) immer an Sicherheit denkt und entsprechende Maßnahmen setzt. Im Folgenden gehen wir, falls erforderlich, noch auf konkrete Maßnahmen ein.

Google Optimize Datenschutzerklärung
------------------------------------

Wir verwenden auf unserer Website Google Optimize, ein Website-Optimierungstool. Dienstanbieter ist das amerikanische Unternehmen Google Inc. Für den europäischen Raum ist das Unternehmen Google Ireland Limited (Gordon House, Barrow Street Dublin 4, Irland) für alle Google-Dienste verantwortlich. Mehr über die Daten, die durch die Verwendung von Google Optimize verarbeitet werden, erfahren Sie in der Privacy Policy auf [https://policies.google.com/privacy?hl=en-US](https://policies.google.com/privacy?hl=en-US) .

Facebook-Pixel Datenschutzerklärung
-----------------------------------

Wir verwenden auf unserer Webseite das Facebook-Pixel von Facebook. Dafür haben wir einen Code auf unserer Webseite implementiert. Der Facebook-Pixel ist ein Ausschnitt aus JavaScript-Code, der eine Ansammlung von Funktionen lädt, mit denen Facebook Ihre Userhandlungen verfolgen kann, sofern Sie über Facebook-Ads auf unsere Webseite gekommen sind. Wenn Sie beispielsweise ein Produkt auf unserer Webseite erwerben, wird das Facebook-Pixel ausgelöst und speichert Ihre Handlungen auf unserer Webseite in einem oder mehreren Cookies. Diese Cookies ermöglichen es Facebook Ihre Userdaten (Kundendaten wie IP-Adresse, User-ID) mit den Daten Ihres Facebook-Kontos abzugleichen. Dann löscht Facebook diese Daten wieder. Die erhobenen Daten sind für uns anonym und nicht einsehbar und werden nur im Rahmen von Werbeanzeigenschaltungen nutzbar. Wenn Sie selbst Facebook-User sind und angemeldet sind, wird der Besuch unserer Webseite automatisch Ihrem Facebook-Benutzerkonto zugeordnet.

Wir wollen unsere Dienstleistungen bzw. Produkte nur jenen Menschen zeigen, die sich auch wirklich dafür interessieren. Mithilfe von Facebook-Pixel können unsere Werbemaßnahmen besser auf Ihre Wünsche und Interessen abgestimmt werden. So bekommen Facebook-User (sofern sie personalisierte Werbung erlaubt haben) passende Werbung zu sehen. Weiters verwendet Facebook die erhobenen Daten zu Analysezwecken und eigenen Werbeanzeigen.

Im Folgenden zeigen wir Ihnen jene Cookies, die durch das Einbinden von Facebook-Pixel auf einer Testseite gesetzt wurden. Bitte beachten Sie, dass dies nur Beispiel-Cookies sind. Je nach Interaktion auf unserer Webseite werden unterschiedliche Cookies gesetzt.

**Name:** \_fbp  
**Wert:** fb.1.1568287647279.257405483-6221148768-7  
**Verwendungszweck:** Dieses Cookie verwendet Facebook, um Werbeprodukte anzuzeigen.  
**Ablaufdatum:** nach 3 Monaten

**Name:** fr  
**Wert:** 0aPf312HOS5Pboo2r..Bdeiuf…1.0.Bdeiuf.  
**Verwendungszweck:** Dieses Cookie wird verwendet, damit Facebook-Pixel auch ordentlich funktioniert.  
**Ablaufdatum:** nach 3 Monaten

**Name:** comment\_author\_50ae8267e2bdf1253ec1a5769f48e062221148768-3  
**Wert:** Name des Autors  
**Verwendungszweck:** Dieses Cookie speichert den Text und den Namen eines Users, der beispielsweise einen Kommentar hinterlässt.  
**Ablaufdatum:** nach 12 Monaten

**Name:** comment\_author\_url\_50ae8267e2bdf1253ec1a5769f48e062  
**Wert:** https%3A%2F%2Fwww.testseite…%2F (URL des Autors)  
**Verwendungszweck:** Dieses Cookie speichert die URL der Website, die der User in einem Textfeld auf unserer Webseite eingibt.  
**Ablaufdatum:** nach 12 Monaten

**Name:** comment\_author\_email\_50ae8267e2bdf1253ec1a5769f48e062  
**Wert:** E-Mail-Adresse des Autors  
**Verwendungszweck:** Dieses Cookie speichert die E-Mail-Adresse des Users, sofern er sie auf der Website bekannt gegeben hat.  
**Ablaufdatum:** nach 12 Monaten

**Anmerkung:** Die oben genannten Cookies beziehen sich auf ein individuelles Userverhalten. Speziell bei der Verwendung von Cookies sind Veränderungen bei Facebook nie auszuschließen.

Sofern Sie bei Facebook angemeldet sind, können Sie Ihre Einstellungen für Werbeanzeigen unter [https://www.facebook.com/ads/preferences/?entry\_product=ad\_settings\_screen](https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen) selbst verändern. Falls Sie kein Facebook-User sind, können Sie auf [http://www.youronlinechoices.com/de/praferenzmanagement/](http://www.youronlinechoices.com/de/praferenzmanagement/?tid=221148768) grundsätzlich Ihre nutzungsbasierte Online-Werbung verwalten. Dort haben Sie die Möglichkeit, Anbieter zu deaktivieren bzw. zu aktivieren.

Wenn Sie mehr über den Datenschutz von Facebook erfahren wollen, empfehlen wir Ihnen die eigenen Datenrichtlinien des Unternehmens auf [https://www.facebook.com/policy.php](https://www.facebook.com/policy.php).

Newsletter Datenschutzerklärung
-------------------------------

Wenn Sie sich für unseren Newsletter eintragen übermitteln Sie die oben genannten persönlichen Daten und geben uns das Recht Sie per E-Mail zu kontaktieren. Die im Rahmen der Anmeldung zum Newsletter gespeicherten Daten nutzen wir ausschließlich für unseren Newsletter und geben diese nicht weiter.

Sollten Sie sich vom Newsletter abmelden – Sie finden den Link dafür in jedem Newsletter ganz unten – dann löschen wir alle Daten die mit der Anmeldung zum Newsletter gespeichert wurden.

Google AdSense Datenschutzerklärung
-----------------------------------

Wir verwenden auf dieser Webseite Google AdSense. Das ist ein Anzeigenprogramm der Firma Google Inc. (1600 Amphitheatre Parkway Mountain View, CA 94043, USA). Mit Google AdSense können wir auf dieser Webseite Werbeanzeigen einblenden, die zu unserem Thema passen. So bieten wir Ihnen Anzeigen, die im Idealfall einen richtigen Mehrwert für Sie darstellen. Im Zuge dieser Datenschutzerklärung über Google AdSense erklären wir Ihnen, warum wir Google AdSense auf unserer Webseite verwenden, welche Daten von Ihnen verarbeitet und gespeichert werden und wie Sie diese Datenspeicherung unterbinden können.

### Was ist Google AdSense?

Das Werbeprogramm Google AdSense gibt es mittlerweile seit 2003. Im Gegensatz zu Google Ads (früher: Google AdWords) kann man hier nicht selbst Werbung schalten. Über Google AdSense werden Werbeanzeigen auf Webseiten, wie zum Beispiel auf unserer, ausgespielt. Der größte Vorteil dieses Werbedienstes im Vergleich zu manch anderen ist, dass Ihnen Google AdSense nur Anzeigen zeigt, die zu unseren Inhalten passen. Google hat einen eigenen Algorithmus, der berechnet, welche Werbeanzeigen Sie zu Gesicht bekommen. Natürlich wollen wir Ihnen nur Werbung bieten, die Sie auch interessiert und Ihnen einen Mehrwert bietet. Google überprüft anhand Ihrer Interessen bzw. Ihres Userverhaltens und anhand unseres Angebots, welche Werbeanzeigen für unsere Webseite und für unserer User geeignet sind. An dieser Stelle wollen wir auch gleich erwähnen, dass wir für die Auswahl der Werbeanzeigen nicht verantwortlich sind. Wir bieten mit unserer Webseite lediglich die Werbefläche an. Die Auswahl der angezeigten Werbung trifft Google. Seit August 2013 werden die Anzeigen auch an die jeweilige Benutzeroberfläche angepasst. Das heißt, egal ob Sie von Ihrem Smartphone, Ihrem PC oder Laptop unsere Webseite besuchen, die Anzeigen passen sich an Ihr Endgerät an.

### Warum verwenden wir Google AdSense auf unserer Webseite?

Das Betreiben einer hochwertigen Webseite erfordert viel Hingabe und großen Einsatz. Im Grunde sind wir mit der Arbeit an unserer Webseite nie fertig. Wir versuchen stets unsere Seite zu pflegen und so aktuell wie möglich zu halten. Natürlich wollen wir mit dieser Arbeit auch einen wirtschaftlichen Erfolg erzielen. Darum haben wir uns für Werbeanzeigen als Einnahmequelle entschieden. Das Wichtigste für uns ist allerdings, Ihren Besuch auf unserer Webseite durch diese Anzeigen nicht zu stören. Mithilfe von Google AdSense wird Ihnen nur Werbung angeboten, die zu unseren Themen und Ihren Interessen passt.

Ähnlich wie bei der Google-Indexierung für eine Webseite, untersucht ein Bot den entsprechenden Content und die entsprechenden Angebote unserer Webseite. Dann werden die Werbeanzeigen inhaltlich angepasst und auf der Webseite präsentiert. Neben den inhaltlichen Überschneidungen zwischen Anzeige und Webseiten-Angebot unterstützt AdSense auch interessensbezogenes Targeting. Das bedeutet, dass Google auch Ihre Daten dazu verwendet, um auf Sie zugeschnittene Werbung anzubieten. So erhalten Sie Werbung, die Ihnen im Idealfall einen echten Mehrwert bietet und wir haben eine höhere Chance ein bisschen etwas zu verdienen.

### Welche Daten werden von Google AdSense gespeichert?

Damit Google AdSense eine maßgeschneiderte, auf Sie angepasste Werbung anzeigen kann, werden unter anderem Cookies verwendet. Cookies sind kleine Textdateien, die bestimmte Informationen auf Ihrem Computer speichern.

In AdSense sollen Cookies bessere Werbung ermöglichen. Die Cookies enthalten keine personenidentifizierbaren Daten. Hierbei ist allerdings zu beachten, dass Google Daten wie zum Beispiel “Pseudonyme Cookie-IDs” (Name oder anderes Identifikationsmerkmal wird durch ein Pseudonym ersetzt) oder IP-Adressen als nicht personenidentifizierbare Informationen ansieht. Im Rahmen der DSGVO können diese Daten allerdings als personenbezogene Daten gelten. Google AdSense sendet nach jeder Impression (das ist immer dann der Fall, wenn Sie eine Anzeige sehen), jedem Klick und jeder anderen Aktivität, die zu einem Aufruf der Google AdSense-Server führt, ein Cookie an den Browser. Sofern der Browser das Cookie akzeptiert, wird es dort gespeichert.

Drittanbieter können im Rahmen von AdSense unter Umständen Cookies in Ihrem Browser platzieren und auslesen bzw. Web-Beacons verwenden, um Daten zu speichern, die sie durch die Anzeigenbereitstellung auf der Webseite erhalten. Als Web-Beacons bezeichnet man kleine Grafiken, die eine Logdatei-Analyse und eine Aufzeichnung der Logdatei machen. Diese Analyse ermöglicht eine statistische Auswertung für das Online-Marketing.

Google kann über diese Cookies bestimmte Informationen über Ihr Userverhalten auf unserer Webseite sammeln. Dazu zählen:

*   Informationen wie Sie mit einer Anzeige umgehen (Klicks, Impression, Mausbewegungen)
*   Informationen, ob in Ihrem Browser schon eine Anzeige zu einem früheren Zeitpunkt erschienen ist. Diese Daten helfen dabei, Ihnen eine Anzeige nicht öfter anzuzeigen.

Dabei analysiert Google die Daten zu den angezeigten Werbemitteln und Ihre IP-Adresse und wertet diese aus. Google verwendet die Daten in erster Linie, um die Effektivität einer Anzeige zu messen und das Werbeangebot zu verbessern. Diese Daten werden nicht mit personenbezogenen Daten, die Google möglicherweise über andere Google-Dienste von Ihnen hat, verknüpft.

Im Folgenden stellen wir Ihnen Cookies vor, die Google AdSense für Trackingzwecke verwendet. Hierbei beziehen wir uns auf eine Test-Webseite, die ausschließlich Google AdSense installiert hat: 

**Name:** uid  
**Wert:** 891269189221148768-0  
**Verwendungszweck:** Das Cookie wird unter der Domain adform.net gespeichert. Es stellt eine eindeutig zugewiesene, maschinell generierte User-ID bereit und sammelt Daten über die Aktivität auf unserer Webseite.  
**Ablaufdatum:** nach 2 Monaten

**Name:** C  
**Wert:** 1  
**Verwendungszweck:** Dieses Cookie identifiziert, ob Ihrer Browser Cookies akzeptiert. Das Cookie wird unter der Domain track.adform.net gespeichert.  
**Ablaufdatum:** nach 1 Monat

**Name:** cid  
**Wert:** 8912691894970695056,0,0,0,0  
**Verwendungszweck:** Dieses Cookie wird unter der Domain track.adform.net gespeichert, steht für Client-ID und wird verwendet, um die Werbung für Sie zu verbessern. Es kann relevantere Werbung an den Besucher weiterleiten und hilft, die Berichte über die Kampagnenleistung zu verbessern.  
**Ablaufdatum:** nach 2 Monaten

**Name:** IDE  
**Wert:** zOtj4TWxwbFDjaATZ2TzNaQmxrU221148768-3  
**Verwendungszweck:** Das Cookie wird unter der Domain doubkeklick.net gespeichert. Es dient dazu, Ihre Aktionen nach der Anzeige bzw. nach dem Klicken der Anzeige zu registrieren. Dadurch kann man messen, wie gut eine Anzeige bei unseren Besuchern ankommt.  
**Ablaufdatum:** nach 1 Monat

**Name:** test\_cookie  
**Wert:** keine Angabe  
**Verwendungszweck:** Mithilfe des „test\_cookies“ kann man überprüfen, ob Ihr Browser überhaupt Cookies unterstützt. Das Cookie wird unter der Domain doubkeklick.net gespeichert.  
**Ablaufdatum:** nach 1 Monat

**Name:** CT592996  
**Wert:**733366  
**Verwendungszweck:** Wird unter der Domain adform.net gespeichert. Das Cookie wird gesetzt sobald Sie auf eine Werbeanzeige klicken. Genauere Informationen über die Verwendung dieses Cookies konnten wir nicht in Erfahrung bringen.  
**Ablaufdatum:** nach einer Stunde

**Anmerkung:** Diese Aufzählung kann keinen Anspruch auf Vollständigkeit erheben, da Google erfahrungsgemäß die Wahl ihrer Cookies immer wieder auch verändert.

### Wie lange und wo werden die Daten gespeichert?

Google erfasst Ihre IP-Adresse und verschiedene Aktivitäten, die Sie auf der Webseite ausführen. Cookies speichern diese Informationen zu den Interaktionen auf unsere Webseite. Laut Google sammelt und speichert das Unternehmen die angegebenen Informationen auf sichere Weise auf den hauseigenen Google-Servern in den USA.

Wenn Sie kein Google-Konto haben bzw. nicht angemeldet sind, speichert Google die erhobenen Daten mit einer eindeutigen Kennung (ID) meist auf Ihrem Browser. Die in Cookies gespeicherten eindeutigen IDs dienen beispielsweise dazu, personalisierte Werbung zu gewährleisten. Wenn Sie in einem Google-Konto angemeldet sind, kann Google auch personenbezogene Daten erheben.

Einige der Daten, die Google speichert, können Sie jederzeit wieder löschen (siehe nächsten Abschnitt). Viele Informationen, die in Cookies gespeichert sind, werden automatisch nach einer bestimmten Zeit wieder gelöscht. Es gibt allerdings auch Daten, die von Google über einen längeren Zeitraum gespeichert werden. Dies ist dann der Fall, wenn Google aus wirtschaftlichen oder rechtlichen Notwendigkeiten, gewisse Daten über einen unbestimmten, längeren Zeitraum speichern muss.

### Wie kann ich meine Daten löschen bzw. die Datenspeicherung verhindern?

Sie haben immer die Möglichkeit Cookies, die sich auf Ihrem Computer befinden, zu löschen oder zu deaktivieren. Wie genau das funktioniert hängt von Ihrem Browser ab.

Hier finden Sie die Anleitung, wie Sie Cookies in Ihrem Browser verwalten:

[Chrome: Cookies in Chrome löschen, aktivieren und verwalten](https://support.google.com/chrome/answer/95647?tid=221148768)

[Safari: Verwalten von Cookies und Websitedaten mit Safari](https://support.apple.com/de-at/guide/safari/sfri11471/mac?tid=221148768)

[Firefox: Cookies löschen, um Daten zu entfernen, die Websites auf Ihrem Computer abgelegt haben](https://support.mozilla.org/de/kb/cookies-und-website-daten-in-firefox-loschen?tid=221148768)

[Internet Explorer: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/17442/windows-internet-explorer-delete-manage-cookies?tid=221148768)

[Microsoft Edge: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/4027947/windows-delete-cookies?tid=221148768)

Falls Sie grundsätzlich keine Cookies haben wollen, können Sie Ihren Browser so einrichten, dass er Sie immer informiert, wenn ein Cookie gesetzt werden soll. So können Sie bei jedem einzelnen Cookie entscheiden, ob Sie das Cookie erlauben oder nicht. Durch das Herunterladen und Installieren dieses Browser-Plug-ins auf [https://support.google.com/ads/answer/7395996](https://support.google.com/ads/answer/7395996) werden ebenfalls alle „Werbecookies“ deaktiviert. Bedenken Sie, dass Sie durch das Deaktivieren dieser Cookies nicht die Werbeanzeigen verhindern, sondern nur die personalisierte Werbung.

Wenn Sie ein Google Konto besitzen, können Sie auf der Webseite [https://adssettings.google.com/authenticated](https://adssettings.google.com/authenticated) personalisierte Werbung deaktivieren. Auch hier sehen Sie dann weiter Anzeigen, allerdings sind diese nicht mehr an Ihre Interessen angepasst. Dennoch werden die Anzeigen auf der Grundlage von ein paar Faktoren, wie Ihrem Standort, dem Browsertyp und der verwendeten Suchbegriffe, angezeigt.

Welche Daten Google grundsätzlich erfasst und wofür sie diese Daten verwenden, können Sie auf [https://www.google.com/intl/de/policies/privacy/](https://www.google.com/intl/de/policies/privacy/) nachlesen.

Eingebettete Social Media Elemente Datenschutzerklärung
-------------------------------------------------------

Wir binden auf unserer Webseite Elemente von Social Media Diensten ein um Bilder, Videos und Texte anzuzeigen.  
Durch den Besuch von Seiten die diese Elemente darstellen, werden Daten von Ihrem Browser zum jeweiligen Social Media Dienst übertragen und dort gespeichert. Wir haben keinen Zugriff auf diese Daten.  
Die folgenden Links führen Sie zu den Seiten der jeweiligen Social Media Dienste wo erklärt wird, wie diese mit Ihren Daten umgehen:

*   Instagram-Datenschutzrichtlinie: [https://help.instagram.com/519522125107875](https://help.instagram.com/519522125107875?tid=221148768)
*   Für YouTube gilt die Google Datenschutzerklärung: [https://policies.google.com/privacy?hl=de](https://policies.google.com/privacy?hl=de&tid=221148768)
*   Facebook-Datenrichtline: [https://www.facebook.com/about/privacy](https://www.facebook.com/about/privacy?tid=221148768)
*   Twitter Datenschutzrichtlinie: [https://twitter.com/de/privacy](https://twitter.com/de/privacy?tid=221148768)

Google Site Kit Datenschutzerklärung
------------------------------------

**Google Site Kit Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Besucher der Website  
🤝 Zweck: Auswertung der Besucherinformationen zur Optimierung des Webangebots.  
📓 Verarbeitete Daten: Zugriffsstatistiken, die Daten wie Standorte der Zugriffe, Gerätedaten, Zugriffsdauer und Zeitpunkt, Navigationsverhalten, Klickverhalten und IP-Adressen enthalten. Mehr Details dazu finden weiter unten und in der Datenschutzerklärung von Google Analytics.  
📅 Speicherdauer: abhängig von den verwendeten Properties  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)

### Was ist Google Site Kit?

Wir haben in unsere Website das WordPress-Plugin Google Site Kit des amerikanischen Unternehmens Google Inc. eingebunden. Für den europäischen Raum ist das Unternehmen Google Ireland Limited (Gordon House, Barrow Street Dublin 4, Irland) für alle Google-Dienste verantwortlich. Mit Google Site Kit können wir schnell und einfach Statistiken, die aus diversen Google-Produkten wie Google Analytics stammen, direkt in unserem WordPress-Dashboard ansehen. Das Tool beziehungsweise die in Google Site Kit eingebundenen Tools sammeln unter anderem auch personenbezogene Daten von Ihnen. In dieser Datenschutzerklärung erklären wir Ihnen, warum wir Google Site Kit verwenden, wie lange und wo Daten gespeichert werden und welche weiteren Datenschutztexte in diesem Zusammenhang für Sie relevant sind.

Google Site Kit ist ein Plugin für das Content-Management-System WordPress. Mit diesem Plugin können wir wichtige Statistiken zur Websiteanalyse direkt in unserem Dashboard ansehen. Dabei handelt es sich um Statistiken, die von anderen Google-Produkten erhoben werden. Allen voran von Google Analytics. Neben Google Analytics können auch noch die Services Google Search Console, Page Speed Insight, Google AdSense, Google Optimize und Google Tag Manager mit Google Site Kit verknüpft werden.

### Warum verwenden wir Google Site Kit auf unserer Website?

Als Dienstleister ist es unsere Aufgabe, Ihnen auf unserer Website das bestmögliche Erlebnis zu bieten. Sie sollen sich auf unserer Website wohl fühlen und schnell und einfach genau das finden, was Sie suchen. Statistische Auswertungen helfen uns dabei, sie besser kennen zu lernen und unser Angebot an Ihre Wünsche und Interessen anzupassen. Für diese Auswertungen nutzen wir verschiedene Google-Tools. Site Kit erleichtert diesbezüglich unsere Arbeit sehr, weil wir die Statistiken der Google-Produkte gleich im Dashboard ansehen und analysieren können. Wir müssen uns also nicht mehr für das jeweilige Tool extra anmelden. Site Kit bietet somit immer einen guten Überblick über die wichtigsten Analyse-Daten.

### Welche Daten werden von Google Site Kit gespeichert?

Wenn Sie im Cookie-Hinweis (auch Script oder Banner genannt) Trackingtools aktiv zugestimmt haben, werden durch Google-Produkte wie Google Analytics Cookies gesetzt und Daten von Ihnen, etwa über Ihr Userverhalten, an Google gesendet, dort gespeichert und verarbeitet. Darunter werden auch personenbezogen Daten wie Ihre IP-Adresse gespeichert.

Für genauere Informationen zu den einzelnen Diensten haben wir eigenen Textabschnitte in dieser Datenschutzerklärung. Sehen Sie sich beispielsweise unsere Datenschutzerklärung zu Google Analytics an. Hier gehen wir sehr genau auf die erhobenen Daten ein. Sie erfahren wie lange Google Analytics Daten speichert, verwaltet und verarbeitet, welche Cookies zum Einsatz kommen können und wie Sie die Datenspeicherung verhindern. Ebenso haben wir auch für weitere Google-Dienste wie etwa den Google Tag Manager oder Google AdSense eigene Datenschutzerklärungen mit umfassenden Informationen.

Im Folgenden zeigen wir Ihnen beispielhafte Google-Analytics-Cookies, die in Ihrem Browser gesetzt werden können, sofern Sie der Datenverarbeitung durch Google grundsätzlich zugestimmt haben. Bitte beachten Sie, dass es sich bei diesen Cookies lediglich um eine Auswahl handelt:

**Name:** \_ga  
**Wert:**2.1326744211.152221148768-2  
**Verwendungszweck:** Standardmäßig verwendet analytics.js das Cookie \_ga, um die User-ID zu speichern. Grundsätzlich dient es zur Unterscheidung der Websitenbesucher.  
**Ablaufdatum:** nach 2 Jahren

**Name:** \_gid  
**Wert:**2.1687193234.152221148768-7  
**Verwendungszweck:** Auch dieses Cookie dient der Unterscheidung von Websitesbesuchern.  
**Ablaufdatum:** nach 24 Stunden

**Name:** \_gat\_gtag\_UA\_  
**Wert:** 1  
**Verwendungszweck:** Dieses Cookie wird zum Senken der Anforderungsrate verwendet.  
**Ablaufdatum:** nach 1 Minute

### Wie lange und wo werden die Daten gespeichert?

Google speichert erhobene Daten auf eigenen Google-Servern, die weltweit verteilt sind. Die meisten Server befinden sich in den Vereinigten Staaten und daher ist es leicht möglich, dass Ihre Daten auch dort gespeichert werden. Auf [https://www.google.com/about/datacenters/inside/locations/?hl=de](https://www.google.com/about/datacenters/inside/locations/?hl=de) sehen Sie genau, wo das Unternehmen Server bereitstellt.

Daten, die durch Google Analytics erhoben werden, werden standardisiert 26 Monate aufbewahrt. Im Anschluss werden Ihre Userdaten gelöscht. Die Aufbewahrungsdauer gilt für alle Daten, die mit Cookies, Usererkennung und Werbe-IDs verknüpft sind.

### Wie kann ich meine Daten löschen bzw. die Datenspeicherung verhindern?

Sie haben immer das Recht, Auskunft über Ihre Daten zu erhalten, Ihre Daten löschen, berichtigen oder einschränken zu lassen. Zudem können Sie auch in Ihrem Browser Cookies jederzeit deaktivieren, löschen oder verwalten. Hier zeigen wir Ihnen die entsprechenden Anleitungen der gängigsten Browser:

[Chrome: Cookies in Chrome löschen, aktivieren und verwalten](https://support.google.com/chrome/answer/95647?tid=221148768)

[Safari: Verwalten von Cookies und Websitedaten mit Safari](https://support.apple.com/de-at/guide/safari/sfri11471/mac?tid=221148768)

[Firefox: Cookies löschen, um Daten zu entfernen, die Websites auf Ihrem Computer abgelegt haben](https://support.mozilla.org/de/kb/cookies-und-website-daten-in-firefox-loschen?tid=221148768)

[Internet Explorer: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/17442/windows-internet-explorer-delete-manage-cookies?tid=221148768)

[Microsoft Edge: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/4027947/windows-delete-cookies?tid=221148768)

Bitte beachten Sie, dass bei der Verwendung dieses Tools Daten von Ihnen auch außerhalb der EU gespeichert und verarbeitet werden können. Die meisten Drittstaaten (darunter auch die USA) gelten nach derzeitigem europäischen Datenschutzrecht als nicht sicher. Daten an unsichere Drittstaaten dürfen also nicht einfach übertragen, dort gespeichert und verarbeitet werden, sofern es keine passenden Garantien (wie etwa EU-Standardvertragsklauseln) zwischen uns und dem außereuropäischen Dienstleister gibt.

### Rechtsgrundlage

Der Einsatz von Google Site Kit setzt Ihre Einwilligung voraus, welche wir mit unserem Cookie Popup eingeholt haben. Diese Einwilligung stellt laut **Art. 6 Abs. 1 lit. a DSGVO (Einwilligung)** die Rechtsgrundlage für die Verarbeitung personenbezogener Daten, wie sie bei der Erfassung durch Web-Analytics Tools vorkommen kann, dar.

Zusätzlich zur Einwilligung besteht von unserer Seite ein berechtigtes Interesse daran, dass Verhalten der Websitebesucher zu analysieren und so unser Angebot technisch und wirtschaftlich zu verbessern. Mit Hilfe von Google Site Kit erkennen wir Fehler der Website, können Attacken identifizieren und die Wirtschaftlichkeit verbessern. Die Rechtsgrundlage dafür ist **Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)**.

Um mehr über die Datenverarbeitung durch Google zu erfahren, empfehlen wir Ihnen die umfassenden Datenschutzrichtlinien von Google unter [https://policies.google.com/privacy?hl=de](https://policies.google.com/privacy?hl=de?tid=221148768).

Google Tag Manager Datenschutzerklärung
---------------------------------------

**Google Tag Manager Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Besucher der Website  
🤝 Zweck: Organisation der einzelnen Tracking-Tools  
📓 Verarbeitete Daten: Der Google Tag Manager speichert selbst keine Daten. Die Daten erfassen die Tags der eingesetzten Web-Analytics-Tools.  
📅 Speicherdauer: abhängig vom eingesetzten Web-Analytics-Tool  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)

### Was ist der Google Tag Manager?

Für unsere Website verwenden wir den Google Tag Manager des Unternehmens Google Inc. Für den europäischen Raum ist das Unternehmen Google Ireland Limited (Gordon House, Barrow Street Dublin 4, Irland) für alle Google-Dienste verantwortlich. Dieser Tag Manager ist eines von vielen hilfreichen Marketing-Produkten von Google. Über den Google Tag Manager können wir Code-Abschnitte von diversen Trackingtools, die wir auf unserer Webseite verwenden, zentral einbauen und verwalten.

In dieser Datenschutzerklärung wollen wir Ihnen genauer erklären was der Google Tag Manager macht, warum wir ihn verwenden und in welcher Form Daten verarbeitet werden.

Der Google Tag Manager ist ein Organisationstool, mit dem wir Webseiten-Tags zentral und über eine Benutzeroberfläche einbinden und verwalten können. Als Tags bezeichnet man kleine Code-Abschnitte, die beispielsweise Ihre Aktivitäten auf unserer Webseite aufzeichnen (tracken). Dafür werden JavaScript-Code-Abschnitte in den Quelltext unserer Seite eingesetzt. Die Tags stammen oft von google-internen Produkten wie Google Ads oder Google Analytics, aber auch Tags von anderen Unternehmen können über den Manager eingebunden und verwaltet werden. Solche Tags übernehmen unterschiedliche Aufgaben. Sie können Browserdaten sammeln, Marketingtools mit Daten füttern, Buttons einbinden, Cookies setzen und auch Nutzer über mehrere Webseiten hinweg verfolgen.

### Warum verwenden wir den Google Tag Manager für unserer Website?

Wie sagt man so schön: Organisation ist die halbe Miete! Und das betrifft natürlich auch die Pflege unserer Webseite. Um unsere Webseite für Sie und alle Menschen, die sich für unsere Produkte und Dienstleistungen interessieren, so gut wie möglich zu gestalten, brauchen wir diverse Trackingtools wie beispielsweise Google Analytics. Die erhobenen Daten dieser Tools zeigen uns, was Sie am meisten interessiert, wo wir unsere Leistungen verbessern können und welchen Menschen wir unsere Angebote noch zeigen sollten. Und damit dieses Tracking funktioniert, müssen wir entsprechende JavaScript-Codes in unsere Webseite einbinden. Grundsätzlich könnten wir jeden Code-Abschnitt der einzelnen Tracking-Tools separat in unseren Quelltext einbauen. Das erfordert allerdings relativ viel Zeit und man verliert leicht den Überblick. Darum nützen wir den Google Tag Manager. Wir können die nötigen Skripte einfach einbauen und von einem Ort aus verwalten. Zudem bietet der Google Tag Manager eine leicht zu bedienende Benutzeroberfläche und man benötigt keine Programmierkenntnisse. So schaffen wir es, Ordnung in unserem Tag-Dschungel zu halten.

### Welche Daten werden vom Google Tag Manager gespeichert?

Der Tag Manager selbst ist eine Domain, die keine Cookies setzt und keine Daten speichert. Er fungiert als bloßer „Verwalter“ der implementierten Tags. Die Daten erfassen die einzelnen Tags der unterschiedlichen Web-Analysetools. Die Daten werden im Google Tag Manager quasi zu den einzelnen Tracking-Tools durchgeschleust und nicht gespeichert.

Ganz anders sieht das allerdings mit den eingebundenen Tags der verschiedenen Web-Analysetools, wie zum Beispiel Google Analytics, aus. Je nach Analysetool werden meist mit Hilfe von Cookies verschiedene Daten über Ihr Webverhalten gesammelt, gespeichert und verarbeitet. Dafür lesen Sie bitte unsere Datenschutztexte zu den einzelnen Analyse- und Trackingtools, die wir auf unserer Webseite verwenden.

In den Kontoeinstellungen des Tag Managers haben wir Google erlaubt, dass Google anonymisierte Daten von uns erhält. Dabei handelt es sich aber nur um die Verwendung und Nutzung unseres Tag Managers und nicht um Ihre Daten, die über die Code-Abschnitte gespeichert werden. Wir ermöglichen Google und anderen, ausgewählte Daten in anonymisierter Form zu erhalten. Wir stimmen somit der anonymen Weitergabe unseren Website-Daten zu. Welche zusammengefassten und anonymen Daten genau weitergeleitet werden, konnten wir – trotz langer Recherche – nicht in Erfahrung bringen. Auf jeden Fall löscht Google dabei alle Infos, die unsere Webseite identifizieren könnten. Google fasst die Daten mit Hunderten anderen anonymen Webseiten-Daten zusammen und erstellt, im Rahmen von Benchmarking-Maßnahmen, Usertrends. Bei Benchmarking werden eigene Ergebnisse mit jenen der Mitbewerber verglichen. Auf Basis der erhobenen Informationen können Prozesse optimiert werden.

### Wie lange und wo werden die Daten gespeichert?

Wenn Google Daten speichert, dann werden diese Daten auf den eigenen Google-Servern gespeichert. Die Server sind auf der ganzen Welt verteilt. Die meisten befinden sich in Amerika. Unter [https://www.google.com/about/datacenters/inside/locations/?hl=de](https://www.google.com/about/datacenters/inside/locations/?hl=de) können Sie genau nachlesen, wo sich die Google-Server befinden.

Wie lange die einzelnen Tracking-Tools Daten von Ihnen speichern, entnehmen Sie unseren individuellen Datenschutztexten zu den einzelnen Tools.

### Wie kann ich meine Daten löschen bzw. die Datenspeicherung verhindern?

Der Google Tag Manager selbst setzt keine Cookies, sondern verwaltet Tags verschiedener Tracking-Webseiten. In unseren Datenschutztexten zu den einzelnen Tracking-Tools, finden Sie detaillierte Informationen wie Sie Ihre Daten löschen bzw. verwalten können.

Bitte beachten Sie, dass bei der Verwendung dieses Tools Daten von Ihnen auch außerhalb der EU gespeichert und verarbeitet werden können. Die meisten Drittstaaten (darunter auch die USA) gelten nach derzeitigem europäischen Datenschutzrecht als nicht sicher. Daten an unsichere Drittstaaten dürfen also nicht einfach übertragen, dort gespeichert und verarbeitet werden, sofern es keine passenden Garantien (wie etwa EU-Standardvertragsklauseln) zwischen uns und dem außereuropäischen Dienstleister gibt.

### Rechtsgrundlage

Der Einsatz des Google Tag Managers setzt Ihre Einwilligung voraus, welche wir mit unserem Cookie Popup eingeholt haben. Diese Einwilligung stellt laut **Art. 6 Abs. 1 lit. a DSGVO (Einwilligung)** die Rechtsgrundlage für die Verarbeitung personenbezogener Daten, wie sie bei der Erfassung durch Web-Analytics Tools vorkommen kann, dar.

Zusätzlich zur Einwilligung besteht von unserer Seite ein berechtigtes Interesse daran, dass Verhalten der Websitebesucher zu analysieren und so unser Angebot technisch und wirtschaftlich zu verbessern. Mit Hilfe der Google Tag Managers können die Wirtschaftlichkeit verbessern. Die Rechtsgrundlage dafür ist **Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)**.

Wenn Sie mehr über den Google Tag Manager erfahren wollen, empfehlen wir Ihnen die FAQs unter [https://www.google.com/intl/de/tagmanager/faq.html](https://www.google.com/intl/de/tagmanager/faq.html).

IONOS WebAnalytics Datenschutzerklärung
---------------------------------------

**IONOS WebAnalytics Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Besucher der Website  
🤝 Zweck: Auswertung der Besucherinformationen zur Optimierung des Webangebots.  
📓 Verarbeitete Daten: Zugriffsstatistiken, die Daten wie Standorte der Zugriffe, Gerätedaten, Zugriffsdauer und Zeitpunkt und IP-Adressen in anonymisierter Form enthalten.  
📅 Speicherdauer: abhängig von der Vertragsdauer mit IONOS WebAnalytics  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)

### Was ist IONOS WebAnalytics?

Wir nutzen auf unserer Website das Analyse-Tool IONOS WebAnalytics des deutschen Unternehmens 1&1 IONOS SE, Elgendorfer Straße 57, 56410 Montabaur, Deutschland. Das Tool hilft uns bei der Analyse unserer Website und dafür werden auch Daten erhoben und gespeichert. Allerdings verzichtet dieses Tool auf die Erhebung von Daten, die Sie als Person identifizieren könnten. Dennoch wollen wir Sie in dieser Datenschutzerklärung genauer über die Datenverarbeitung und Speicherung informieren und auch erklären, warum wir IONOS WebAnalytics nutzen.

IONOS WebAnalytics ist, wie es der Name auch vermuten lässt, ein Tool, das der Analyse unserer Website dient. Das Softwareprogramm sammelt etwa Daten darüber, wie lange Sie sich auf unserer Website befinden, welche Buttons Sie klicken oder von welcher anderen Website Sie zu uns gefunden haben. So bekommen wir einen guten Überblick über das Userverhalten auf unserer Website. All diese Informationen sind anonym. Das bedeutet, dass wir durch diese Daten nicht Sie als Person identifizieren, sondern lediglich allgemeine Nutzungsinformationen und Statistiken erhalten.

### Warum verwenden wir IONOS WebAnalytics auf unserer Website?

Unser Ziel ist es, Ihnen ein bestmögliches Erlebnis auf unserer Website zu bieten. Wir sind von unseren Angeboten überzeugt und wollen, dass unserer Website für Sie ein hilfreicher und nützlicher Ort ist. Dafür müssen wir unsere Website so gut wie möglich an Ihre Wünsche und Anliegen anpassen. Mit einem Webanalysetool wie IONOS WebAnalytics und den daraus resultierenden Daten können wir unsere Website dahingehend verbessern. Die Daten können uns weiters auch dienlich sein, Werbe- und Marketingmaßnahmen individueller zu gestalten. Bei all diesen Webanalysen liegt uns aber dennoch der Schutz personenbezogener Daten am Herzen. Entgegen anderen Analysetools speichert und verarbeitet IONOS WebAnalytics keine Daten, die Sie als Person erkennen könnten.

### Welche Daten werden von IONOS WebAnalytics gespeichert?

Die Daten werden durch Logfiles oder durch einen sogenannten Pixel erhoben und gespeichert. Ein Pixel ist ein Ausschnitt aus JavaScript-Code, der eine Ansammlung von Funktionen lädt, mit dem man Userverhalten verfolgen kann. WebAnalytics verzichtet bewusst auf die Verwendung von Cookies.

IONOS speichert keine personenbezogenen Daten von Ihnen. Bei der Übermittlung eines Seitenaufrufes wird zwar Ihre IP-Adresse übertragen, allerdings dann sofort anonymisiert und so verarbeitet, dass man Sie als Person nicht identifizieren kann.

Folgenden Daten werden von IONOS WebAnalytics gespeichert:

*   Ihr Browsertyp und Ihre Browserversion
*   welche Website Sie zuvor besucht haben (Referrer)
*   welche spezifische Webseite Sie bei uns aufgerufen haben
*   welches Betriebssystem Sie nutzen
*   welches Endgerät Sie verwenden (PC, Tablet oder Smartphone)
*   wann Sie auf unsere Seite gekommen sind
*   Ihre IP-Adresse in anonymisierter Form

Die Daten werden an keine Drittanbieter weitergegeben und nur für statistische Auswertungen genutzt.

### Wie lange und wo werden die Daten gespeichert?

Die Daten werden solange gespeichert, bis der Vertrag zwischen IONOS WebAnalytics und uns ausläuft. Die Daten werden im Falle eines regulären Webhosting-Tarifs in unserem Log-Verzeichnis gespeichert und daraus grafische Statistiken erzeugt. Diese Logs werden alle 8 Wochen gelöscht. Im Falle eines MyWebsite-Tarifs werden die Daten über einen Pixel ermittelt. Hier werden die Daten nur innerhalb der IONOS WebAnalytics gespeichert und verarbeitet.

### Wie kann ich meine Daten löschen bzw. die Datenspeicherung verhindern?

Grundsätzlich haben Sie jederzeit das Recht auf Auskunft, Berichtigung bzw. Löschung und Einschränkung der Verarbeitung Ihrer personenbezogenen Daten. Sie können zudem auch jederzeit die Einwilligung zur Verarbeitung der Daten widerrufen. Da über IONOS WebAnalytics allerdings keine personenbezogenen Daten gespeichert oder verarbeitet werden und somit eine Zuordnung von Ihnen als Person nicht möglich ist, gibt es auch die Möglichkeit solche Daten zu löschen nicht.

### Rechtsgrundlage

Der Einsatz von IONOS WebAnalytics setzt Ihre Einwilligung voraus, welche wir mit unserem Cookie Popup eingeholt haben. Diese Einwilligung stellt laut **Art. 6 Abs. 1 lit. a DSGVO (Einwilligung)** die Rechtsgrundlage für die Verarbeitung personenbezogener Daten, wie sie bei der Erfassung durch Web-Analytics Tools vorkommen kann, dar.

Zusätzlich zur Einwilligung besteht von unserer Seite ein berechtigtes Interesse daran, dass Verhalten der Websitebesucher zu analysieren und so unser Angebot technisch und wirtschaftlich zu verbessern. Mit Hilfe von IONOS WebAnalytics erkennen wir Fehler der Website, können Attacken identifizieren und die Wirtschaftlichkeit verbessern. Die Rechtsgrundlage dafür ist **Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)**.

Wir hoffen, wir konnten Ihnen die wichtigsten Informationen rund um die wirklich sparsame Datenverarbeitung von IONOS WebAnalytics näherbringen. Wenn Sie mehr über den Tracking-Dienst erfahren wollen, empfehlen wir Ihnen die Datenschutzerklärung des Unternehmens unter [https://www.ionos.de/hilfe/datenschutz/datenverarbeitung-von-webseitenbesuchern-ihres-11-ionos-produktes/webanalytics/?tid=221148768](https://www.ionos.de/hilfe/datenschutz/datenverarbeitung-von-webseitenbesuchern-ihres-11-ionos-produktes/webanalytics/).

Web Analytics
-------------

**Web Analytics Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Besucher der Website  
🤝 Zweck: Auswertung der Besucherinformationen zur Optimierung des Webangebots.  
📓 Verarbeitete Daten: Zugriffsstatistiken, die Daten wie Standorte der Zugriffe, Gerätedaten, Zugriffsdauer und Zeitpunkt, Navigationsverhalten, Klickverhalten und IP-Adressen enthalten. Mehr Details dazu finden Sie beim jeweils eingesetzten Web Analytics Tool.  
📅 Speicherdauer: abhängig vom eingesetzten Web-Analytics-Tool  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)

### Was ist Web Analytics?

Wir verwenden auf unserer Website Software zur Auswertung des Verhaltens der Website-Besucher, kurz Web Analytics oder Web-Analyse genannt. Dabei werden Daten gesammelt, die der jeweilige Analytic-Tool-Anbieter (auch Trackingtool genannt) speichert, verwaltet und verarbeitet. Mit Hilfe der Daten werden Analysen über das Nutzerverhalten auf unserer Website erstellt und uns als Websitebetreiber zur Verfügung gestellt. Zusätzlich bieten die meisten Tools verschiedene Testmöglichkeiten an. So können wir etwa testen, welche Angebote oder Inhalte bei unseren Besuchern am besten ankommen. Dafür zeigen wir Ihnen für einen begrenzten Zeitabschnitt zwei verschiedene Angebote. Nach dem Test (sogenannter A/B-Test) wissen wir, welches Produkt bzw. welcher Inhalt unsere Websitebesucher interessanter finden. Für solche Testverfahren, wie auch für andere Analytics-Verfahren, können auch Userprofile erstellt werden und die Daten in Cookies gespeichert werden.

### Warum betreiben wir Web Analytics?

Mit unserer Website haben wir ein klares Ziel vor Augen: wir wollen für unsere Branche das besten Webangebot auf dem Markt liefern. Um dieses Ziel zu erreichen, wollen wir einerseits das beste und interessanteste Angebot bieten und andererseits darauf achten, dass Sie sich auf unserer Website rundum wohlfühlen. Mit Hilfe von Webanalyse-Tools können wir das Verhalten unserer Websitebesucher genauer unter die Lupe nehmen und dann entsprechend unser Webangebot für Sie und uns verbessern. So können wir beispielsweise erkennen wie alt unsere Besucher durchschnittlich sind, woher sie kommen, wann unsere Website am meisten besucht wird oder welche Inhalte oder Produkte besonders beliebt sind. All diese Informationen helfen uns die Website zu optimieren und somit bestens an Ihre Bedürfnisse, Interessen und Wünsche anzupassen.

### Welche Daten werden verarbeitet?

Welche Daten genau gespeichert werden, hängt natürlich von den verwendeten Analyse-Tools ab. Doch in der Regel wird zum Beispiel gespeichert, welche Inhalte Sie auf unserer Website ansehen, auf welche Buttons oder Links Sie klicken, wann Sie eine Seite aufrufen, welchen Browser sie verwenden, mit welchem Gerät (PC, Tablet, Smartphone usw.) Sie die Website besuchen oder welches Computersystem Sie verwenden. Wenn Sie damit einverstanden waren, dass auch Standortdaten erhoben werden dürfen, können auch diese durch den Webanalyse-Tool-Anbieter verarbeitet werden.

Zudem wird auch Ihre IP-Adresse gespeichert. Gemäß der Datenschutz-Grundverordnung (DSGVO) sind IP-Adressen personenbezogene Daten. Ihre IP-Adresse wird allerdings in der Regel pseudonymisiert (also in unkenntlicher und gekürzter Form) gespeichert. Für den Zweck der Tests, der Webanalyse und der Weboptimierung werden grundsätzlich keine direkten Daten, wie etwa Ihr Name, Ihr Alter, Ihre Adresse oder Ihre E-Mail-Adresse gespeichert. All diese Daten werden, sofern sie erhoben werden, pseudonymisiert gespeichert. So können Sie als Person nicht identifiziert werden.

Das folgende Beispiel zeigt schematisch die Funktionsweise von Google Analytics als Beispiel für client-basiertes Webtracking mit Java-Script-Code.

![Schematischer Datenfluss bei Google Analytics](https://www.adsimple.at/wp-content/uploads/2021/04/google-analytics-dataflow.svg)

Wie lange die jeweiligen Daten gespeichert werden, hängt immer vom Anbieter ab. Manche Cookies speichern Daten nur für ein paar Minuten bzw. bis Sie die Website wieder verlassen, andere Cookies können Daten über mehrere Jahre speichern.

### Dauer der Datenverarbeitung

Über die Dauer der Datenverarbeitung informieren wir Sie weiter unten, sofern wir weitere Informationen dazu haben. Generell verarbeiten wir personenbezogene Daten nur so lange wie es für die Bereitstellung unserer Dienstleistungen und Produkte unbedingt notwendig ist. Wenn es, wie zum Beispiel im Fall von Buchhaltung, gesetzlich vorgeschrieben ist, kann diese Speicherdauer auch überschritten werden.

### Widerspruchsrecht

Sie haben auch jederzeit das Recht und die Möglichkeit Ihre Einwilligung zur Verwendung von Cookies bzw. Drittanbietern zu widerrufen. Das funktioniert entweder über unser Cookie-Management-Tool oder über andere Opt-Out-Funktionen. Zum Beispiel können Sie auch die Datenerfassung durch Cookies verhindern, indem Sie in Ihrem Browser die Cookies verwalten, deaktivieren oder löschen.

### Rechtsgrundlage

Der Einsatz von Web-Analytics setzt Ihre Einwilligung voraus, welche wir mit unserem Cookie Popup eingeholt haben. Diese Einwilligung stellt laut **Art. 6 Abs. 1 lit. a DSGVO (Einwilligung)** die Rechtsgrundlage für die Verarbeitung personenbezogener Daten, wie sie bei der Erfassung durch Web-Analytics Tools vorkommen kann, dar.

Zusätzlich zur Einwilligung besteht von unserer Seite ein berechtigtes Interesse daran, dass Verhalten der Websitebesucher zu analysieren und so unser Angebot technisch und wirtschaftlich zu verbessern. Mit Hilfe von Web-Analytics erkennen wir Fehler der Website, können Attacken identifizieren und die Wirtschaftlichkeit verbessern. Die Rechtsgrundlage dafür ist **Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)**. Wir setzen die Tools gleichwohl nur ein, soweit sie eine Einwilligung erteilt haben.

Da bei Web-Analytics-Tools Cookies zum Einsatz kommen, empfehlen wir Ihnen auch das Lesen unserer allgemeinen Datenschutzerklärung zu Cookies. Um zu erfahren, welche Daten von Ihnen genau gespeichert und verarbeitet werden, sollten Sie die Datenschutzerklärungen der jeweiligen Tools durchlesen.

Informationen zu speziellen Web-Analytics-Tools, erhalten Sie – sofern vorhanden – in den folgenden Abschnitten.

Facebook Automatischer erweiterter Abgleich Datenschutzerklärung
----------------------------------------------------------------

Wir haben im Rahmen der Facebook-Pixel-Funktion auch den automatischen erweiterten Abgleich (engl. Automatic Advanced Matching) aktiviert. Diese Funktion des Pixels ermöglicht uns, gehashte E-Mails, Namen, Geschlecht, Stadt, Bundesland, Postleitzahl und Geburtsdatum oder Telefonnummer als zusätzliche Informationen an Facebook zu senden, sofern Sie uns diese Daten zur Verfügung gestellt haben. Diese Aktivierung ermöglicht uns Werbekampagnen auf Facebook noch genauer auf Menschen, die sich für unsere Dienstleistungen oder Produkte interessieren, anzupassen.

Google Analytics Google-Signale Datenschutzerklärung
----------------------------------------------------

Wir haben in Google Analytics die Google-Signale aktiviert. So werden die bestehenden Google-Analytics-Funktionen (Werbeberichte, Remarketing, gerätübergreifende Berichte und Berichte zu Interessen und demografische Merkmale) aktualisiert, um zusammengefasste und anonymisierte Daten von Ihnen zu erhalten, sofern Sie personalisierte Anzeigen in Ihrem Google-Konto erlaubt haben.

Das besondere daran ist, dass es sich dabei um ein Cross-Device-Tracking handelt. Das heißt Ihre Daten können geräteübergreifend analysiert werden. Durch die Aktivierung von Google-Signale werden Daten erfasst und mit dem Google-Konto verknüpft. Google kann dadurch zum Beispiel erkennen, wenn Sie auf unsere Webseite über ein Smartphone ein Produkt ansehen und erst später über einen Laptop das Produkt kaufen. Dank der Aktivierung von Google-Signale können wir gerätübergreifende Remarketing-Kampagnen starten, die sonst in dieser Form nicht möglich wären. Remarketing bedeutet, dass wir Ihnen auch auf anderen Webseiten unser Angebot zeigen können.

In Google Analytics werden zudem durch die Google-Signale weitere Besucherdaten wie Standort, Suchverlauf, YouTube-Verlauf und Daten über Ihre Handlungen auf unserer Webseite, erfasst. Wir erhalten dadurch von Google bessere Werbeberichte und nützlichere Angaben zu Ihren Interessen und demografischen Merkmalen. Dazu gehören Ihr Alter, welche Sprache sie sprechen, wo Sie wohnen oder welchem Geschlecht Sie angehören. Weiters kommen auch noch soziale Kriterien wie Ihr Beruf, Ihr Familienstand oder Ihr Einkommen hinzu. All diese Merkmal helfen Google Analytics Personengruppen bzw. Zielgruppen zu definieren.

Die Berichte helfen uns auch Ihr Verhalten, Ihre Wünsche und Interessen besser einschätzen zu können. Dadurch können wir unsere Dienstleistungen und Produkte für Sie optimieren und anpassen. Diese Daten laufen standardmäßig nach 26 Monaten ab. Bitte beachten Sie, dass diese Datenerfassung nur erfolgt, wenn Sie personalisierte Werbung in Ihrem Google-Konto zugelassen haben. Es handelt sich dabei immer um zusammengefasste und anonyme Daten und nie um Daten einzelner Personen. In Ihrem Google-Konto können Sie diese Daten verwalten bzw. auch löschen.

Google Analytics Datenschutzerklärung
-------------------------------------

**Google Analytics Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Besucher der Website  
🤝 Zweck: Auswertung der Besucherinformationen zur Optimierung des Webangebots.  
📓 Verarbeitete Daten: Zugriffsstatistiken, die Daten wie Standorte der Zugriffe, Gerätedaten, Zugriffsdauer und Zeitpunkt, Navigationsverhalten, Klickverhalten und IP-Adressen enthalten. Mehr Details dazu finden Sie weiter unten in dieser Datenschutzerklärung.  
📅 Speicherdauer: abhängig von den verwendeten Properties  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)

### Was ist Google Analytics?

Wir verwenden auf unserer Website das Analyse-Tracking Tool Google Analytics (GA) des amerikanischen Unternehmens Google Inc. Für den europäischen Raum ist das Unternehmen Google Ireland Limited (Gordon House, Barrow Street Dublin 4, Irland) für alle Google-Dienste verantwortlich. Google Analytics sammelt Daten über Ihre Handlungen auf unserer Website. Wenn Sie beispielsweise einen Link anklicken, wird diese Aktion in einem Cookie gespeichert und an Google Analytics versandt. Mithilfe der Berichte, die wir von Google Analytics erhalten, können wir unsere Website und unser Service besser an Ihre Wünsche anpassen. Im Folgenden gehen wir näher auf das Tracking-Tool ein und informieren Sie vor allem darüber, welche Daten gespeichert werden und wie Sie das verhindern können.

Google Analytics ist ein Trackingtool, das der Datenverkehrsanalyse unserer Website dient. Damit Google Analytics funktioniert, wird ein Tracking-Code in den Code unserer Website eingebaut. Wenn Sie unsere Website besuchen, zeichnet dieser Code verschiedene Handlungen auf, die Sie auf unserer Website ausführen. Sobald Sie unsere Website verlassen, werden diese Daten an die Google-Analytics-Server gesendet und dort gespeichert.

Google verarbeitet die Daten und wir bekommen Berichte über Ihr Userverhalten. Dabei kann es sich unter anderem um folgende Berichte handeln:

*   Zielgruppenberichte: Über Zielgruppenberichte lernen wir unsere User besser kennen und wissen genauer, wer sich für unser Service interessiert.
*   Anzeigeberichte: Durch Anzeigeberichte können wir unsere Onlinewerbung leichter analysieren und verbessern.
*   Akquisitionsberichte: Akquisitionsberichte geben uns hilfreiche Informationen darüber, wie wir mehr Menschen für unser Service begeistern können.
*   Verhaltensberichte: Hier erfahren wir, wie Sie mit unserer Website interagieren. Wir können nachvollziehen welchen Weg Sie auf unserer Seite zurücklegen und welche Links Sie anklicken.
*   Conversionsberichte: Conversion nennt man einen Vorgang, bei dem Sie aufgrund einer Marketing-Botschaft eine gewünschte Handlung ausführen. Zum Beispiel, wenn Sie von einem reinen Websitebesucher zu einem Käufer oder Newsletter-Abonnent werden. Mithilfe dieser Berichte erfahren wir mehr darüber, wie unsere Marketing-Maßnahmen bei Ihnen ankommen. So wollen wir unsere Conversionrate steigern.
*   Echtzeitberichte: Hier erfahren wir immer sofort, was gerade auf unserer Website passiert. Zum Beispiel sehen wir wie viele User gerade diesen Text lesen.

### Warum verwenden wir Google Analytics auf unserer Website?

Unser Ziel mit dieser Website ist klar: Wir wollen Ihnen das bestmögliche Service bieten. Die Statistiken und Daten von Google Analytics helfen uns dieses Ziel zu erreichen.

Die statistisch ausgewerteten Daten zeigen uns ein klares Bild von den Stärken und Schwächen unserer Website. Einerseits können wir unsere Seite so optimieren, dass sie von interessierten Menschen auf Google leichter gefunden wird. Andererseits helfen uns die Daten, Sie als Besucher besser zu verstehen. Wir wissen somit sehr genau, was wir an unserer Website verbessern müssen, um Ihnen das bestmögliche Service zu bieten. Die Daten dienen uns auch, unsere Werbe- und Marketing-Maßnahmen individueller und kostengünstiger durchzuführen. Schließlich macht es nur Sinn, unsere Produkte und Dienstleistungen Menschen zu zeigen, die sich dafür interessieren.

### Welche Daten werden von Google Analytics gespeichert?

Google Analytics erstellt mithilfe eines Tracking-Codes eine zufällige, eindeutige ID, die mit Ihrem Browser-Cookie verbunden ist. So erkennt Sie Google Analytics als neuen User. Wenn Sie das nächste Mal unsere Seite besuchen, werden Sie als „wiederkehrender“ User erkannt. Alle gesammelten Daten werden gemeinsam mit dieser User-ID gespeichert. So ist es überhaupt erst möglich pseudonyme Userprofile auszuwerten.

Um mit Google Analytics unsere Website analysieren zu können, muss eine Property-ID in den Tracking-Code eingefügt werden. Die Daten werden dann in der entsprechenden Property gespeichert. Für jede neu angelegte Property ist die Google Analytics 4-Property standardmäßig. Alternativ kann man aber auch noch die Universal Analytics Property erstellen. Je nach verwendeter Property werden Daten unterschiedlich lange gespeichert.

Durch Kennzeichnungen wie Cookies und App-Instanz-IDs werden Ihre Interaktionen auf unserer Website gemessen. Interaktionen sind alle Arten von Handlungen, die Sie auf unserer Website ausführen. Wenn Sie auch andere Google-Systeme (wie z.B. ein Google-Konto) nützen, können über Google Analytics generierte Daten mit Drittanbieter-Cookies verknüpft werden. Google gibt keine Google Analytics-Daten weiter, außer wir als Websitebetreiber genehmigen das. Zu Ausnahmen kann es kommen, wenn es gesetzlich erforderlich ist.

Folgende Cookies werden von Google Analytics verwendet:

**Name:** \_ga  
**Wert:** 2.1326744211.152221148768-5  
**Verwendungszweck:** Standardmäßig verwendet analytics.js das Cookie \_ga, um die User-ID zu speichern. Grundsätzlich dient es zur Unterscheidung der Webseitenbesucher.  
**Ablaufdatum:** nach 2 Jahren

**Name:** \_gid  
**Wert:** 2.1687193234.152221148768-1  
**Verwendungszweck:** Das Cookie dient auch zur Unterscheidung der Webseitenbesucher  
**Ablaufdatum:** nach 24 Stunden

**Name:** \_gat\_gtag\_UA\_  
**Wert:** 1  
**Verwendungszweck:** Wird zum Senken der Anforderungsrate verwendet. Wenn Google Analytics über den Google Tag Manager bereitgestellt wird, erhält dieser Cookie den Namen \_dc\_gtm\_ .  
**Ablaufdatum:** nach 1 Minute

**Name:** AMP\_TOKEN  
**Wert:** keine Angaben  
**Verwendungszweck:** Das Cookie hat einen Token, mit dem eine User ID vom AMP-Client-ID-Dienst abgerufen werden kann. Andere mögliche Werte weisen auf eine Abmeldung, eine Anfrage oder einen Fehler hin.  
**Ablaufdatum:** nach 30 Sekunden bis zu einem Jahr

**Name:** \_\_utma  
**Wert:** 1564498958.1564498958.1564498958.1  
**Verwendungszweck:** Mit diesem Cookie kann man Ihr Verhalten auf der Website verfolgen und die Leistung messen. Das Cookie wird jedes Mal aktualisiert, wenn Informationen an Google Analytics gesendet werden.  
**Ablaufdatum:** nach 2 Jahren

**Name:** \_\_utmt  
**Wert:** 1  
**Verwendungszweck:** Das Cookie wird wie \_gat\_gtag\_UA\_ zum Drosseln der Anforderungsrate verwendet.  
**Ablaufdatum:** nach 10 Minuten

**Name:** \_\_utmb  
**Wert:** 3.10.1564498958  
**Verwendungszweck:** Dieses Cookie wird verwendet, um neue Sitzungen zu bestimmen. Es wird jedes Mal aktualisiert, wenn neue Daten bzw. Infos an Google Analytics gesendet werden.  
**Ablaufdatum:** nach 30 Minuten

**Name:** \_\_utmc  
**Wert:** 167421564  
**Verwendungszweck:** Dieses Cookie wird verwendet, um neue Sitzungen für wiederkehrende Besucher festzulegen. Dabei handelt es sich um ein Session-Cookie und wird nur solange gespeichert, bis Sie den Browser wieder schließen.  
**Ablaufdatum:** Nach Schließung des Browsers

**Name:** \_\_utmz  
**Wert:** m|utmccn=(referral)|utmcmd=referral|utmcct=/  
**Verwendungszweck:** Das Cookie wird verwendet, um die Quelle des Besucheraufkommens auf unserer Website zu identifizieren. Das heißt, das Cookie speichert, von wo Sie auf unsere Website gekommen sind. Das kann eine andere Seite bzw. eine Werbeschaltung gewesen sein.  
**Ablaufdatum:** nach 6 Monaten

**Name:** \_\_utmv  
**Wert:** keine Angabe  
**Verwendungszweck:** Das Cookie wird verwendet, um benutzerdefinierte Userdaten zu speichern. Es wird immer aktualisiert, wenn Informationen an Google Analytics gesendet werden.  
**Ablaufdatum:** nach 2 Jahren

**Anmerkung:** Diese Aufzählung kann keinen Anspruch auf Vollständigkeit erheben, da Google die Wahl ihrer Cookies immer wieder auch verändert.

Hier zeigen wir Ihnen einen Überblick über die wichtigsten Daten, die mit Google Analytics erhoben werden:

**Heatmaps:** Google legt sogenannte Heatmaps an. Über Heatmaps sieht man genau jene Bereiche, die Sie anklicken. So bekommen wir Informationen, wo Sie auf unserer Seite „unterwegs“ sind.

**Sitzungsdauer:** Als Sitzungsdauer bezeichnet Google die Zeit, die Sie auf unserer Seite verbringen, ohne die Seite zu verlassen. Wenn Sie 20 Minuten inaktiv waren, endet die Sitzung automatisch.

**Absprungrate** (engl. Bouncerate): Von einem Absprung ist die Rede, wenn Sie auf unserer Website nur eine Seite ansehen und dann unsere Website wieder verlassen.

**Kontoerstellung:** Wenn Sie auf unserer Website ein Konto erstellen bzw. eine Bestellung machen, erhebt Google Analytics diese Daten.

**IP-Adresse:** Die IP-Adresse wird nur in gekürzter Form dargestellt, damit keine eindeutige Zuordnung möglich ist.

**Standort:** Über die IP-Adresse kann das Land und Ihr ungefährer Standort bestimmt werden. Diesen Vorgang bezeichnet man auch als IP- Standortbestimmung.

**Technische Informationen:** Zu den technischen Informationen zählen unter anderem Ihr Browsertyp, Ihr Internetanbieter oder Ihre Bildschirmauflösung.

**Herkunftsquelle:** Google Analytics beziehungsweise uns interessiert natürlich auch über welche Website oder welche Werbung Sie auf unsere Seite gekommen sind.

Weitere Daten sind Kontaktdaten, etwaige Bewertungen, das Abspielen von Medien (z.B., wenn Sie ein Video über unsere Seite abspielen), das Teilen von Inhalten über Social Media oder das Hinzufügen zu Ihren Favoriten. Die Aufzählung hat keinen Vollständigkeitsanspruch und dient nur zu einer allgemeinen Orientierung der Datenspeicherung durch Google Analytics.

### Wie lange und wo werden die Daten gespeichert?

Google hat Ihre Server auf der ganzen Welt verteilt. Die meisten Server befinden sich in Amerika und folglich werden Ihre Daten meist auf amerikanischen Servern gespeichert. Hier können Sie genau nachlesen wo sich die Google-Rechenzentren befinden: [https://www.google.com/about/datacenters/inside/locations/?hl=de](https://www.google.com/about/datacenters/inside/locations/?hl=de)

Ihre Daten werden auf verschiedenen physischen Datenträgern verteilt. Das hat den Vorteil, dass die Daten schneller abrufbar sind und vor Manipulation besser geschützt sind. In jedem Google-Rechenzentrum gibt es entsprechende Notfallprogramme für Ihre Daten. Wenn beispielsweise die Hardware bei Google ausfällt oder Naturkatastrophen Server lahmlegen, bleibt das Risiko einer Dienstunterbrechung bei Google dennoch gering.

Die Aufbewahrungsdauer der Daten hängt von den verwendeten Properties ab. Bei der Verwendung der neueren Google Analytics 4-Properties ist die Aufbewahrungsdauer Ihrer Userdaten auf 14 Monate fix eingestellt. Für andere sogenannte Ereignisdaten haben wir die Möglichkeit eine Aufbewahrungsdauer von 2 Monaten oder 14 Monaten zu wählen.

Bei Universal Analytics-Properties ist bei Google Analytics eine Aufbewahrungsdauer Ihrer Userdaten von 26 Monaten standardisiert eingestellt. Dann werden Ihre Userdaten gelöscht. Allerdings haben wir die Möglichkeit, die Aufbewahrungsdauer von Nutzdaten selbst zu wählen. Dafür stehen uns fünf Varianten zur Verfügung:

*   Löschung nach 14 Monaten
*   Löschung nach 26 Monaten
*   Löschung nach 38 Monaten
*   Löschung nach 50 Monaten
*   Keine automatische Löschung

Zusätzlich gibt es auch die Option, dass Daten erst dann gelöscht werden, wenn Sie innerhalb des von uns gewählten Zeitraums nicht mehr unsere Website besuchen. In diesem Fall wird die Aufbewahrungsdauer jedes Mal zurückgesetzt, wenn Sie unsere Website innerhalb des festgelegten Zeitraums wieder besuchen.

Wenn der festgelegte Zeitraum abgelaufen ist, werden einmal im Monat die Daten gelöscht. Diese Aufbewahrungsdauer gilt für Ihre Daten, die mit Cookies, Usererkennung und Werbe-IDs (z.B. Cookies der DoubleClick-Domain) verknüpft sind. Berichtergebnisse basieren auf aggregierten Daten und werden unabhängig von Nutzerdaten gespeichert. Aggregierte Daten sind eine Zusammenschmelzung von Einzeldaten zu einer größeren Einheit.

### Wie kann ich meine Daten löschen bzw. die Datenspeicherung verhindern?

Nach dem Datenschutzrecht der Europäischen Union haben Sie das Recht, Auskunft über Ihre Daten zu erhalten, sie zu aktualisieren, zu löschen oder einzuschränken. Mithilfe des Browser-Add-ons zur Deaktivierung von Google Analytics-JavaScript (ga.js, analytics.js, dc.js) verhindern Sie, dass Google Analytics Ihre Daten verwendet. Das Browser-Add-on können Sie unter [https://tools.google.com/dlpage/gaoptout?hl=de](https://tools.google.com/dlpage/gaoptout?hl=de) runterladen und installieren. Beachten Sie bitte, dass durch dieses Add-on nur die Datenerhebung durch Google Analytics deaktiviert wird.

Falls Sie grundsätzlich Cookies (unabhängig von Google Analytics) deaktivieren, löschen oder verwalten wollen, gibt es für jeden Browser eine eigene Anleitung:

[Chrome: Cookies in Chrome löschen, aktivieren und verwalten](https://support.google.com/chrome/answer/95647?tid=221148768)

[Safari: Verwalten von Cookies und Websitedaten mit Safari](https://support.apple.com/de-at/guide/safari/sfri11471/mac?tid=221148768)

[Firefox: Cookies löschen, um Daten zu entfernen, die Websites auf Ihrem Computer abgelegt haben](https://support.mozilla.org/de/kb/cookies-und-website-daten-in-firefox-loschen?tid=221148768)

[Internet Explorer: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/17442/windows-internet-explorer-delete-manage-cookies?tid=221148768)

[Microsoft Edge: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/4027947/windows-delete-cookies?tid=221148768)

Bitte beachten Sie, dass bei der Verwendung dieses Tools Daten von Ihnen auch außerhalb der EU gespeichert und verarbeitet werden können. Die meisten Drittstaaten (darunter auch die USA) gelten nach derzeitigem europäischen Datenschutzrecht als nicht sicher. Daten an unsichere Drittstaaten dürfen also nicht einfach übertragen, dort gespeichert und verarbeitet werden, sofern es keine passenden Garantien (wie etwa EU-Standardvertragsklauseln) zwischen uns und dem außereuropäischen Dienstleister gibt.

### Rechtsgrundlage

Der Einsatz von Google Analytics setzt Ihre Einwilligung voraus, welche wir mit unserem Cookie Popup eingeholt haben. Diese Einwilligung stellt laut **Art. 6 Abs. 1 lit. a DSGVO (Einwilligung)** die Rechtsgrundlage für die Verarbeitung personenbezogener Daten, wie sie bei der Erfassung durch Web-Analytics Tools vorkommen kann, dar.

Zusätzlich zur Einwilligung besteht von unserer Seite ein berechtigtes Interesse daran, dass Verhalten der Websitebesucher zu analysieren und so unser Angebot technisch und wirtschaftlich zu verbessern. Mit Hilfe von Google Analytics erkennen wir Fehler der Website, können Attacken identifizieren und die Wirtschaftlichkeit verbessern. Die Rechtsgrundlage dafür ist **Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)**.

Wir weisen darauf hin, dass nach Meinung des Europäischen Gerichtshofs derzeit kein angemessenes Schutzniveau für den Datentransfer in die USA besteht. Die Datenverarbeitung geschieht im Wesentlichen durch Google. Dies kann dazu führen, dass gegebenenfalls Daten nicht anonymisiert verarbeitet und gespeichert werden. Ferner können gegebenenfalls US-amerikanische staatliche Behörden Zugriff auf einzelne Daten nehmen. Es kann ferner vorkommen, dass diese Daten mit Daten aus anderen Diensten von Google, bei denen Sie ein Nutzerkonto haben, verknüpft werden.

Wir hoffen, wir konnten Ihnen die wichtigsten Informationen rund um die Datenverarbeitung von Google Analytics näherbringen. Wenn Sie mehr über den Tracking-Dienst erfahren wollen, empfehlen wir diese beiden Links: [http://www.google.com/analytics/terms/de.html](http://www.google.com/analytics/terms/de.html) und [https://support.google.com/analytics/answer/6004245?hl=de](https://support.google.com/analytics/answer/6004245?hl=de).

Facebook Datenschutzerklärung
-----------------------------

**Facebook Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Besucher der Website  
🤝 Zweck: Optimierung unserer Serviceleistung  
📓 Verarbeitete Daten: Daten wie etwa Kundendaten, Daten zum Nutzerverhalten, Informationen zu Ihrem Gerät und Ihre IP-Adresse.  
Mehr Details dazu finden Sie weiter unten in der Datenschutzerklärung.  
📅 Speicherdauer: bis die Daten für Facebooks Zwecke nicht mehr nützlich sind  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)

### Was sind Facebook-Tools?

Wir verwenden auf unserer Webseite ausgewählte Tools von Facebook. Facebook ist ein Social Media Network des Unternehmens Facebook Ireland Ltd., 4 Grand Canal Square, Grand Canal Harbour, Dublin 2 Ireland. Mithilfe dieser Tools können wir Ihnen und Menschen, die sich für unsere Produkte und Dienstleistungen interessieren, das bestmögliche Angebot bieten. Im Folgenden geben wir einen Überblick über die verschiedenen Facebook Tools, welche Daten an Facebook gesendet werden und wie Sie diese Daten löschen können.

Neben vielen anderen Produkten bietet Facebook auch die sogenannten “Facebook Business Tools” an. Das ist die offizielle Bezeichnung von Facebook. Da der Begriff aber kaum bekannt ist, haben wir uns dafür entschieden, sie lediglich Facebook-Tools zu nennen. Darunter finden sich unter anderem:

*   Facebook-Pixel
*   soziale Plug-ins (wie z.B der „Gefällt mir“- oder „Teilen“-Button)
*   Facebook Login
*   Account Kit
*   APIs (Programmierschnittstelle)
*   SDKs (Sammlung von Programmierwerkzeugen)
*   Plattform-Integrationen
*   Plugins
*   Codes
*   Spezifikationen
*   Dokumentationen
*   Technologien und Dienstleistungen

Durch diese Tools erweitert Facebook Dienstleistungen und hat die Möglichkeit, Informationen über User-Aktivitäten außerhalb von Facebook zu erhalten.

### Warum verwenden wir Facebook-Tools auf unserer Website?

Wir wollen unsere Dienstleistungen und Produkte nur Menschen zeigen, die sich auch wirklich dafür interessieren. Mithilfe von Werbeanzeigen (Facebook-Ads) können wir genau diese Menschen erreichen. Damit den Usern passende Werbung gezeigt werden kann, benötigt Facebook allerdings Informationen über die Wünsche und Bedürfnisse der Menschen. So werden dem Unternehmen Informationen über das Userverhalten (und Kontaktdaten) auf unserer Webseite zur Verfügung gestellt. Dadurch sammelt Facebook bessere User-Daten und kann interessierten Menschen die passende Werbung über unsere Produkte bzw. Dienstleistungen anzeigen. Die Tools ermöglichen somit maßgeschneiderte Werbekampagnen auf Facebook.

Daten über Ihr Verhalten auf unserer Webseite nennt Facebook „Event-Daten“. Diese werden auch für Messungs- und Analysedienste verwendet. Facebook kann so in unserem Auftrag „Kampagnenberichte“ über die Wirkung unserer Werbekampagnen erstellen. Weiters bekommen wir durch Analysen einen besseren Einblick, wie Sie unsere Dienstleistungen, Webseite oder Produkte verwenden. Dadurch optimieren wir mit einigen dieser Tools Ihre Nutzererfahrung auf unserer Webseite. Beispielsweise können Sie mit den sozialen Plug-ins Inhalte auf unserer Seite direkt auf Facebook teilen.

### Welche Daten werden von Facebook-Tools gespeichert?

Durch die Nutzung einzelner Facebook-Tools können personenbezogene Daten (Kundendaten) an Facebook gesendet werden. Abhängig von den benutzten Tools können Kundendaten wie Name, Adresse, Telefonnummer und IP-Adresse versandt werden.

Facebook verwendet diese Informationen, um die Daten mit den Daten, die es selbst von Ihnen hat (sofern Sie Facebook-Mitglied sind) abzugleichen. Bevor Kundendaten an Facebook übermittelt werden, erfolgt ein sogenanntes „Hashing“. Das bedeutet, dass ein beliebig großer Datensatz in eine Zeichenkette transformiert wird. Dies dient auch der Verschlüsselung von Daten.

Neben den Kontaktdaten werden auch „Event-Daten“ übermittelt. Unter „Event-Daten“ sind jene Informationen gemeint, die wir über Sie auf unserer Webseite erhalten. Zum Beispiel, welche Unterseiten Sie besuchen oder welche Produkte Sie bei uns kaufen. Facebook teilt die erhaltenen Informationen nicht mit Drittanbietern (wie beispielsweise Werbetreibende), außer das Unternehmen hat eine explizite Genehmigung oder ist rechtlich dazu verpflichtet. „Event-Daten“ können auch mit Kontaktdaten verbunden werden. Dadurch kann Facebook bessere personalisierte Werbung anbieten. Nach dem bereits erwähnten Abgleichungsprozess löscht Facebook die Kontaktdaten wieder.

Um Werbeanzeigen optimiert ausliefern zu können, verwendet Facebook die Event-Daten nur, wenn diese mit anderen Daten (die auf andere Weise von Facebook erfasst wurden) zusammengefasst wurden. Diese Event-Daten nützt Facebook auch für Sicherheits-, Schutz-, Entwicklungs- und Forschungszwecke. Viele dieser Daten werden über Cookies zu Facebook übertragen. Cookies sind kleine Text-Dateien, die zum Speichern von Daten bzw. Informationen in Browsern verwendet werden. Je nach verwendeten Tools und abhängig davon, ob Sie Facebook-Mitglied sind, werden unterschiedlich viele Cookies in Ihrem Browser angelegt. In den Beschreibungen der einzelnen Facebook Tools gehen wir näher auf einzelne Facebook-Cookies ein. Allgemeine Informationen über die Verwendung von Facebook-Cookies erfahren Sie auch auf [https://www.facebook.com/policies/cookies](https://www.facebook.com/policies/cookies?tid=221148768).

### Wie lange und wo werden die Daten gespeichert?

Grundsätzlich speichert Facebook Daten bis sie nicht mehr für die eigenen Dienste und Facebook-Produkte benötigt werden. Facebook hat auf der ganzen Welt Server verteilt, wo seine Daten gespeichert werden. Kundendaten werden allerdings, nachdem sie mit den eigenen Userdaten abgeglichen wurden, innerhalb von 48 Stunden gelöscht.

### Wie kann ich meine Daten löschen bzw. die Datenspeicherung verhindern?

Entsprechend der Datenschutz Grundverordnung haben Sie das Recht auf Auskunft, Berichtigung, Übertragbarkeit und Löschung Ihrer Daten.

Eine komplette Löschung der Daten erfolgt nur, wenn Sie Ihr Facebook-Konto vollständig löschen. Und so funktioniert das Löschen Ihres Facebook-Kontos:

1) Klicken Sie rechts bei Facebook auf Einstellungen.

2) Anschließend klicken Sie in der linken Spalte auf „Deine Facebook-Informationen“.

3) Nun klicken Sie “Deaktivierung und Löschung”.

4) Wählen Sie jetzt „Konto löschen“ und klicken Sie dann auf „Weiter und Konto löschen“

5) Geben Sie nun Ihr Passwort ein, klicken Sie auf „Weiter“ und dann auf „Konto löschen“

Die Speicherung der Daten, die Facebook über unsere Seite erhält, erfolgt unter anderem über Cookies (z.B. bei sozialen Plugins). In Ihrem Browser können Sie einzelne oder alle Cookies deaktivieren, löschen oder verwalten. Je nach dem welchen Browser Sie verwenden, funktioniert dies auf unterschiedliche Art und Weise. Die folgenden Anleitungen zeigen, wie Sie Cookies in Ihrem Browser verwalten:

[Chrome: Cookies in Chrome löschen, aktivieren und verwalten](https://support.google.com/chrome/answer/95647?tid=221148768)

[Safari: Verwalten von Cookies und Websitedaten mit Safari](https://support.apple.com/de-at/guide/safari/sfri11471/mac?tid=221148768)

[Firefox: Cookies löschen, um Daten zu entfernen, die Websites auf Ihrem Computer abgelegt haben](https://support.mozilla.org/de/kb/cookies-und-website-daten-in-firefox-loschen?tid=221148768)

[Internet Explorer: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/17442/windows-internet-explorer-delete-manage-cookies?tid=221148768)

[Microsoft Edge: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/4027947/windows-delete-cookies?tid=221148768)

Falls Sie grundsätzlich keine Cookies haben wollen, können Sie Ihren Browser so einrichten, dass er Sie immer informiert, wenn ein Cookie gesetzt werden soll. So können Sie bei jedem einzelnen Cookie entscheiden, ob Sie es erlauben oder nicht.

Bitte beachten Sie, dass bei der Verwendung dieses Tools Daten von Ihnen auch außerhalb der EU gespeichert und verarbeitet werden können. Die meisten Drittstaaten (darunter auch die USA) gelten nach derzeitigem europäischen Datenschutzrecht als nicht sicher. Daten an unsichere Drittstaaten dürfen also nicht einfach übertragen, dort gespeichert und verarbeitet werden, sofern es keine passenden Garantien (wie etwa EU-Standardvertragsklauseln) zwischen uns und dem außereuropäischen Dienstleister gibt.

### Rechtsgrundlage

Wenn Sie eingewilligt haben, dass Daten von Ihnen durch eingebundene Social-Media-Elemente verarbeitet und gespeichert werden können, gilt diese Einwilligung als Rechtsgrundlage der Datenverarbeitung **(Art. 6 Abs. 1 lit. a DSGVO)**. Grundsätzlich werden Ihre Daten auch auf Grundlage unseres berechtigten Interesses **(Art. 6 Abs. 1 lit. f DSGVO)** an einer schnellen und guten Kommunikation mit Ihnen oder anderen Kunden und Geschäftspartnern gespeichert und verarbeitet. Die meisten Social-Media-Plattformen setzen auch Cookies in Ihrem Browser, um Daten zu speichern. Darum empfehlen wir Ihnen, unseren Datenschutztext über Cookies genau durchzulesen und die Datenschutzerklärung oder die Cookie-Richtlinien des jeweiligen Dienstanbieters anzusehen.

Wir hoffen wir haben Ihnen die wichtigsten Informationen über die Nutzung und Datenverarbeitung durch die Facebook-Tools nähergebracht. Wenn Sie mehr darüber erfahren wollen, wie Facebook Ihre Daten verwendet, empfehlen wir Ihnen die Datenrichtlinien auf [https://www.facebook.com/about/privacy/update](https://www.facebook.com/about/privacy/update).

Instagram Datenschutzerklärung
------------------------------

**Instagram Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Besucher der Website  
🤝 Zweck: Optimierung unserer Serviceleistung  
📓 Verarbeitete Daten: Daten wie etwa Daten zum Nutzerverhalten, Informationen zu Ihrem Gerät und Ihre IP-Adresse.  
Mehr Details dazu finden Sie weiter unten in der Datenschutzerklärung.  
📅 Speicherdauer: bis Instagram die Daten für ihre Zwecke nicht mehr benötigt  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)

### Was ist Instagram?

Wir haben auf unserer Webseite Funktionen von Instagram eingebaut. Instagram ist eine Social Media Plattform des Unternehmens Instagram LLC, 1601 Willow Rd, Menlo Park CA 94025, USA. Instagram ist seit 2012 ein Tochterunternehmen von Facebook Inc. und gehört zu den Facebook-Produkten. Das Einbetten von Instagram-Inhalten auf unserer Webseite nennt man Embedding. Dadurch können wir Ihnen Inhalte wie Buttons, Fotos oder Videos von Instagram direkt auf unserer Webseite zeigen. Wenn Sie Webseiten unserer Webpräsenz aufrufen, die eine Instagram-Funktion integriert haben, werden Daten an Instagram übermittelt, gespeichert und verarbeitet. Instagram verwendet dieselben Systeme und Technologien wie Facebook. Ihre Daten werden somit über alle Facebook-Firmen hinweg verarbeitet.

Im Folgenden wollen wir Ihnen einen genaueren Einblick geben, warum Instagram Daten sammelt, um welche Daten es sich handelt und wie Sie die Datenverarbeitung weitgehend kontrollieren können. Da Instagram zu Facebook Inc. gehört, beziehen wir unsere Informationen einerseits von den Instagram-Richtlinien, andererseits allerdings auch von den Facebook-Datenrichtlinien selbst.

Instagram ist eines der bekanntesten Social Media Netzwerken weltweit. Instagram kombiniert die Vorteile eines Blogs mit den Vorteilen von audiovisuellen Plattformen wie YouTube oder Vimeo. Sie können auf „Insta“ (wie viele der User die Plattform salopp nennen) Fotos und kurze Videos hochladen, mit verschiedenen Filtern bearbeiten und auch in anderen sozialen Netzwerken verbreiten. Und wenn Sie selbst nicht aktiv sein wollen, können Sie auch nur anderen interessante Users folgen.

### Warum verwenden wir Instagram auf unserer Website?

Instagram ist jene Social Media Plattform, die in den letzten Jahren so richtig durch die Decke ging. Und natürlich haben auch wir auf diesen Boom reagiert. Wir wollen, dass Sie sich auf unserer Webseite so wohl wie möglich fühlen. Darum ist für uns eine abwechslungsreiche Aufbereitung unserer Inhalte selbstverständlich. Durch die eingebetteten Instagram-Funktionen können wir unseren Content mit hilfreichen, lustigen oder spannenden Inhalten aus der Instagram-Welt bereichern. Da Instagram eine Tochtergesellschaft von Facebook ist, können uns die erhobenen Daten auch für personalisierte Werbung auf Facebook dienlich sein. So bekommen unsere Werbeanzeigen nur Menschen, die sich wirklich für unsere Produkte oder Dienstleistungen interessieren.

Instagram nützt die gesammelten Daten auch zu Messungs- und Analysezwecken. Wir bekommen zusammengefasste Statistiken und so mehr Einblick über Ihre Wünsche und Interessen. Wichtig ist zu erwähnen, dass diese Berichte Sie nicht persönlich identifizieren.

### Welche Daten werden von Instagram gespeichert?

Wenn Sie auf eine unserer Seiten stoßen, die Instagram-Funktionen (wie Instagrambilder oder Plug-ins) eingebaut haben, setzt sich Ihr Browser automatisch mit den Servern von Instagram in Verbindung. Dabei werden Daten an Instagram versandt, gespeichert und verarbeitet. Und zwar unabhängig, ob Sie ein Instagram-Konto haben oder nicht. Dazu zählen Informationen über unserer Webseite, über Ihren Computer, über getätigte Käufe, über Werbeanzeigen, die Sie sehen und wie Sie unser Angebot nutzen. Weiters werden auch Datum und Uhrzeit Ihrer Interaktion mit Instagram gespeichert. Wenn Sie ein Instagram-Konto haben bzw. eingeloggt sind, speichert Instagram deutlich mehr Daten über Sie.

Facebook unterscheidet zwischen Kundendaten und Eventdaten. Wir gehen davon aus, dass dies bei Instagram genau so der Fall ist. Kundendaten sind zum Beispiel Name, Adresse, Telefonnummer und IP-Adresse. Diese Kundendaten werden erst an Instagram übermittelt werden, wenn Sie zuvor „gehasht“ wurden. Hashing meint, ein Datensatz wird in eine Zeichenkette verwandelt. Dadurch kann man die Kontaktdaten verschlüsseln. Zudem werden auch die oben genannten „Event-Daten“ übermittelt. Unter „Event-Daten“ versteht Facebook – und folglich auch Instagram – Daten über Ihr Userverhalten. Es kann auch vorkommen, dass Kontaktdaten mit Event-Daten kombiniert werden. Die erhobenen Kontaktdaten werden mit den Daten, die Instagram bereits von Ihnen hat, abgeglichen.

Über kleine Text-Dateien (Cookies), die meist in Ihrem Browser gesetzt werden, werden die gesammelten Daten an Facebook übermittelt. Je nach verwendeten Instagram-Funktionen und ob Sie selbst ein Instagram-Konto haben, werden unterschiedlich viele Daten gespeichert.

Wir gehen davon aus, dass bei Instagram die Datenverarbeitung gleich funktioniert wie bei Facebook. Das bedeutet: wenn Sie ein Instagram-Konto haben oder [www.instagram.com](http://www.instagram.com?tid=221148768) besucht haben, hat Instagram zumindest ein Cookie gesetzt. Wenn das der Fall ist, sendet Ihr Browser über das Cookie Infos an Instagram, sobald Sie mit einer Instagram-Funktion in Berührung kommen. Spätestens nach 90 Tagen (nach Abgleichung) werden diese Daten wieder gelöscht bzw. anonymisiert. Obwohl wir uns intensiv mit der Datenverarbeitung von Instagram beschäftigt haben, können wir nicht ganz genau sagen, welche Daten Instagram exakt sammelt und speichert.

Im Folgenden zeigen wir Ihnen Cookies, die in Ihrem Browser mindestens gesetzt werden, wenn Sie auf eine Instagram-Funktion (wie z.B. Button oder ein Insta-Bild) klicken. Bei unserem Test gehen wir davon aus, dass Sie kein Instagram-Konto haben. Wenn Sie bei Instagram eingeloggt sind, werden natürlich deutlich mehr Cookies in Ihrem Browser gesetzt.

Diese Cookies wurden bei unserem Test verwendet:

**Name:** csrftoken  
**Wert:** “”  
**Verwendungszweck:** Dieses Cookie wird mit hoher Wahrscheinlichkeit aus Sicherheitsgründen gesetzt, um Fälschungen von Anfragen zu verhindern. Genauer konnten wir das allerdings nicht in Erfahrung bringen.  
**Ablaufdatum:** nach einem Jahr

**Name:** mid  
**Wert:** “”  
**Verwendungszweck:** Instagram setzt dieses Cookie, um die eigenen Dienstleistungen und Angebote in und außerhalb von Instagram zu optimieren. Das Cookie legt eine eindeutige User-ID fest.  
**Ablaufdatum:** nach Ende der Sitzung

**Name:** fbsr\_221148768124024  
**Wert:** keine Angaben  
**Verwendungszweck:** Dieses Cookie speichert die Log-in-Anfrage für User der Instagram-App.**Ablaufdatum:** nach Ende der Sitzung

**Name:** rur  
**Wert:** ATN  
**Verwendungszweck:** Dabei handelt es sich um ein Instagram-Cookie, das die Funktionalität auf Instagram gewährleistet.  
**Ablaufdatum:** nach Ende der Sitzung

**Name:** urlgen  
**Wert:** “{“194.96.75.33″: 1901}:1iEtYv:Y833k2\_UjKvXgYe221148768”  
**Verwendungszweck:** Dieses Cookie dient den Marketingzwecken von Instagram.  
**Ablaufdatum:** nach Ende der Sitzung

**Anmerkung:** Wir können hier keinen Vollständigkeitsanspruch erheben. Welche Cookies im individuellen Fall gesetzt werden, hängt von den eingebetteten Funktionen und Ihrer Verwendung von Instagram ab.

### Wie lange und wo werden die Daten gespeichert?

Instagram teilt die erhaltenen Informationen zwischen den Facebook-Unternehmen mit externen Partnern und mit Personen, mit denen Sie sich weltweit verbinden. Die Datenverarbeitung erfolgt unter Einhaltung der eigenen Datenrichtlinie. Ihre Daten sind, unter anderem aus Sicherheitsgründen, auf den Facebook-Servern auf der ganzen Welt verteilt. Die meisten dieser Server stehen in den USA.

### Wie kann ich meine Daten löschen bzw. die Datenspeicherung verhindern?

Dank der Datenschutz Grundverordnung haben Sie das Recht auf Auskunft, Übertragbarkeit, Berichtigung und Löschung Ihrer Daten. In den Instagram-Einstellungen können Sie Ihre Daten verwalten. Wenn Sie Ihre Daten auf Instagram völlig löschen wollen, müssen Sie Ihr Instagram-Konto dauerhaft löschen.

Und so funktioniert die Löschung des Instagram-Kontos:

Öffnen Sie zuerst die Instagram-App. Auf Ihrer Profilseite gehen Sie nach unten und klicken Sie auf „Hilfebereich“. Jetzt kommen Sie auf die Webseite des Unternehmens. Klicken Sie auf der Webseite auf „Verwalten des Kontos“ und dann auf „Dein Konto löschen“.

Wenn Sie Ihr Konto ganz löschen, löscht Instagram Posts wie beispielsweise Ihre Fotos und Status-Updates. Informationen, die andere Personen über Sie geteilt haben, gehören nicht zu Ihrem Konto und werden folglich nicht gelöscht.

Wie bereits oben erwähnt, speichert Instagram Ihre Daten in erster Linie über Cookies. Diese Cookies können Sie in Ihrem Browser verwalten, deaktivieren oder löschen. Abhängig von Ihrem Browser funktioniert die Verwaltung immer ein bisschen anders. Hier zeigen wir Ihnen die Anleitungen der wichtigsten Browser.

[Chrome: Cookies in Chrome löschen, aktivieren und verwalten](https://support.google.com/chrome/answer/95647?tid=221148768)

[Safari: Verwalten von Cookies und Websitedaten mit Safari](https://support.apple.com/de-at/guide/safari/sfri11471/mac?tid=221148768)

[Firefox: Cookies löschen, um Daten zu entfernen, die Websites auf Ihrem Computer abgelegt haben](https://support.mozilla.org/de/kb/cookies-und-website-daten-in-firefox-loschen?tid=221148768)

[Internet Explorer: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/17442/windows-internet-explorer-delete-manage-cookies?tid=221148768)

[Microsoft Edge: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/4027947/windows-delete-cookies?tid=221148768)

Sie können auch grundsätzlich Ihren Browser so einrichten, dass Sie immer informiert werden, wenn ein Cookie gesetzt werden soll. Dann können Sie immer individuell entscheiden, ob Sie das Cookie zulassen wollen oder nicht.

Bitte beachten Sie, dass bei der Verwendung dieses Tools Daten von Ihnen auch außerhalb der EU gespeichert und verarbeitet werden können. Die meisten Drittstaaten (darunter auch die USA) gelten nach derzeitigem europäischen Datenschutzrecht als nicht sicher. Daten an unsichere Drittstaaten dürfen also nicht einfach übertragen, dort gespeichert und verarbeitet werden, sofern es keine passenden Garantien (wie etwa EU-Standardvertragsklauseln) zwischen uns und dem außereuropäischen Dienstleister gibt.

### Rechtsgrundlage

Wenn Sie eingewilligt haben, dass Daten von Ihnen durch eingebundene Social-Media-Elemente verarbeitet und gespeichert werden können, gilt diese Einwilligung als Rechtsgrundlage der Datenverarbeitung **(Art. 6 Abs. 1 lit. a DSGVO)**. Grundsätzlich werden Ihre Daten auch auf Grundlage unseres berechtigten Interesses **(Art. 6 Abs. 1 lit. f DSGVO)** an einer schnellen und guten Kommunikation mit Ihnen oder anderen Kunden und Geschäftspartnern gespeichert und verarbeitet. Die meisten Social-Media-Plattformen setzen auch Cookies in Ihrem Browser, um Daten zu speichern. Darum empfehlen wir Ihnen, unseren Datenschutztext über Cookies genau durchzulesen und die Datenschutzerklärung oder die Cookie-Richtlinien des jeweiligen Dienstanbieters anzusehen.

Wir haben versucht, Ihnen die wichtigsten Informationen über die Datenverarbeitung durch Instagram näherzubringen. Auf [https://help.instagram.com/519522125107875](https://help.instagram.com/519522125107875)  
können Sie sich noch näher mit den Datenrichtlinien von Instagram auseinandersetzen.

ShareThis Datenschutzerklärung
------------------------------

**ShareThis Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Besucher der Website  
🤝 Zweck: Optimierung unserer Serviceleistung  
📓 Verarbeitete Daten: Daten wie etwa Daten zum Nutzerverhalten, Informationen zu Ihrem Gerät und Ihre IP-Adresse.  
Mehr Details dazu finden Sie weiter unten in der Datenschutzerklärung.  
📅 Speicherdauer: die gesammelten Daten werden bis zu 14 Monate gespeichert  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)

### Was ist ShareThis?

Auf unserer Website haben wir Funktionen von ShareThis der Firma ShareThis Inc. (4005 Miranda Ave, Suite 100, Palo Alto, 94304 Kalifornien, USA) eingebaut. Dabei handelt es sich zum Beispiel um “Teilen”-Plug-ins verschiedener Social-Media-Kanäle. Mit Hilfe dieser Funktionen können Sie Inhalte unserer Website auf Social-Media-Kanälen teilen. Wenn Sie eine Webseite mit einer ShareThis-Funktion aufrufen, können Daten von Ihnen an das Unternehmen übertragen, gespeichert und verarbeitet werden. Mit dieser Datenschutzerklärungen erfahren Sie warum wir ShareThis verwenden, welche Daten verarbeitet werden und wie Sie diese Datenübertragung unterbinden können.

ShareThis ist ein Technologieunternehmen, das Websitebetreibern Tools zur Steigerung der Websitequalität anbietet. Durch die Nutzung der Social-Plugins von ShareThis können Sie Inhalte unserer Website in verschiedenen Social-Media-Kanälen wie Facebook, Twitter, Instagram und Co teilen. Das Unternehmen bietet das Teilen von Inhalten für über 40 verschiedene Kanäle an und wird von über 3 Millionen Websitebetreibern weltweit genutzt. Die von ShareThis gesammelten Daten werden auch für individuelle Werbeanzeigen genutzt.

### Warum verwenden wir ShareThis auf unserer Website?

Wir wollen mit unserem Content überzeugen und natürlich freuen wir uns, wenn unser Content auch weiterempfohlen wird. Dann wissen wir, wir sind am richtigen Weg. Am einfachsten funktioniert das über „Share/Teilen-Buttons“ direkt auf unserer Website. Durch die Vielzahl an verschiedenen Social-Media-Kanälen kann so unser Content auch einem breiten Publikum präsentiert werden. Das hilft uns im Internet bekannter und erfolgreicher zu werden. Zudem dienen die Plug-ins auch Ihnen, weil Sie mit nur einem Klick, interessante Inhalte mit Ihrer Social-Media-Community teilen können.

### Welche Daten werden von ShareThis gespeichert?

Wenn Sie Inhalte mit ShareThis teilen und Sie mit dem jeweiligen Social-Media-Konto angemeldet sind, können Daten wie beispielsweise der Besuch auf unserer Website und das Teilen von Inhalten dem Userkonto des entsprechenden Social-Media-Kanals zugeordnet werden. ShareThis verwendet Cookies, Pixel, HTTP-Header und Browser Identifier, um Daten zu Ihrem Besucherverhalten zu sammeln. Zudem werden manche dieser Daten nach einer Pseudonymisierung mit Dritten geteilt.

Hier eine Liste der möglicherweise verarbeiteten Daten:

*   Eindeutige ID eines im Webbrowser platzierten Cookies
*   Allgemeines Klickverhalten
*   Adressen der besuchten Webseiten
*   Suchanfragen über die ein Besucher zur Seite mit ShareThis gelangt ist
*   Navigation von Webseite zu Webseite falls dies über ShareThis Dienste abgelaufen ist
*   Verweildauer auf einer Webseite
*   Welche Elemente angeklickt oder hervorgehoben wurden
*   Die IP-Adresse des Computers oder mobilen Gerätes
*   Mobile Werbe-IDs (Apple IDFA oder Google AAID)
*   In HTTP-Headern oder anderen verwendeten Übertragungsprotokollen enthaltene Informationen
*   Welches Programm auf dem Computer (Browser) oder welches Betriebssystem verwendet wurde (iOS)

ShareThis verwendet Cookies, die wir im Folgenden beispielhaft auflisten. Mehr zu den ShareThis Cookies finden Sie unter [https://www.sharethis.com/privacy/](https://www.sharethis.com/privacy/).

**Name:** \_\_unam  
**Wert:** 8961a7f179d1d017ac27lw87qq69V69221148768-5  
**Verwendungszweck:** Dieses Cookie zählt die „Clicks“ und „Shares“ auf einer Webseite.  
**Ablaufdatum:** nach 9 Monaten

**Name:** \_\_stid  
**Wert:** aGCDwF4hjVEI+oIsABW7221148768Ag==  
**Verwendungszweck:** Dieses Cookie speichert Userverhalten, wie zum Beispiel die aufgerufenen Webseiten, die Navigation von Seite zu Seite und die Verweildauer auf der Webseite.**Ablaufdatum:** nach 2 Jahren

**Name:** \_\_sharethis\_cookie\_test\_\_  
**Wert:** 0  
**Verwendungszweck:** Dieses Cookie überwacht die „Clickstream“-Aktivität. Das bedeutet es beobachtet wo Sie auf der Webseite geklickt haben.**Ablaufdatum:** nach Sitzungsende

**Anmerkung:** Wir können hier keinen Vollständigkeitsanspruch erheben. Welche Cookies im individuellen Fall gesetzt werden, hängt von den eingebetteten Funktionen und Ihrer Verwendung ab.

### Wie lange und wo werden die Daten gespeichert?

ShareThis bewahrt gesammelte Daten für einen Zeitraum von bis zu 14 Monaten ab dem Datum der Datenerfassung auf. ShareThis-Cookies laufen 13 Monate nach der letzten Aktualisierung ab. Da ShareThis ein amerikanisches Unternehmen ist, werden Daten auf amerikanischen ShareThis-Servern übertragen und gespeichert.

### Wie kann ich meine Daten löschen bzw. die Datenspeicherung verhindern?

Wenn Sie keine Werbung mehr sehen möchten, die auf von ShareThis gesammelten Daten basiert, können Sie den Opt-out-Button auf [https://www.sharethis.com/privacy/](https://www.sharethis.com/privacy/?tid=221148768) verwenden. Dabei wird ein Opt-out-Cookie gesetzt, das Sie nicht löschen dürfen, um diese Einstellung weiterhin zu behalten.

Sie können Ihre Präferenzen für nutzungsbasierte Onlinewerbung auch über [http://www.youronlinechoices.com/at/](http://www.youronlinechoices.com/at/) im Präferenzmanagement festlegen.

Weiters haben Sie auch die Möglichkeit Daten, die über Cookies gespeichert werden, in Ihrem Browser zu verwalten, zu deaktivieren oder zu löschen. Wie die Verwaltung genau funktioniert, hängt von Ihrem Browser ab. Hier finden Sie die Anleitungen zu den momentan bekanntesten Browsern.

[Chrome: Cookies in Chrome löschen, aktivieren und verwalten](https://support.google.com/chrome/answer/95647?tid=221148768)

[Safari: Verwalten von Cookies und Websitedaten mit Safari](https://support.apple.com/de-at/guide/safari/sfri11471/mac?tid=221148768)

[Firefox: Cookies löschen, um Daten zu entfernen, die Websites auf Ihrem Computer abgelegt haben](https://support.mozilla.org/de/kb/cookies-und-website-daten-in-firefox-loschen?tid=221148768)

[Internet Explorer: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/17442/windows-internet-explorer-delete-manage-cookies?tid=221148768)

[Microsoft Edge: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/4027947/windows-delete-cookies?tid=221148768)

Sie können Ihren Browser auch so einrichten, dass Sie immer informiert werden, wenn ein Cookie gesetzt werden soll.

Bitte beachten Sie, dass bei der Verwendung dieses Tools Daten von Ihnen auch außerhalb der EU gespeichert und verarbeitet werden können. Die meisten Drittstaaten (darunter auch die USA) gelten nach derzeitigem europäischen Datenschutzrecht als nicht sicher. Daten an unsichere Drittstaaten dürfen also nicht einfach übertragen, dort gespeichert und verarbeitet werden, sofern es keine passenden Garantien (wie etwa EU-Standardvertragsklauseln) zwischen uns und dem außereuropäischen Dienstleister gibt.

### Rechtsgrundlage

Wenn Sie eingewilligt haben, dass Daten von Ihnen durch eingebundene Social-Media-Elemente verarbeitet und gespeichert werden können, gilt diese Einwilligung als Rechtsgrundlage der Datenverarbeitung **(Art. 6 Abs. 1 lit. a DSGVO)**. Grundsätzlich werden Ihre Daten auch auf Grundlage unseres berechtigten Interesses **(Art. 6 Abs. 1 lit. f DSGVO)** an einer schnellen und guten Kommunikation mit Ihnen oder anderen Kunden und Geschäftspartnern gespeichert und verarbeitet. Die meisten Social-Media-Plattformen setzen auch Cookies in Ihrem Browser, um Daten zu speichern. Darum empfehlen wir Ihnen, unseren Datenschutztext über Cookies genau durchzulesen und die Datenschutzerklärung oder die Cookie-Richtlinien des jeweiligen Dienstanbieters anzusehen.

Wenn Sie mehr über die Verarbeitung Ihrer Daten durch ShareThis wissen möchten, finden Sie alle Informationen unter [https://www.sharethis.com/privacy/](https://www.sharethis.com/privacy/?tid=221148768).

YouTube Datenschutzerklärung
----------------------------

**YouTube Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Besucher der Website  
🤝 Zweck: Optimierung unserer Serviceleistung  
📓 Verarbeitete Daten: Daten wie etwa Kontaktdaten, Daten zum Nutzerverhalten, Informationen zu Ihrem Gerät und Ihre IP-Adresse können gespeichert werden.  
Mehr Details dazu finden Sie weiter unten in dieser Datenschutzerklärung.  
📅 Speicherdauer: Daten bleiben grundsätzlich gespeichert, solange sie für den Dienstzweck nötig sind  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)

### Was ist YouTube?

Wir haben auf unserer Website YouTube-Videos eingebaut. So können wir Ihnen interessante Videos direkt auf unserer Seite präsentieren. YouTube ist ein Videoportal, das seit 2006 eine Tochterfirma von Google ist. Betrieben wird das Videoportal durch YouTube, LLC, 901 Cherry Ave., San Bruno, CA 94066, USA. Wenn Sie auf unserer Website eine Seite aufrufen, die ein YouTube-Video eingebettet hat, verbindet sich Ihr Browser automatisch mit den Servern von YouTube bzw. Google. Dabei werden (je nach Einstellungen) verschiedene Daten übertragen. Für die gesamte Datenverarbeitung im europäischen Raum ist Google Ireland Limited (Gordon House, Barrow Street Dublin 4, Irland) verantwortlich.

Im Folgenden wollen wir Ihnen genauer erklären, welche Daten verarbeitet werden, warum wir YouTube-Videos eingebunden haben und wie Sie Ihre Daten verwalten oder löschen können.

Auf YouTube können die User kostenlos Videos ansehen, bewerten, kommentieren und selbst hochladen. Über die letzten Jahre wurde YouTube zu einem der wichtigsten Social-Media-Kanäle weltweit. Damit wir Videos auf unserer Webseite anzeigen können, stellt YouTube einen Codeausschnitt zur Verfügung, den wir auf unserer Seite eingebaut haben.

### Warum verwenden wir YouTube-Videos auf unserer Website?

YouTube ist die Videoplattform mit den meisten Besuchern und dem besten Content. Wir sind bemüht, Ihnen die bestmögliche User-Erfahrung auf unserer Webseite zu bieten. Und natürlich dürfen interessante Videos dabei nicht fehlen. Mithilfe unserer eingebetteten Videos stellen wir Ihnen neben unseren Texten und Bildern weiteren hilfreichen Content zur Verfügung. Zudem wird unsere Webseite auf der Google-Suchmaschine durch die eingebetteten Videos leichter gefunden. Auch wenn wir über Google Ads Werbeanzeigen schalten, kann Google – dank der gesammelten Daten – diese Anzeigen wirklich nur Menschen zeigen, die sich für unsere Angebote interessieren.

### Welche Daten werden von YouTube gespeichert?

Sobald Sie eine unserer Seiten besuchen, die ein YouTube-Video eingebaut hat, setzt YouTube zumindest ein Cookie, das Ihre IP-Adresse und unsere URL speichert. Wenn Sie in Ihrem YouTube-Konto eingeloggt sind, kann YouTube Ihre Interaktionen auf unserer Webseite meist mithilfe von Cookies Ihrem Profil zuordnen. Dazu zählen Daten wie Sitzungsdauer, Absprungrate, ungefährer Standort, technische Informationen wie Browsertyp, Bildschirmauflösung oder Ihr Internetanbieter. Weitere Daten können Kontaktdaten, etwaige Bewertungen, das Teilen von Inhalten über Social Media oder das Hinzufügen zu Ihren Favoriten auf YouTube sein.

Wenn Sie nicht in einem Google-Konto oder einem Youtube-Konto angemeldet sind, speichert Google Daten mit einer eindeutigen Kennung, die mit Ihrem Gerät, Browser oder App verknüpft sind. So bleibt beispielsweise Ihre bevorzugte Spracheinstellung beibehalten. Aber viele Interaktionsdaten können nicht gespeichert werden, da weniger Cookies gesetzt werden.

In der folgenden Liste zeigen wir Cookies, die in einem Test im Browser gesetzt wurden. Wir zeigen einerseits Cookies, die ohne angemeldetes YouTube-Konto gesetzt werden. Andererseits zeigen wir Cookies, die mit angemeldetem Account gesetzt werden. Die Liste kann keinen Vollständigkeitsanspruch erheben, weil die Userdaten immer von den Interaktionen auf YouTube abhängen.

**Name:** YSC  
**Wert:** b9-CV6ojI5Y221148768-1  
**Verwendungszweck:** Dieses Cookie registriert eine eindeutige ID, um Statistiken des gesehenen Videos zu speichern.  
**Ablaufdatum:** nach Sitzungsende

**Name:** PREF  
**Wert:** f1=50000000  
**Verwendungszweck:** Dieses Cookie registriert ebenfalls Ihre eindeutige ID. Google bekommt über PREF Statistiken, wie Sie YouTube-Videos auf unserer Webseite verwenden.  
**Ablaufdatum:** nach 8 Monaten

**Name:** GPS  
**Wert:** 1  
**Verwendungszweck:** Dieses Cookie registriert Ihre eindeutige ID auf mobilen Geräten, um den GPS-Standort zu tracken.  
**Ablaufdatum:** nach 30 Minuten

**Name:** VISITOR\_INFO1\_LIVE  
**Wert:** 95Chz8bagyU  
**Verwendungszweck:** Dieses Cookie versucht die Bandbreite des Users auf unseren Webseiten (mit eingebautem YouTube-Video) zu schätzen.  
**Ablaufdatum:** nach 8 Monaten

Weitere Cookies, die gesetzt werden, wenn Sie mit Ihrem YouTube-Konto angemeldet sind:

**Name:** APISID  
**Wert:** zILlvClZSkqGsSwI/AU1aZI6HY7221148768-  
**Verwendungszweck:** Dieses Cookie wird verwendet, um ein Profil über Ihre Interessen zu erstellen. Genützt werden die Daten für personalisierte Werbeanzeigen.  
**Ablaufdatum:** nach 2 Jahren

**Name:** CONSENT  
**Wert:** YES+AT.de+20150628-20-0  
**Verwendungszweck:** Das Cookie speichert den Status der Zustimmung eines Users zur Nutzung unterschiedlicher Services von Google. CONSENT dient auch der Sicherheit, um User zu überprüfen und Userdaten vor unbefugten Angriffen zu schützen.  
**Ablaufdatum:** nach 19 Jahren

**Name:** HSID  
**Wert:** AcRwpgUik9Dveht0I  
**Verwendungszweck:** Dieses Cookie wird verwendet, um ein Profil über Ihre Interessen zu erstellen. Diese Daten helfen personalisierte Werbung anzeigen zu können.  
**Ablaufdatum:** nach 2 Jahren

**Name:** LOGIN\_INFO  
**Wert:** AFmmF2swRQIhALl6aL…  
**Verwendungszweck:** In diesem Cookie werden Informationen über Ihre Login-Daten gespeichert.  
**Ablaufdatum:** nach 2 Jahren

**Name:** SAPISID  
**Wert:** 7oaPxoG-pZsJuuF5/AnUdDUIsJ9iJz2vdM  
**Verwendungszweck:** Dieses Cookie funktioniert, indem es Ihren Browser und Ihr Gerät eindeutig identifiziert. Es wird verwendet, um ein Profil über Ihre Interessen zu erstellen.  
**Ablaufdatum:** nach 2 Jahren

**Name:** SID  
**Wert:** oQfNKjAsI221148768-  
**Verwendungszweck:** Dieses Cookie speichert Ihre Google-Konto-ID und Ihren letzten Anmeldezeitpunkt in digital signierter und verschlüsselter Form.  
**Ablaufdatum:** nach 2 Jahren

**Name:** SIDCC  
**Wert:** AN0-TYuqub2JOcDTyL  
**Verwendungszweck:** Dieses Cookie speichert Informationen, wie Sie die Webseite nutzen und welche Werbung Sie vor dem Besuch auf unserer Seite möglicherweise gesehen haben.  
**Ablaufdatum:** nach 3 Monaten

### Wie lange und wo werden die Daten gespeichert?

Die Daten, die YouTube von Ihnen erhält und verarbeitet werden auf den Google-Servern gespeichert. Die meisten dieser Server befinden sich in Amerika. Unter [https://www.google.com/about/datacenters/inside/locations/?hl=de](https://www.google.com/about/datacenters/inside/locations/?hl=de)  sehen Sie genau wo sich die Google-Rechenzentren befinden. Ihre Daten sind auf den Servern verteilt. So sind die Daten schneller abrufbar und vor Manipulation besser geschützt.

Die erhobenen Daten speichert Google unterschiedlich lang. Manche Daten können Sie jederzeit löschen, andere werden automatisch nach einer begrenzten Zeit gelöscht und wieder andere werden von Google über längere Zeit gespeichert. Einige Daten (wie Elemente aus „Meine Aktivität“, Fotos oder Dokumente, Produkte), die in Ihrem Google-Konto gespeichert sind, bleiben so lange gespeichert, bis Sie sie löschen. Auch wenn Sie nicht in einem Google-Konto angemeldet sind, können Sie einige Daten, die mit Ihrem Gerät, Browser oder App verknüpft sind, löschen.

### Wie kann ich meine Daten löschen bzw. die Datenspeicherung verhindern?

Grundsätzlich können Sie Daten im Google Konto manuell löschen. Mit der 2019 eingeführten automatischen Löschfunktion von Standort- und Aktivitätsdaten werden Informationen abhängig von Ihrer Entscheidung – entweder 3 oder 18 Monate gespeichert und dann gelöscht.

Unabhängig, ob Sie ein Google-Konto haben oder nicht, können Sie Ihren Browser so konfigurieren, dass Cookies von Google gelöscht bzw. deaktiviert werden. Je nachdem welchen Browser Sie verwenden, funktioniert dies auf unterschiedliche Art und Weise. Die folgenden Anleitungen zeigen, wie Sie Cookies in Ihrem Browser verwalten:

[Chrome: Cookies in Chrome löschen, aktivieren und verwalten](https://support.google.com/chrome/answer/95647?tid=221148768)

[Safari: Verwalten von Cookies und Websitedaten mit Safari](https://support.apple.com/de-at/guide/safari/sfri11471/mac?tid=221148768)

[Firefox: Cookies löschen, um Daten zu entfernen, die Websites auf Ihrem Computer abgelegt haben](https://support.mozilla.org/de/kb/cookies-und-website-daten-in-firefox-loschen?tid=221148768)

[Internet Explorer: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/17442/windows-internet-explorer-delete-manage-cookies?tid=221148768)

[Microsoft Edge: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/4027947/windows-delete-cookies?tid=221148768)

Falls Sie grundsätzlich keine Cookies haben wollen, können Sie Ihren Browser so einrichten, dass er Sie immer informiert, wenn ein Cookie gesetzt werden soll. So können Sie bei jedem einzelnen Cookie entscheiden, ob Sie es erlauben oder nicht. Da YouTube ein Tochterunternehmen von Google ist, gibt es eine gemeinsame Datenschutzerklärung. Wenn Sie mehr über den Umgang mit Ihren Daten erfahren wollen, empfehlen wir Ihnen die Datenschutzerklärung unter [https://policies.google.com/privacy?hl=de.](https://policies.google.com/privacy?hl=de)

### Rechtsgrundlage

Wenn Sie eingewilligt haben, dass Daten von Ihnen durch eingebundene YouTube-Elemente verarbeitet und gespeichert werden können, gilt diese Einwilligung als Rechtsgrundlage der Datenverarbeitung **(Art. 6 Abs. 1 lit. a DSGVO)**. Grundsätzlich werden Ihre Daten auch auf Grundlage unseres berechtigten Interesses **(Art. 6 Abs. 1 lit. f DSGVO)** an einer schnellen und guten Kommunikation mit Ihnen oder anderen Kunden und Geschäftspartnern gespeichert und verarbeitet. YouTube setzt auch Cookies in Ihrem Browser, um Daten zu speichern. Darum empfehlen wir Ihnen, unseren Datenschutztext über Cookies genau durchzulesen und die Datenschutzerklärung oder die Cookie-Richtlinien des jeweiligen Dienstanbieters anzusehen.

Google Fonts Datenschutzerklärung
---------------------------------

**Google Fonts Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Besucher der Website  
🤝 Zweck: Optimierung unserer Serviceleistung  
📓 Verarbeitete Daten: Daten wie etwa IP-Adresse und CSS- und Schrift-Anfragen  
Mehr Details dazu finden Sie weiter unten in dieser Datenschutzerklärung.  
📅 Speicherdauer: Font-Dateien werden bei Google ein Jahr gespeichert  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)

### Was sind Google Fonts?

Auf unserer Website verwenden wir Google Fonts. Das sind die “Google-Schriften” der Firma Google Inc. Für den europäischen Raum ist das Unternehmen Google Ireland Limited (Gordon House, Barrow Street Dublin 4, Irland) für alle Google-Dienste verantwortlich.

Für die Verwendung von Google-Schriftarten müssen Sie sich nicht anmelden bzw. ein Passwort hinterlegen. Weiters werden auch keine Cookies in Ihrem Browser gespeichert. Die Dateien (CSS, Schriftarten/Fonts) werden über die Google-Domains fonts.googleapis.com und fonts.gstatic.com angefordert. Laut Google sind die Anfragen nach CSS und Schriften vollkommen getrennt von allen anderen Google-Diensten. Wenn Sie ein Google-Konto haben, brauchen Sie keine Sorge haben, dass Ihre Google-Kontodaten, während der Verwendung von Google Fonts, an Google übermittelt werden. Google erfasst die Nutzung von CSS (Cascading Style Sheets) und der verwendeten Schriftarten und speichert diese Daten sicher. Wie die Datenspeicherung genau aussieht, werden wir uns noch im Detail ansehen.

Google Fonts (früher Google Web Fonts) ist ein Verzeichnis mit über 800 Schriftarten, die [Google](https://de.wikipedia.org/wiki/Google_LLC?tid=221148768) Ihren Nutzern kostenlos zu Verfügung stellen.

Viele dieser Schriftarten sind unter der SIL Open Font License veröffentlicht, während andere unter der Apache-Lizenz veröffentlicht wurden. Beides sind freie Software-Lizenzen.

### Warum verwenden wir Google Fonts auf unserer Website?

Mit Google Fonts können wir auf der eigenen Webseite Schriften nutzen, und müssen sie nicht auf unserem eigenen Server hochladen. Google Fonts ist ein wichtiger Baustein, um die Qualität unserer Webseite hoch zu halten. Alle Google-Schriften sind automatisch für das Web optimiert und dies spart Datenvolumen und ist speziell für die Verwendung bei mobilen Endgeräten ein großer Vorteil. Wenn Sie unsere Seite besuchen, sorgt die niedrige Dateigröße für eine schnelle Ladezeit. Des Weiteren sind Google Fonts sichere Web Fonts. Unterschiedliche Bildsynthese-Systeme (Rendering) in verschiedenen Browsern, Betriebssystemen und mobilen Endgeräten können zu Fehlern führen. Solche Fehler können teilweise Texte bzw. ganze Webseiten optisch verzerren. Dank des schnellen Content Delivery Network (CDN) gibt es mit Google Fonts keine plattformübergreifenden Probleme. Google Fonts unterstützt alle gängigen Browser (Google Chrome, Mozilla Firefox, Apple Safari, Opera) und funktioniert zuverlässig auf den meisten modernen mobilen Betriebssystemen, einschließlich Android 2.2+ und iOS 4.2+ (iPhone, iPad, iPod). Wir verwenden die Google Fonts also, damit wir unser gesamtes Online-Service so schön und einheitlich wie möglich darstellen können.

### Welche Daten werden von Google gespeichert?

Wenn Sie unsere Webseite besuchen, werden die Schriften über einen Google-Server nachgeladen. Durch diesen externen Aufruf werden Daten an die Google-Server übermittelt. So erkennt Google auch, dass Sie bzw. Ihre IP-Adresse unsere Webseite besucht. Die Google Fonts API wurde entwickelt, um Verwendung, Speicherung und Erfassung von Endnutzerdaten auf das zu reduzieren, was für eine ordentliche Bereitstellung von Schriften nötig ist. API steht übrigens für „Application Programming Interface“ und dient unter anderem als Datenübermittler im Softwarebereich.

Google Fonts speichert CSS- und Schrift-Anfragen sicher bei Google und ist somit geschützt. Durch die gesammelten Nutzungszahlen kann Google feststellen, wie gut die einzelnen Schriften ankommen. Die Ergebnisse veröffentlicht Google auf internen Analyseseiten, wie beispielsweise Google Analytics. Zudem verwendet Google auch Daten des eigenen Web-Crawlers, um festzustellen, welche Webseiten Google-Schriften verwenden. Diese Daten werden in der BigQuery-Datenbank von Google Fonts veröffentlicht. Unternehmer und Entwickler nützen das Google-Webservice BigQuery, um große Datenmengen untersuchen und bewegen zu können.

Zu bedenken gilt allerdings noch, dass durch jede Google Font Anfrage auch Informationen wie Spracheinstellungen, IP-Adresse, Version des Browsers, Bildschirmauflösung des Browsers und Name des Browsers automatisch an die Google-Server übertragen werden. Ob diese Daten auch gespeichert werden, ist nicht klar feststellbar bzw. wird von Google nicht eindeutig kommuniziert.

### Wie lange und wo werden die Daten gespeichert?

Anfragen für CSS-Assets speichert Google einen Tag lang auf seinen Servern, die hauptsächlich außerhalb der EU angesiedelt sind. Das ermöglicht uns, mithilfe eines Google-Stylesheets die Schriftarten zu nutzen. Ein Stylesheet ist eine Formatvorlage, über die man einfach und schnell z.B. das Design bzw. die Schriftart einer Webseite ändern kann.

Die Font-Dateien werden bei Google ein Jahr gespeichert. Google verfolgt damit das Ziel, die Ladezeit von Webseiten grundsätzlich zu verbessern. Wenn Millionen von Webseiten auf die gleichen Schriften verweisen, werden sie nach dem ersten Besuch zwischengespeichert und erscheinen sofort auf allen anderen später besuchten Webseiten wieder. Manchmal aktualisiert Google Schriftdateien, um die Dateigröße zu reduzieren, die Abdeckung von Sprache zu erhöhen und das Design zu verbessern.

### Wie kann ich meine Daten löschen bzw. die Datenspeicherung verhindern?

Jene Daten, die Google für einen Tag bzw. ein Jahr speichert können nicht einfach gelöscht werden. Die Daten werden beim Seitenaufruf automatisch an Google übermittelt. Um diese Daten vorzeitig löschen zu können, müssen Sie den Google-Support auf [https://support.google.com/?hl=de&tid=221148768](https://support.google.com/?hl=de&tid=221148768) kontaktieren. Datenspeicherung verhindern Sie in diesem Fall nur, wenn Sie unsere Seite nicht besuchen.

Anders als andere Web-Schriften erlaubt uns Google uneingeschränkten Zugriff auf alle Schriftarten. Wir können also unlimitiert auf ein Meer an Schriftarten zugreifen und so das Optimum für unsere Webseite rausholen. Mehr zu Google Fonts und weiteren Fragen finden Sie auf [https://developers.google.com/fonts/faq?tid=221148768](https://developers.google.com/fonts/faq?tid=221148768). Dort geht zwar Google auf datenschutzrelevante Angelegenheiten ein, doch wirklich detaillierte Informationen über Datenspeicherung sind nicht enthalten. Es ist relativ schwierig, von Google wirklich präzise Informationen über gespeicherten Daten zu bekommen.

### Rechtsgrundlage

Wenn Sie eingewilligt haben, dass Google Fonts eingesetzt werden darf, ist die Rechtsgrundlage der entsprechenden Datenverarbeitung diese Einwilligung. Diese Einwilligung stellt laut **Art. 6 Abs. 1 lit. a DSGVO (Einwilligung)** die Rechtsgrundlage für die Verarbeitung personenbezogener Daten, wie sie bei der Erfassung durch Google Fonts vorkommen kann, dar.

Von unserer Seite besteht zudem ein berechtigtes Interesse, Google Font zu verwenden, um unser Online-Service zu optimieren. Die dafür entsprechende Rechtsgrundlage ist **Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)**.

Welche Daten grundsätzlich von Google erfasst werden und wofür diese Daten verwendet werden, können Sie auch auf [https://www.google.com/intl/de/policies/privacy/](https://policies.google.com/privacy?hl=de&tid=221148768) nachlesen.

Font Awesome Datenschutzerklärung
---------------------------------

**Font Awesome Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Besucher der Website  
🤝 Zweck: Optimierung unserer Serviceleistung  
📓 Verarbeitete Daten: etwa IP-Adresse und und welche Icon-Dateien geladen werden  
Mehr Details dazu finden Sie weiter unten in dieser Datenschutzerklärung.  
📅 Speicherdauer: Dateien in identifizierbarer Form werden wenige Wochen gespeichert  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)

### Was ist Font Awesome?

Wir verwenden auf unserer Website Font Awesome des amerikanischen Unternehmens Fonticons (307 S. Main St., Suite 202, Bentonville, AR 72712, USA). Wenn Sie eine unserer Webseite aufrufen, wird die Web-Schriftart Font Awesome (im Speziellen Icons) über das Font Awesome Content Delivery Netzwerk (CDN) geladen. So werden die Texte bzw. Schriften und Icons auf jedem Endgerät passend angezeigt. In dieser Datenschutzerklärung gehen wir näher auf die Datenspeicherung und Datenverarbeitung durch diesen Service ein.

Icons spielen für Websites eine immer wichtigere Rolle. Font Awesome ist eine Web-Schriftart, die speziell für Webdesigner und Webentwickler entwickelt wurde. Mit Font Awesome können etwa Icons mit Hilfe der Stylesheet-Sprache CSS nach Belieben skaliert und gefärbt werden. Sie ersetzen so alte Bild-Icons. Font Awesome CDN ist der einfachste Weg die Icons oder Schriftarten auf Ihre Website zu laden. Dafür mussten wir nur eine kleine Code-Zeile in unsere Website einbinden.

### Warum verwenden wir Font Awesome auf unserer Website?

Durch Font Awesome können Inhalte auf unserer Website besser aufbereitet werden. So können Sie sich auf unserer Website besser orientieren und die Inhalte leichter erfassen. Mit den Icons kann man sogar manchmal ganze Wörter ersetzen und Platz sparen. Da ist besonders praktisch, wenn wir Inhalte speziell für Smartphones optimieren.  Diese Icons werden statt als Bild als HMTL-Code eingefügt. Dadurch können wir die Icons mit CSS genauso bearbeiten, wie wir wollen. Gleichzeitig verbessern wir mit Font Awesome auch unsere Ladegeschwindigkeit, weil es sich nur um HTML-Elemente handelt und nicht um Icon-Bilder. All diese Vorteile helfen uns, die Website für Sie noch übersichtlicher, frischer und schneller zu machen.

### Welche Daten werden von Font Awesome gespeichert?

Zum Laden von Icons und Symbolen wird das Font Awesome Content Delivery Network (CDN) verwendet. CDNs sind Netzwerke von Servern, die weltweit verteilt sind und es möglich machen, schnell Dateien aus der Nähe zu laden. So werden auch, sobald Sie eine unserer Seiten aufrufen, die entsprechenden Icons von Font Awesome bereitgestellt.

Damit die Web-Schriftarten geladen werden können, muss Ihr Browser eine Verbindung zu den Servern des Unternehmens Fonticons, Inc. herstellen. Dabei wird Ihre IP-Adresse erkannt. Font Awesome sammelt auch Daten darüber, welche Icon-Dateien wann heruntergeladen werden. Weiters werden auch technische Daten wie etwa Ihre Browser-Version, Bildschirmauflösung oder der Zeitpunkt der ausgerufenen Seite übertragen.

Aus folgenden Gründen werden diese Daten gesammelt und gespeichert:

*   um Content Delivery Netzwerke zu optimieren
*   um technische Fehler zu erkennen und zu beheben
*   um CDNs vor Missbrauch und Angriffen zu schützen
*   um Gebühren von Font Awesome Pro-Kunden berechnen zu können
*   um die Beliebtheit von Icons zu erfahren
*   um zu wissen, welchen Computer und welche Software Sie verwenden

Falls Ihr Browser Web-Schriftarten nicht zulässt, wird automatisch eine Standardschrift Ihres PCs verwendet. Nach derzeitigem Stand unserer Erkenntnis werden keine Cookies gesetzt. Wir sind mit der Datenschutzabteilung von Font Awesome in Kontakt und geben Ihnen Bescheid, sobald wir näheres in Erfahrung bringen.

### Wie lange und wo werden die Daten gespeichert?

Font Awesome speichert Daten über die Nutzung des Content Delivery Network auf Servern auch in den Vereinigten Staaten von Amerika. Die CDN-Server befinden sich allerdings weltweit und speichern Userdaten, wo Sie sich befinden. In identifizierbarer Form werden die Daten in der Regel nur wenige Wochen gespeichert. Aggregierte Statistiken über die Nutzung von den CDNs können auch länger gespeichert werden. Personenbezogene Daten sind hier nicht enthalten.

### Wie kann ich meine Daten löschen bzw. die Datenspeicherung verhindern?

Font Awesome speichert nach aktuellem Stand unseres Wissens keine personenbezogenen Daten über die Content Delivery Netzwerke. Wenn Sie nicht wollen, dass Daten über die verwendeten Icons gespeichert werden, können Sie leider unsere Website nicht besuchen. Wenn Ihr Browser keine Web-Schriftarten erlaubt, werden auch keine Daten übertragen oder gespeichert. In diesem Fall wird einfach die Standard-Schrift Ihres Computers verwendet.

### Rechtsgrundlage

Wenn Sie eingewilligt haben, dass Font Awesome eingesetzt werden darf, ist die Rechtsgrundlage der entsprechenden Datenverarbeitung diese Einwilligung. Diese Einwilligung stellt laut **Art. 6 Abs. 1 lit. a DSGVO (Einwilligung)** die Rechtsgrundlage für die Verarbeitung personenbezogener Daten, wie sie bei der Erfassung durch Font Awesome vorkommen kann, dar.

Von unserer Seite besteht zudem ein berechtigtes Interesse, Font Awesome zu verwenden, um unser Online-Service zu optimieren. Die dafür entsprechende Rechtsgrundlage ist **Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)**.

Wenn Sie mehr über Font Awesome und deren Umgang mit Daten erfahren wollen, empfehlen wir Ihnen die Datenschutzerklärung unter [https://fontawesome.com/privacy](https://fontawesome.com/privacy) und die Hilfeseite unter [https://fontawesome.com/help](https://fontawesome.com/help?tid=221148768).

Google reCAPTCHA Datenschutzerklärung
-------------------------------------

**Google reCAPTCHA Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Besucher der Website  
🤝 Zweck: Optimierung unserer Serviceleistung und Schutz vor Cyberangriffen  
📓 Verarbeitete Daten: Daten wie etwa IP-Adresse, Browserinformationen, Ihr Betriebssystem, eingeschränkte Standorts- und Nutzungsdaten  
Mehr Details dazu finden Sie weiter unten in dieser Datenschutzerklärung.  
📅 Speicherdauer: abhängig von den gespeicherten Daten  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)

### Was ist reCAPTCHA?

Unser oberstes Ziel ist es, unsere Webseite für Sie und für uns bestmöglich zu sichern und zu schützen. Um das zu gewährleisten, verwenden wir Google reCAPTCHA der Firma Google Inc. Für den europäischen Raum ist das Unternehmen Google Ireland Limited (Gordon House, Barrow Street Dublin 4, Irland) für alle Google-Dienste verantwortlich. Mit reCAPTCHA können wir feststellen, ob Sie auch wirklich ein Mensch aus Fleisch und Blut sind und kein Roboter oder eine andere Spam-Software. Unter Spam verstehen wir jede, auf elektronischen Weg, unerwünschte Information, die uns ungefragter Weise zukommt. Bei den klassischen CAPTCHAS mussten Sie zur Überprüfung meist Text- oder Bildrätsel lösen. Mit reCAPTCHA von Google müssen wir Sie meist nicht mit solchen Rätseln belästigen. Hier reicht es in den meisten Fällen, wenn Sie einfach ein Häkchen setzen und so bestätigen, dass Sie kein Bot sind. Mit der neuen Invisible reCAPTCHA Version müssen Sie nicht mal mehr ein Häkchen setzen. Wie das genau funktioniert und vor allem welche Daten dafür verwendet werden, erfahren Sie im Verlauf dieser Datenschutzerklärung.

reCAPTCHA ist ein freier Captcha-Dienst von Google, der Webseiten vor Spam-Software und den Missbrauch durch nicht-menschliche Besucher schützt. Am häufigsten wird dieser Dienst verwendet, wenn Sie Formulare im Internet ausfüllen. Ein Captcha-Dienst ist eine Art automatischer Turing-Test, der sicherstellen soll, dass eine Handlung im Internet von einem Menschen und nicht von einem Bot vorgenommen wird. Im klassischen Turing-Test (benannt nach dem Informatiker Alan Turing) stellt ein Mensch die Unterscheidung zwischen Bot und Mensch fest. Bei Captchas übernimmt das auch der Computer bzw. ein Softwareprogramm. Klassische Captchas arbeiten mit kleinen Aufgaben, die für Menschen leicht zu lösen sind, doch für Maschinen erhebliche Schwierigkeiten aufweisen. Bei reCAPTCHA müssen Sie aktiv keine Rätsel mehr lösen. Das Tool verwendet moderne Risikotechniken, um Menschen von Bots zu unterscheiden. Hier müssen Sie nur noch das Textfeld „Ich bin kein Roboter“ ankreuzen bzw. bei Invisible reCAPTCHA ist selbst das nicht mehr nötig. Bei reCAPTCHA wird ein JavaScript-Element in den Quelltext eingebunden und dann läuft das Tool im Hintergrund und analysiert Ihr Benutzerverhalten. Aus diesen Useraktionen berechnet die Software einen sogenannten Captcha-Score. Google berechnet mit diesem Score schon vor der Captcha-Eingabe wie hoch die Wahrscheinlichkeit ist, dass Sie ein Mensch sind. reCAPTCHA bzw. Captchas im Allgemeinen kommen immer dann zum Einsatz, wenn Bots gewisse Aktionen (wie z.B. Registrierungen, Umfragen usw.) manipulieren oder missbrauchen könnten.

### Warum verwenden wir reCAPTCHA auf unserer Website?

Wir wollen nur Menschen aus Fleisch und Blut auf unserer Seite begrüßen. Bots oder Spam-Software unterschiedlichster Art dürfen getrost zuhause bleiben. Darum setzen wir alle Hebel in Bewegung, uns zu schützen und die bestmögliche Benutzerfreundlichkeit für Sie anzubieten. Aus diesem Grund verwenden wir Google reCAPTCHA der Firma Google. So können wir uns ziemlich sicher sein, dass wir eine „botfreie“ Webseite bleiben. Durch die Verwendung von reCAPTCHA werden Daten an Google übermittelt, um festzustellen, ob Sie auch wirklich ein Mensch sind. reCAPTCHA dient also der Sicherheit unserer Webseite und in weiterer Folge damit auch Ihrer Sicherheit. Zum Beispiel könnte es ohne reCAPTCHA passieren, dass bei einer Registrierung ein Bot möglichst viele E-Mail-Adressen registriert, um im Anschluss Foren oder Blogs mit unerwünschten Werbeinhalten „zuzuspamen“. Mit reCAPTCHA können wir solche Botangriffe vermeiden.

### Welche Daten werden von reCAPTCHA gespeichert?

reCAPTCHA sammelt personenbezogene Daten von Usern, um festzustellen, ob die Handlungen auf unserer Webseite auch wirklich von Menschen stammen. Es kann also die IP-Adresse und andere Daten, die Google für den reCAPTCHA-Dienst benötigt, an Google versendet werden. IP-Adressen werden innerhalb der Mitgliedstaaten der EU oder anderer Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum fast immer zuvor gekürzt, bevor die Daten auf einem Server in den USA landen. Die IP-Adresse wird nicht mit anderen Daten von Google kombiniert, sofern Sie nicht während der Verwendung von reCAPTCHA mit Ihrem Google-Konto angemeldet sind. Zuerst prüft der reCAPTCHA-Algorithmus, ob auf Ihrem Browser schon Google-Cookies von anderen Google-Diensten (YouTube. Gmail usw.) platziert sind. Anschließend setzt reCAPTCHA ein zusätzliches Cookie in Ihrem Browser und erfasst einen Schnappschuss Ihres Browserfensters.

Die folgende Liste von gesammelten Browser- und Userdaten, hat nicht den Anspruch auf Vollständigkeit. Vielmehr sind es Beispiele von Daten, die nach unserer Erkenntnis, von Google verarbeitet werden.

*   Referrer URL (die Adresse der Seite von der der Besucher kommt)
*   IP-Adresse (z.B. 256.123.123.1)
*   Infos über das Betriebssystem (die Software, die den Betrieb Ihres Computers ermöglicht. Bekannte Betriebssysteme sind Windows, Mac OS X oder Linux)
*   Cookies (kleine Textdateien, die Daten in Ihrem Browser speichern)
*   Maus- und Keyboardverhalten (jede Aktion, die Sie mit der Maus oder der Tastatur ausführen wird gespeichert)
*   Datum und Spracheinstellungen (welche Sprache bzw. welches Datum Sie auf Ihrem PC voreingestellt haben wird gespeichert)
*   Alle Javascript-Objekte (JavaScript ist eine Programmiersprache, die Webseiten ermöglicht, sich an den User anzupassen. JavaScript-Objekte können alle möglichen Daten unter einem Namen sammeln)
*   Bildschirmauflösung (zeigt an aus wie vielen Pixeln die Bilddarstellung besteht)

Unumstritten ist, dass Google diese Daten verwendet und analysiert noch bevor Sie auf das Häkchen „Ich bin kein Roboter“ klicken. Bei der Invisible reCAPTCHA-Version fällt sogar das Ankreuzen weg und der ganze Erkennungsprozess läuft im Hintergrund ab. Wie viel und welche Daten Google genau speichert, erfährt man von Google nicht im Detail.

Folgende Cookies werden von reCAPTCHA verwendet: Hierbei beziehen wir uns auf die reCAPTCHA Demo-Version von Google unter [https://www.google.com/recaptcha/api2/demo](https://www.google.com/recaptcha/api2/demo). All diese Cookies benötigen zu Trackingzwecken eine eindeutige Kennung. Hier ist eine Liste an Cookies, die Google reCAPTCHA auf der Demo-Version gesetzt hat:

**Name:** IDE  
**Wert:** WqTUmlnmv\_qXyi\_DGNPLESKnRNrpgXoy1K-pAZtAkMbHI-221148768-8  
**Verwendungszweck:** Dieses Cookie wird von der Firma DoubleClick (gehört auch Google) gesetzt, um die Aktionen eines Users auf der Webseite im Umgang mit Werbeanzeigen zu registrieren und zu melden. So kann die Werbewirksamkeit gemessen und entsprechende Optimierungsmaßnahmen getroffen werden. IDE wird in Browsern unter der Domain doubleclick.net gespeichert.  
**Ablaufdatum:** nach einem Jahr

**Name:** 1P\_JAR  
**Wert:** 2019-5-14-12  
**Verwendungszweck:** Dieses Cookie sammelt Statistiken zur Webseite-Nutzung und misst Conversions. Eine Conversion entsteht z.B., wenn ein User zu einem Käufer wird. Das Cookie wird auch verwendet, um Usern relevante Werbeanzeigen einzublenden. Weiters kann man mit dem Cookie vermeiden, dass ein User dieselbe Anzeige mehr als einmal zu Gesicht bekommt.  
**Ablaufdatum:** nach einem Monat

**Name:** ANID  
**Wert:** U7j1v3dZa2211487680xgZFmiqWppRWKOr  
**Verwendungszweck:** Viele Infos konnten wir über dieses Cookie nicht in Erfahrung bringen. In der Datenschutzerklärung von Google wird das Cookie im Zusammenhang mit „Werbecookies“ wie z. B. “DSID”, “FLC”, “AID”, “TAID” erwähnt. ANID wird unter Domain google.com gespeichert.  
**Ablaufdatum:** nach 9 Monaten

**Name:** CONSENT  
**Wert:** YES+AT.de+20150628-20-0  
**Verwendungszweck:** Das Cookie speichert den Status der Zustimmung eines Users zur Nutzung unterschiedlicher Services von Google. CONSENT dient auch der Sicherheit, um User zu überprüfen, Betrügereien von Anmeldeinformationen zu verhindern und Userdaten vor unbefugten Angriffen zu schützen.  
**Ablaufdatum:** nach 19 Jahren

**Name:** NID  
**Wert:** 0WmuWqy221148768zILzqV\_nmt3sDXwPeM5Q  
**Verwendungszweck:** NID wird von Google verwendet, um Werbeanzeigen an Ihre Google-Suche anzupassen. Mit Hilfe des Cookies „erinnert“ sich Google an Ihre meist eingegebenen Suchanfragen oder Ihre frühere Interaktion mit Anzeigen. So bekommen Sie immer maßgeschneiderte Werbeanzeigen. Das Cookie enthält eine einzigartige ID, um persönliche Einstellungen des Users für Werbezwecke zu sammeln.  
**Ablaufdatum:** nach 6 Monaten

**Name:** DV  
**Wert:** gEAABBCjJMXcI0dSAAAANbqc221148768-4  
**Verwendungszweck:** Sobald Sie das „Ich bin kein Roboter“-Häkchen angekreuzt haben, wird dieses Cookie gesetzt. Das Cookie wird von Google Analytics für personalisierte Werbung verwendet. DV sammelt Informationen in anonymisierter Form und wird weiters benutzt, um User-Unterscheidungen zu treffen.  
**Ablaufdatum:** nach 10 Minuten

**Anmerkung:** Diese Aufzählung kann keinen Anspruch auf Vollständigkeit erheben, da Google erfahrungsgemäß die Wahl ihrer Cookies immer wieder auch verändert.

### Wie lange und wo werden die Daten gespeichert?

Durch das Einfügen von reCAPTCHA werden Daten von Ihnen auf den Google-Server übertragen. Wo genau diese Daten gespeichert werden, stellt Google, selbst nach wiederholtem Nachfragen, nicht klar dar. Ohne eine Bestätigung von Google erhalten zu haben, ist davon auszugehen, dass Daten wie Mausinteraktion, Verweildauer auf der Webseite oder Spracheinstellungen auf den europäischen oder amerikanischen Google-Servern gespeichert werden. Die IP-Adresse, die Ihr Browser an Google übermittelt, wird grundsätzlich nicht mit anderen Google-Daten aus weiteren Google-Diensten zusammengeführt. Wenn Sie allerdings während der Nutzung des reCAPTCHA-Plug-ins bei Ihrem Google-Konto angemeldet sind, werden die Daten zusammengeführt. Dafür gelten die abweichenden Datenschutzbestimmungen der Firma Google.

### Wie kann ich meine Daten löschen bzw. die Datenspeicherung verhindern?

Wenn Sie wollen, dass über Sie und über Ihr Verhalten keine Daten an Google übermittelt werden, müssen Sie sich, bevor Sie unsere Webseite besuchen bzw. die reCAPTCHA-Software verwenden, bei Google vollkommen ausloggen und alle Google-Cookies löschen. Grundsätzlich werden die Daten sobald Sie unsere Seite aufrufen automatisch an Google übermittelt. Um diese Daten wieder zu löschen, müssen Sie den Google-Support auf  [https://support.google.com/?hl=de&tid=221148768](https://support.google.com/?hl=de&tid=221148768) kontaktieren.

Wenn Sie also unsere Webseite verwenden, erklären Sie sich einverstanden, dass Google LLC und deren Vertreter automatisch Daten erheben, bearbeiten und nutzen.

Bitte beachten Sie, dass bei der Verwendung dieses Tools Daten von Ihnen auch außerhalb der EU gespeichert und verarbeitet werden können. Die meisten Drittstaaten (darunter auch die USA) gelten nach derzeitigem europäischen Datenschutzrecht als nicht sicher. Daten an unsichere Drittstaaten dürfen also nicht einfach übertragen, dort gespeichert und verarbeitet werden, sofern es keine passenden Garantien (wie etwa EU-Standardvertragsklauseln) zwischen uns und dem außereuropäischen Dienstleister gibt.

### Rechtsgrundlage

Wenn Sie eingewilligt haben, dass Google reCAPTCHA eingesetzt werden darf, ist die Rechtsgrundlage der entsprechenden Datenverarbeitung diese Einwilligung. Diese Einwilligung stellt laut **Art. 6 Abs. 1 lit. a DSGVO (Einwilligung)** die Rechtsgrundlage für die Verarbeitung personenbezogener Daten, wie sie bei der Erfassung durch Google reCAPTCHA vorkommen kann, dar.

Von unserer Seite besteht zudem ein berechtigtes Interesse, Google reCAPTCHA zu verwenden, um unser Online-Service zu optimieren und sicherer zu machen. Die dafür entsprechende Rechtsgrundlage ist **Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)**.

Etwas mehr über reCAPTCHA erfahren Sie auf der Webentwickler-Seite von Google auf [https://developers.google.com/recaptcha/](https://developers.google.com/recaptcha/). Google geht hier zwar auf die technische Entwicklung der reCAPTCHA näher ein, doch genaue Informationen über Datenspeicherung und datenschutzrelevanten Themen sucht man auch dort vergeblich. Eine gute Übersicht über die grundsätzliche Verwendung von Daten bei Google finden Sie in der hauseigenen Datenschutzerklärung auf [https://www.google.com/intl/de/policies/privacy/](https://policies.google.com/privacy?hl=de&tid=221148768).

Benutzerdefinierte Google Suche Datenschutzerklärung
----------------------------------------------------

**Benutzerdefinierte Google Suche Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Besucher der Website  
🤝 Zweck: Optimierung unserer Serviceleistung  
📓 Verarbeitete Daten: Daten wie etwa IP-Adresse und eingegebene Suchbegriffe werden bei Google gespeichert  
Mehr Details dazu finden Sie weiter unten in dieser Datenschutzerklärung.  
📅 Speicherdauer: die Speicherdauer variiert abhängig von den gespeicherten Daten  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)

### Was ist die benutzerdefinierte Google Suche?

Wir haben auf unserer Website das Google-Plug-in zur benutzerdefinierten Suche eingebunden. Google ist die größte und bekannteste Suchmaschine weltweit und wird von dem US-amerikanische Unternehmen Google Inc. betrieben. Für den europäischen Raum ist das Unternehmen Google Ireland Limited (Gordon House, Barrow Street Dublin 4, Irland) verantwortlich. Durch die benutzerdefinierte Google Suche können Daten von Ihnen an Google übertragen werden. In dieser Datenschutzerklärung informieren wir Sie, warum wir dieses Plug-in verwenden, welche Daten verarbeitet werden und wie Sie diese Datenübertragung verwalten oder unterbinden können.

Das Plug-in zur benutzerdefinierten Google Suche ist eine Google-Suchleiste direkt auf unserer Website. Die Suche findet wie auf [www.google.com](https://www.google.com?tid=221148768) statt, nur fokussieren sich die Suchergebnisse auf unsere Inhalte und Produkte bzw. auf einen eingeschränkten Suchkreis.

### Warum verwenden wir die benutzerdefinierte Google Suche auf unserer Website?

Eine Website mit vielen interessanten Inhalten wird oft so groß, dass man unter Umständen den Überblick verliert. Über die Zeit hat sich auch bei uns viel wertvolles Material angesammelt und wir wollen als Teil unserer Dienstleistung, dass Sie unsere Inhalte so schnell und einfach wie möglich finden. Durch die benutzerdefinierte Google-Suche wird das Finden von interessanten Inhalten zu einem Kinderspiel. Das eingebaute Google-Plug-in verbessert insgesamt die Qualität unserer Website und macht Ihnen das Suchen leichter.

### Welche Daten werden durch die benutzerdefinierte Google Suche gespeichert?

Durch die benutzerdefinierte Google-Suche werden nur Daten von Ihnen an Google übertragen, wenn Sie die auf unserer Website eingebaute Google-Suche aktiv verwenden. Das heißt, erst wenn Sie einen Suchbegriff in die Suchleiste eingeben und dann diesen Begriff bestätigen (z.B. auf „Enter“ klicken) wird neben dem Suchbegriff auch Ihre IP-Adresse an Google gesandt, gespeichert und dort verarbeitet. Anhand der gesetzten Cookies (wie z.B. 1P\_JAR) ist davon auszugehen, dass Google auch Daten zur Webseiten-Nutzung erhält. Wenn Sie während Ihrem Besuch auf unserer Webseite, über die eingebaute Google-Suchfunktion, Inhalte suchen und gleichzeitig mit Ihrem Google-Konto angemeldet sind, kann Google die erhobenen Daten auch Ihrem Google-Konto zuordnen. Als Websitebetreiber haben wir keinen Einfluss darauf, was Google mit den erhobenen Daten macht bzw. wie Google die Daten verarbeitet.

Folgende Cookie werden in Ihrem Browser gesetzt, wenn Sie die benutzerdefinierte Google Suche verwenden und nicht mit einem Google-Konto angemeldet sind:

**Name:** 1P\_JAR  
**Wert:** 2020-01-27-13221148768-5  
**Verwendungszweck:** Dieses Cookie sammelt Statistiken zur Website-Nutzung und misst Conversions. Eine Conversion entsteht zum Beispiel, wenn ein User zu einem Käufer wird. Das Cookie wird auch verwendet, um Usern relevante Werbeanzeigen einzublenden.  
**Ablaufdatum:** nach einem Monat

**Name:** CONSENT  
**Wert:** WP.282f52221148768-9  
**Verwendungszweck:** Das Cookie speichert den Status der Zustimmung eines Users zur Nutzung unterschiedlicher Services von Google. CONSENT dient auch der Sicherheit, um User zu überprüfen und Userdaten vor unbefugten Angriffen zu schützen.  
**Ablaufdatum:** nach 18 Jahren

**Name:** NID  
**Wert:** 196=pwIo3B5fHr-8  
**Verwendungszweck:** NID wird von Google verwendet, um Werbeanzeigen an Ihre Google-Suche anzupassen. Mit Hilfe des Cookies „erinnert“ sich Google an Ihre eingegebenen Suchanfragen oder Ihre frühere Interaktion mit Anzeigen. So bekommen Sie immer maßgeschneiderte Werbeanzeigen.  
**Ablaufdatum:** nach 6 Monaten

**Anmerkung:** Diese Aufzählung kann keinen Anspruch auf Vollständigkeit erheben, da Google die Wahl ihrer Cookies immer wieder auch verändert.

### Wie lange und wo werden die Daten gespeichert?

Die Google-Server sind auf der ganzen Welt verteilt. Da es sich bei Google um ein amerikanisches Unternehmen handelt, werden die meisten Daten auf amerikanischen Servern gespeichert. Unter [https://www.google.com/about/datacenters/inside/locations/?hl=de](https://www.google.com/about/datacenters/inside/locations/?hl=de) sehen Sie genau, wo die Google-Server stehen.  
Ihre Daten werden auf verschiedenen physischen Datenträgern verteilt. Dadurch sind die Daten schneller abrufbar und vor möglichen Manipulationen besser geschützt. Google hat auch entsprechende Notfallprogramme für Ihre Daten. Wenn es beispielsweise bei Google interne technische Probleme gibt und dadurch Server nicht mehr funktionieren, bleibt das Risiko einer Dienstunterbrechung und eines Datenverlusts dennoch gering.  
Je nach dem um welche Daten es sich handelt, speichert Google diese unterschiedlich lange. Manche Daten können Sie selbst löschen, andere werden von Google automatisch gelöscht oder anonymisiert. Es gibt aber auch Daten, die Google länger speichert, wenn dies aus juristischen oder geschäftlichen Gründen erforderlich ist.

### Wie kann ich meinen Daten löschen bzw. die Datenspeicherung verhindern?

Nach dem Datenschutzrecht der Europäischen Union haben Sie das Recht, Auskunft über Ihre Daten zu erhalten, sie zu aktualisieren, zu löschen oder einzuschränken. Es gibt einige Daten, die Sie jederzeit löschen können. Wenn Sie ein Google-Konto besitzen, können Sie dort Daten zu Ihrer Webaktivität löschen bzw. festlegen, dass sie nach einer bestimmten Zeit gelöscht werden sollen.  
In Ihrem Browser haben Sie zudem die Möglichkeit, Cookies zu deaktivieren, zu löschen oder nach Ihren Wünschen und Vorlieben zu verwalten. Hier finden Sie Anleitungen zu den wichtigsten Browsern:

[Chrome: Cookies in Chrome löschen, aktivieren und verwalten](https://support.google.com/chrome/answer/95647?tid=221148768)

[Safari: Verwalten von Cookies und Websitedaten mit Safari](https://support.apple.com/de-at/guide/safari/sfri11471/mac?tid=221148768)

[Firefox: Cookies löschen, um Daten zu entfernen, die Websites auf Ihrem Computer abgelegt haben](https://support.mozilla.org/de/kb/cookies-und-website-daten-in-firefox-loschen?tid=221148768)

[Internet Explorer: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/17442/windows-internet-explorer-delete-manage-cookies?tid=221148768)

[Microsoft Edge: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/4027947/windows-delete-cookies?tid=221148768)

Bitte beachten Sie, dass bei der Verwendung dieses Tools Daten von Ihnen auch außerhalb der EU gespeichert und verarbeitet werden können. Die meisten Drittstaaten (darunter auch die USA) gelten nach derzeitigem europäischen Datenschutzrecht als nicht sicher. Daten an unsichere Drittstaaten dürfen also nicht einfach übertragen, dort gespeichert und verarbeitet werden, sofern es keine passenden Garantien (wie etwa EU-Standardvertragsklauseln) zwischen uns und dem außereuropäischen Dienstleister gibt.

### Rechtsgrundlage

Wenn Sie eingewilligt haben, dass die benutzerdefinierte Google Suche eingesetzt werden darf, ist die Rechtsgrundlage der entsprechenden Datenverarbeitung diese Einwilligung. Diese Einwilligung stellt laut **Art. 6 Abs. 1 lit. a DSGVO (Einwilligung)** die Rechtsgrundlage für die Verarbeitung personenbezogener Daten, wie sie bei der Erfassung durch die benutzerdefinierte Google Suche vorkommen kann, dar.

Von unserer Seite besteht zudem ein berechtigtes Interesse, die benutzerdefinierte Google Suche zu verwenden, um unser Online-Service zu optimieren. Die dafür entsprechende Rechtsgrundlage ist **Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)**.

Wir hoffen wir konnten Ihnen die wichtigsten Informationen rund um die Datenverarbeitung durch Google näherbringen. Wenn Sie mehr darüber erfahren wollen, empfehlen wir die umfangreiche Datenschutzerklärung von Google unter [https://policies.google.com/privacy?hl=de](https://policies.google.com/privacy?hl=de).

Cookie Consent Management Platform
----------------------------------

**Cookie Consent Management Platform Zusammenfassung**  
👥 Betroffene: Website Besucher  
🤝 Zweck: Einholung und Verwaltung der Zustimmung zu bestimmten Cookies und somit dem Einsatz bestimmter Tools  
📓 Verarbeitete Daten:  Daten zur Verwaltung der eingestellten Cookie-Einstellungen wie IP-Adresse, Zeitpunkt der Zustimmung, Art der Zustimmung, einzelne Zustimmungen. Mehr Details dazu finden Sie beim jeweils eingesetzten Tool.  
📅 Speicherdauer: Hängt vom eingesetzten Tool ab, man muss sich auf Zeiträume von mehreren Jahren einstellen  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit.f DSGVO (berechtigte Interessen)

### Was ist eine Cookie Consent Manangement Platform?

Wir verwenden auf unserer Website eine Consent Management Platform (CMP) Software, die uns und Ihnen den korrekten und sicheren Umgang mit verwendeten Skripten und Cookies erleichtert. Die Software erstellt automatisch ein Cookie-Popup, scannt und kontrolliert alle Skripts und Cookies, bietet eine datenschutzrechtlich notwendige Cookie-Einwilligung für Sie und hilft uns und Ihnen den Überblick über alle Cookies zu behalten. Bei den meisten Cookie Consent Management Tools werden alle vorhandenen Cookies identifiziert und kategorisiert. Sie als Websitebesucher entscheiden dann selbst, ob und welche Skripte und Cookies Sie zulassen oder nicht zulassen. Die folgende Grafik stellt die Beziehung zwischen Browser, Webserver und CMP dar.

![Consent Management Platform Überblick](https://www.adsimple.at/wp-content/uploads/2021/03/consent-management-platform-overview.svg)

### Warum verwenden wir ein Cookie-Management-Tool?

Unser Ziel ist es, Ihnen im Bereich Datenschutz die bestmögliche Transparenz zu bieten. Zudem sind wir dazu auch rechtlich verpflichtet. Wir wollen Sie über alle Tools und alle Cookies, die Daten von Ihnen speichern und verarbeiten können, so gut wie möglich aufklären. Es ist auch Ihr Recht, selbst zu entscheiden, welche Cookies Sie akzeptieren und welche nicht. Um Ihnen dieses Recht einzuräumen, müssen wir zuerst genau wissen, welche Cookies überhaupt auf unserer Website gelandet sind. Dank eines Cookie-Management-Tools, welches die Website regelmäßig nach allen vorhandenen Cookies scannt, wissen wir über alle Cookies Bescheid und können Ihnen DSGVO-konform Auskunft darüber geben. Über das Einwilligungssystem können Sie dann Cookies akzeptieren oder ablehnen.

### Welche Daten werden verarbeitet?

Im Rahmen unseres Cookie-Management-Tools können Sie jedes einzelnen Cookies selbst verwalten und haben die vollständige Kontrolle über die Speicherung und Verarbeitung Ihrer Daten. Die Erklärung Ihrer Einwilligung wird gespeichert, damit wir Sie nicht bei jedem neuen Besuch unserer Website abfragen müssen und wir Ihre Einwilligung, wenn gesetzlich nötig, auch nachweisen können. Gespeichert wird dies entweder in einem Opt-in-Cookie oder auf einem Server. Je nach Anbieter des Cookie-Management-Tools variiert Speicherdauer Ihrer Cookie-Einwilligung. Meist werden diese Daten (etwa pseudonyme User-ID, Einwilligungs-Zeitpunkt, Detailangaben zu den Cookie-Kategorien oder Tools, Browser, Gerätinformationen) bis zu zwei Jahren gespeichert.

### Dauer der Datenverarbeitung

Über die Dauer der Datenverarbeitung informieren wir Sie weiter unten, sofern wir weitere Informationen dazu haben. Generell verarbeiten wir personenbezogene Daten nur so lange wie es für die Bereitstellung unserer Dienstleistungen und Produkte unbedingt notwendig ist. Daten, die in Cookies gespeichert werden, werden unterschiedlich lange gespeichert. Manche Cookies werden bereits nach dem Verlassen der Website wieder gelöscht, anderen können über einige Jahre in Ihrem Browser gespeichert sein. Die genaue Dauer der Datenverarbeitung hängt vom verwendeten Tool ab, meistens sollten Sie sich auf eine Speicherdauer von mehreren Jahren einstellen. In den jeweiligen Datenschutzerklärungen der einzelnen Anbieter erhalten Sie in der Regel genaue Informationen über die Dauer der Datenverarbeitung.

### Widerspruchsrecht

Sie haben auch jederzeit das Recht und die Möglichkeit Ihre Einwilligung zur Verwendung von Cookies zu widerrufen. Das funktioniert entweder über unser Cookie-Management-Tool oder über andere Opt-Out-Funktionen. Zum Bespiel können Sie auch die Datenerfassung durch Cookies verhindern, indem Sie in Ihrem Browser die Cookies verwalten, deaktivieren oder löschen.

Informationen zu speziellen Cookie-Management-Tools, erfahren Sie – sofern vorhanden – in den folgenden Abschnitten.

### Rechtsgrundlage

Wenn Sie Cookies zustimmen, werden über diese Cookies personenbezogene Daten von Ihnen verarbeitet und gespeichert. Falls wir durch Ihre **Einwilligung** (Artikel 6 Abs. 1 lit. a DSGVO) Cookies verwenden dürfen, ist diese Einwilligung auch gleichzeitig die Rechtsgrundlage für die Verwendung von Cookies bzw. die Verarbeitung Ihrer Daten. Um die Einwilligung zu Cookies verwalten zu können und Ihnen die Einwilligung ermöglichen zu können, kommt eine Cookie-Consent-Management-Platform-Software zum Einsatz. Der Einsatz dieser Software ermöglicht uns, die Website auf effiziente Weise rechtskonform zu betreiben, was ein **berechtigtes Interesse** (Artikel 6 Abs. 1 lit. f DSGVO) darstellt.

Social Media
------------

**Social Media Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Besucher der Website  
🤝 Zweck: Darstellung und Optimierung unserer Serviceleistung, Kontakt zu Besuchern, Interessenten u.a., Werbung  
📓 Verarbeitete Daten: Daten wie etwa Telefonnummern, E-Mail-Adressen, Kontaktdaten, Daten zum Nutzerverhalten, Informationen zu Ihrem Gerät und Ihre IP-Adresse.  
Mehr Details dazu finden Sie beim jeweils eingesetzten Social-Media-Tool.  
📅 Speicherdauer: abhängig von den verwendeten Social-Media-Plattformen  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit. f DSGVO (Berechtigte Interessen)

### Was ist Social Media?

Zusätzlich zu unserer Website sind wir auch in diversen Social-Media-Plattformen aktiv. Dabei können Daten von Usern verarbeitet werden, damit wir gezielt User, die sich für uns interessieren, über die sozialen Netzwerke ansprechen können. Darüber hinaus können auch Elemente einer Social-Media-Plattform direkt in unsere Website eingebettet sein. Das ist etwa der Fall, wenn Sie einen sogenannten Social-Button auf unserer Website anklicken und direkt zu unserem Social-Media-Auftritt weitergeleitet werden. Als sogenannte Sozialen Medien oder Social Media werden Websites und Apps bezeichnet, über die angemeldete Mitglieder Inhalte produzieren, Inhalte offen oder in bestimmten Gruppen austauschen und sich mit anderen Mitgliedern vernetzen können.

### Warum nutzen wir Social Media?

Seit Jahren sind Social-Media-Plattformen der Ort, wo Menschen online kommunizieren und in Kontakt treten. Mit unseren Social-Media-Auftritten können wir unsere Produkte und Dienstleistungen Interessenten näherbringen. Die auf unserer Website eingebundenen Social-Media-Elemente helfen Ihnen, schnell und ohne Komplikationen zu unseren Social-Media-Inhalten wechseln können.

Die Daten, die durch Ihre Nutzung eines Social-Media-Kanals gespeichert und verarbeitet werden, haben in erster Linie den Zweck, Webanalysen durchführen zu können. Ziel dieser Analysen ist es, genauere und personenbezogene Marketing- und Werbestrategien entwickeln zu können. Abhängig von Ihrem Verhalten auf einer Social-Media-Plattform, können mit Hilfe der ausgewerteten Daten, passende Rückschlüsse auf Ihre Interessen getroffen werden und sogenannte Userprofile erstellt werden. So ist es den Plattformen auch möglich, Ihnen maßgeschneiderte Werbeanzeigen zu präsentieren. Meistens werden für diesen Zweck Cookies in Ihrem Browser gesetzt, die Daten zu Ihrem Nutzungsverhalten speichern.

In der Regel sind wir und der Anbieter der Social-Media-Plattform sogenannte gemeinsame Verarbeiter im Sinne des Art. 26 DSGVO und arbeiten auf Grundlage einer diesbezüglichen Vereinbarung. Das Wesentliche der Vereinbarung ist weiter unten bei der betroffenen Plattform wiedergegeben.

Bitte beachten Sie, dass bei der Nutzung der Social-Media-Plattformen oder unserer eingebauten Elemente auch Daten von Ihnen außerhalb der Europäischen Union verarbeitet werden können, da viele Social-Media-Kanäle, beispielsweise Facebook oder Twitter, amerikanische Unternehmen sind. Dadurch können Sie möglicherweise Ihre Rechte in Bezug auf Ihre personenbezogenen Daten nicht mehr so leicht einfordern bzw. durchsetzen.

### Welche Daten werden verarbeitet?

Welche Daten genau gespeichert und verarbeitet werden, hängt vom jeweiligen Anbieter der Social-Media-Plattform ab. Aber für gewöhnlich handelt es sich um Daten wie etwa Telefonnummern, E-Mailadressen, Daten, die Sie in ein Kontaktformular eingeben, Nutzerdaten wie zum Beispiel welche Buttons Sie klicken, wen Sie liken oder wem folgen, wann Sie welche Seiten besucht haben, Informationen zu Ihrem Gerät und Ihre IP-Adresse. Die meisten dieser Daten werden in Cookies gespeichert. Speziell wenn Sie selbst ein Profil bei dem besuchten Social-Media-Kanal haben und angemeldet sind, können Daten mit Ihrem Profil verknüpft werden.

Alle Daten, die über eine Social-Media-Plattform erhoben werden, werden auch auf den Servern der Anbieter gespeichert. Somit haben auch nur die Anbieter Zugang zu den Daten und können Ihnen die passenden Auskünfte geben bzw. Änderungen vornehmen.

Wenn Sie genau wissen wollen, welche Daten bei den Social-Media-Anbietern gespeichert und verarbeitet werden und wie sie der Datenverarbeitung widersprechen können, sollten Sie die jeweilige Datenschutzerklärung des Unternehmens sorgfältig durchlesen. Auch wenn Sie zur Datenspeicherung und Datenverarbeitung Fragen haben oder entsprechende Rechte geltend machen wollen, empfehlen wir Ihnen, sich direkt an den Anbieter wenden.

### Dauer der Datenverarbeitung

Über die Dauer der Datenverarbeitung informieren wir Sie weiter unten, sofern wir weitere Informationen dazu haben. Beispielsweise speichert die Social-Media-Plattform Facebook Daten, bis sie für den eigenen Zweck nicht mehr benötigt werden. Kundendaten, die mit den eigenen Userdaten abgeglichen werden, werden aber schon innerhalb von zwei Tagen gelöscht. Generell verarbeiten wir personenbezogene Daten nur so lange wie es für die Bereitstellung unserer Dienstleistungen und Produkte unbedingt notwendig ist. Wenn es, wie zum Beispiel im Fall von Buchhaltung, gesetzlich vorgeschrieben ist, kann diese Speicherdauer auch überschritten werden.

### Widerspruchsrecht

Sie haben auch jederzeit das Recht und die Möglichkeit Ihre Einwilligung zur Verwendung von Cookies bzw. Drittanbietern wie eingebettete Social-Media-Elemente zu widerrufen. Das funktioniert entweder über unser Cookie-Management-Tool oder über andere Opt-Out-Funktionen. Zum Bespiel können Sie auch die Datenerfassung durch Cookies verhindern, indem Sie in Ihrem Browser die Cookies verwalten, deaktivieren oder löschen.

Da bei Social-Media-Tools Cookies zum Einsatz kommen können, empfehlen wir Ihnen auch unsere allgemeine Datenschutzerklärung über Cookies. Um zu erfahren, welche Daten von Ihnen genau gespeichert und verarbeitet werden, sollten Sie die Datenschutzerklärungen der jeweiligen Tools durchlesen.

### Rechtsgrundlage

Wenn Sie eingewilligt haben, dass Daten von Ihnen durch eingebundene Social-Media-Elemente verarbeitet und gespeichert werden können, gilt diese Einwilligung als Rechtsgrundlage der Datenverarbeitung **(Art. 6 Abs. 1 lit. a DSGVO)**. Grundsätzlich werden Ihre Daten bei Vorliegen einer Einwilligung auch auf Grundlage unseres berechtigten Interesses **(Art. 6 Abs. 1 lit. f DSGVO)** an einer schnellen und guten Kommunikation mit Ihnen oder anderen Kunden und Geschäftspartnern gespeichert und verarbeitet. Die meisten Social-Media-Plattformen setzen auch Cookies in Ihrem Browser, um Daten zu speichern. Darum empfehlen wir Ihnen, unseren Datenschutztext über Cookies genau durchzulesen und die Datenschutzerklärung oder die Cookie-Richtlinien des jeweiligen Dienstanbieters anzusehen.

Informationen zu speziellen Social-Media-Plattformen erfahren Sie – sofern vorhanden – in den folgenden Abschnitten.

AdSimple Cookie Manager Datenschutzerklärung
--------------------------------------------

**AdSimple Cookie Manager Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Website Besucher  
🤝 Zweck: Einholung der Zustimmung zu bestimmten Cookies und somit dem Einsatz bestimmter Tools  
📓 Verarbeitete Daten: Daten zur Verwaltung der eingestellten Cookie-Einstellungen wie IP-Adresse, Zeitpunkt der Zustimmung, Art der Zustimmung, einzelne Zustimmungen. Mehr Details dazu finden Sie weiter unter in dieser Datenschutzerklärung  
📅 Speicherdauer: das verwendete Cookie läuft nach einem Jahr ab  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit.f DSGVO (berechtigte Interessen)

### Was ist der AdSimple Cookie Manager?

Wir verwenden auf unserer Website den AdSimple Cookie Manager des Softwareentwicklungs- und Online-Marketing Unternehmens AdSimple GmbH, Fabriksgasse 20, 2230 Gänserndorf. Der AdSimple Cookie Manager bietet uns unter anderem die Möglichkeit, Ihnen einen umfangreichen und datenschutzkonformen Cookie-Hinweis zu liefern, damit Sie selbst entscheiden können, welche Cookies Sie zulassen und welche nicht. Durch die Verwendung dieser Software werden Daten von Ihnen an AdSimple gesendet und gespeichert. In dieser Datenschutzerklärung informieren wir Sie, warum wir den AdSimple Cookie Manager verwenden, welche Daten übertragen und gespeichert werden und wie Sie diese Datenübertragung verhindern können.

Der AdSimple Cookie Manager ist eine Software, die unsere Website scannt und alle vorhandenen Cookies identifiziert und kategorisiert. Zudem werden Sie als Websitebesucher über ein Cookie Hinweis Script über die Verwendung von Cookies informiert und entscheiden selbst welche Cookies Sie zulassen und welche nicht.

### Warum verwenden wir den AdSimple Cookie Manager auf unserer Website?

Wir wollen Ihnen maximale Transparenz im Bereich Datenschutz bieten. Um das zu gewährleisten, müssen wir zuerst genau wissen, welche Cookies im Laufe der Zeit auf unserer Website gelandet sind. Dadurch, dass der Cookie Manager von AdSimple regelmäßig unsere Website scannt und alle Cookies ausfindig macht, haben wir die volle Kontrolle über diese Cookies und können so DSGVO-konform handeln. Wir können Sie dadurch über die Nutzung der Cookies auf unserer Website genau informieren. Weiters bekommen Sie stets einen aktuellen und datenschutzkonformen Cookie-Hinweis und entscheiden per Checkbox-System selbst, welche Cookies Sie akzeptieren bzw. blockieren.

### Welche Daten werden von dem AdSimple Cookie Manager gespeichert?

Wenn Sie Cookies auf unserer Website zustimmen, wird folgendes Cookie von dem AdSimple Cookie Manager gesetzt:

**Name:** acm\_status  
**Wert:** “:true,”statistik”:true,”marketing”:true,”socialmedia”:true,”einstellungen”:true}  
**Verwendungszweck:** In diesem Cookie wird Ihr Zustimmungsstatus, gespeichert. Dadurch kann unsere Website auch bei zukünftigen Besuchen den aktuellen Status lesen und befolgen.  
**Ablaufdatum:** nach einem Jahr

### Wie lange und wo werden die Daten gespeichert?

Alle Daten, die durch den AdSimple Cookie Manager erhoben werden, werden ausschließlich innerhalb der Europäischen Union übertragen und gespeichert. Die erhobenen Daten werden auf den Servern von AdSimple bei der Hetzner GmbH in Deutschland gespeichert. Zugriff auf diese Daten hat ausschließlich die AdSimple GmbH und die Hetzner GmbH.

### Wie kann ich meine Daten löschen bzw. die Datenspeicherung verhindern?

Sie haben jederzeit das Recht auf Ihre personenbezogenen Daten zuzugreifen und sie auch zu löschen. Die Datenerfassung und Speicherung können Sie beispielsweise verhindern, indem Sie über das Cookie-Hinweis-Script die Verwendung von Cookies ablehnen. Eine weitere Möglichkeit die Datenverarbeitung zu unterbinden bzw. nach Ihren Wünschen zu verwalten, bietet Ihr Browser. Je nach Browser funktioniert die Cookie-Verwaltung etwas anders. Hier finden Sie die Anleitungen zu den momentan bekanntesten Browsern:

[Chrome: Cookies in Chrome löschen, aktivieren und verwalten](https://support.google.com/chrome/answer/95647?tid=221148768)

[Safari: Verwalten von Cookies und Websitedaten mit Safari](https://support.apple.com/de-at/guide/safari/sfri11471/mac?tid=221148768)

[Firefox: Cookies löschen, um Daten zu entfernen, die Websites auf Ihrem Computer abgelegt haben](https://support.mozilla.org/de/kb/cookies-und-website-daten-in-firefox-loschen?tid=221148768)

[Internet Explorer: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/17442/windows-internet-explorer-delete-manage-cookies?tid=221148768)

[Microsoft Edge: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/4027947/windows-delete-cookies?tid=221148768)

### Rechtsgrundlage

Wenn Sie Cookies zustimmen, werden über diese Cookies personenbezogene Daten von Ihnen verarbeitet und gespeichert. Falls wir durch Ihre **Einwilligung** (Artikel 6 Abs. 1 lit. a DSGVO) Cookies verwenden dürfen, ist diese Einwilligung auch gleichzeitig die Rechtsgrundlage für die Verwendung von Cookies bzw. die Verarbeitung Ihrer Daten. Um die Einwilligung zu Cookies verwalten zu können und Ihnen die Einwilligung ermöglichen zu können, kommt der AdSimple Cookie Manager zum Einsatz. Der Einsatz dieser Software ermöglicht uns, die Website auf effiziente Weise rechtskonform zu betreiben, was ein **berechtigtes Interesse** (Artikel 6 Abs. 1 lit. f DSGVO) darstellt.

Wir hoffen, wir haben Ihnen einen guten Überblick über den Datenverkehr und die Datenverarbeitung durch den AdSimple Cookie Manager geliefert. Wenn Sie mehr über dieses Tool erfahren wollen, empfehlen wir Ihnen die Beschreibungsseite auf [https://www.adsimple.at/adsimple-cookie-manager/.](https://www.adsimple.at/adsimple-cookie-manager/?tid=221148768)

Cookiebot Datenschutzerklärung
------------------------------

**Cookiebot Datenschutzerklärung Zusammenfassung**  
👥 Betroffene: Website Besucher  
🤝 Zweck: Einholung der Zustimmung zu bestimmten Cookies und somit dem Einsatz bestimmter Tools  
📓 Verarbeitete Daten:  Daten zur Verwaltung der eingestellten Cookie-Einstellungen wie IP-Adresse, Zeitpunkt der Zustimmung, Art der Zustimmung, einzelne Zustimmungen. Mehr Details dazu finden Sie beim jeweils eingesetzten Tool.  
📅 Speicherdauer: die Daten werden nach einem Jahr gelöscht  
⚖️ Rechtsgrundlagen: Art. 6 Abs. 1 lit. a DSGVO (Einwilligung), Art. 6 Abs. 1 lit.f DSGVO (berechtigte Interessen)

### Was ist Cookiebot?

Wir verwenden auf unserer Website Funktionen des Anbieters Cookiebot. Hinter Cookiebot steht das Unternehmen Cybot A/S, Havnegade 39, 1058 Kopenhagen, DK. Cookiebot bietet uns unter anderem die Möglichkeit, Ihnen einen umfangreichen Cookie-Hinweis (auch Cookie-Banner oder Cookie-Notice genannt) zu liefern. Durch die Verwendung dieser Funktion können Daten von Ihnen an Cookiebot bzw. Cybot gesendet, gespeichert und verarbeitet werden. In dieser Datenschutzerklärung informieren wir Sie warum wir Cookiebot nutzen, welche Daten übertragen werden und wie Sie diese Datenübertragung verhindern können.

Cookiebot ist ein Software-Produkt des Unternehmens Cybot. Die Software erstellt automatisch einen DSGVO-konformen Cookie-Hinweis für unserer Websitebesucher. Zudem scannt, kontrolliert und wertet die Technologie hinter Cookiebot alle Cookies und Tracking-Maßnahmen auf unserer Website.

### Warum verwenden wir Cookiebot auf unserer Website?

Datenschutz nehmen wir sehr ernst. Wir wollen Ihnen ganz genau zeigen, was auf unserer Website vor sich geht und welche Ihrer Daten gespeichert werden. Cookiebot hilft uns einen guten Überblick über all unsere Cookies (Erst- und Drittanbieter-Cookies) zu erhalten. So können wir Sie über die Nutzung von Cookies auf unserer Website exakt und transparent informieren. Sie bekommen stets einen aktuellen und datenschutzkonformen Cookie-Hinweis und entscheiden selbst, welche Cookies Sie zulassen und welche nicht.

### Welche Daten werden von Cookiebot gespeichert?

Wenn Sie Cookies zulassen, werden folgende Daten an Cybot übertragen, gespeichert und verarbeitet.

*   IP-Adresse (in anonymisierter Form, die letzten 3 Ziffern werden auf 0 gesetzt)
*   Datum und Uhrzeit Ihres Einverständnisses
*   unsere Website-URL
*   technische Browserdaten
*   verschlüsselter, anonymer Key
*   die Cookies, die Sie zugelassen haben (als Zustimmungsnachweis)

Folgenden Cookies werden von Cookiebot gesetzt, wenn Sie der Verwendung von Cookies zugestimmt haben:

**Name:** CookieConsent  
**Wert:** {stamp:’P7to4eNgIHvJvDerjKneBsmJQd9221148768-2  
**Verwendungszweck:** In diesem Cookie wird Ihr Zustimmungsstatus, gespeichert. Dadurch kann unsere Website auch bei zukünftigen Besuchen den aktuellen Status lesen und befolgen.  
**Ablaufdatum:** nach einem Jahr

**Name:** CookieConsentBulkTicket  
**Wert:** kDSPWpA%2fjhljZKClPqsncfR8SveTnNWhys5NojaxdFYBPjZ2PaDnUw%3d%3221148768-6  
**Verwendungszweck:** Dieses Cookie wird gesetzt, wenn Sie alle Cookies erlauben und somit eine “Sammelzustimmung” aktiviert haben. Das Cookie speichert dann eine eigene, zufällige und eindeutige ID.  
**Ablaufdatum:** nach einem Jahr

**Anmerkung:** Bitte bedenken Sie, dass es sich hier um eine beispielhafte Liste handelt und wir keinen Anspruch auf Vollständigkeit erheben können. In der Cookie-Erklärung unter [https://www.cookiebot.com/de/cookie-declaration/](https://www.cookiebot.com/de/cookie-declaration/) sehen Sie, welche weiteren Cookies eingesetzt werden können.

Laut der Datenschutzerklärung von Cybot verkauft das Unternehmen personenbezogene Daten nicht weiter. Cybot gibt allerdings Daten an vertrauensvolle Dritt- oder Subunternehmen weiter, die dem Unternehmen helfen, die eigenen betriebswirtschaftlichen Ziele zu erreichen. Daten werden auch dann weitergegeben, wenn dies rechtlich erforderlich ist.

### Wie lange und wo werden die Daten gespeichert?

Alle erhobenen Daten werden ausschließlich innerhalb der Europäischen Union übertragen, gespeichert und weitergeleitet. Die Daten werden in einem Azure-Rechenzentrum (Cloud-Anbieter ist Microsoft) gespeichert. Auf  [https://azure.microsoft.com/de-de/global-infrastructure/regions/](https://azure.microsoft.com/de-de/global-infrastructure/regions/?tid=221148768) erfahren Sie mehr über alle „Azure-Regionen“. Alle User Daten werden von Cookiebot nach 12 Monaten ab der Registrierung (Cookie-Zustimmung) bzw. unmittelbar nach Kündigung des Cookiebot-Services gelöscht.

### Wie kann ich meine Daten löschen bzw. die Datenspeicherung verhindern?

Sie haben jederzeit das Recht auf Ihre personenbezogenen Daten zuzugreifen und sie auch zu löschen. Die Datenerfassung und Speicherung können Sie beispielsweise verhindern, indem Sie über den Cookie-Hinweis die Verwendung von Cookies ablehnen. Eine weitere Möglichkeit die Datenverarbeitung zu unterbinden bzw. nach Ihren Wünschen zu verwalten, bietet Ihr Browser. Je nach Browser funktioniert die Cookie-Verwaltung etwas anders. Hier finden Sie die Anleitungen zu den momentan bekanntesten Browsern:

[Chrome: Cookies in Chrome löschen, aktivieren und verwalten](https://support.google.com/chrome/answer/95647?tid=221148768)

[Safari: Verwalten von Cookies und Websitedaten mit Safari](https://support.apple.com/de-at/guide/safari/sfri11471/mac?tid=221148768)

[Firefox: Cookies löschen, um Daten zu entfernen, die Websites auf Ihrem Computer abgelegt haben](https://support.mozilla.org/de/kb/cookies-und-website-daten-in-firefox-loschen?tid=221148768)

[Internet Explorer: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/17442/windows-internet-explorer-delete-manage-cookies?tid=221148768)

[Microsoft Edge: Löschen und Verwalten von Cookies](https://support.microsoft.com/de-at/help/4027947/windows-delete-cookies?tid=221148768)

### Rechtsgrundlage

Wenn Sie Cookies zustimmen, werden über diese Cookies personenbezogene Daten von Ihnen verarbeitet und gespeichert. Falls wir durch Ihre **Einwilligung** (Artikel 6 Abs. 1 lit. a DSGVO) Cookies verwenden dürfen, ist diese Einwilligung auch gleichzeitig die Rechtsgrundlage für die Verwendung von Cookies bzw. die Verarbeitung Ihrer Daten. Um die Einwilligung zu Cookies verwalten zu können und Ihnen die Einwilligung ermöglichen zu können, kommt der Cookiebot zum Einsatz. Der Einsatz dieser Software ermöglicht uns, die Website auf effiziente Weise rechtskonform zu betreiben, was ein **berechtigtes Interesse** (Artikel 6 Abs. 1 lit. f DSGVO) darstellt.

Wenn Sie mehr über die Datenschutzrichtlinien von „Cookiebot“ bzw. dem dahinterstehenden Unternehmen Cybot erfahren wollen, empfehlen wir Ihnen die Datenschutzrichtlinien unter [https://www.cookiebot.com/de/privacy-policy/](https://www.cookiebot.com/de/privacy-policy/?tid=221148768) durchzulesen.

Facebook Soziale Plug-ins Datenschutzerklärung
----------------------------------------------

Auf unserer Webseite sind sogenannte soziale Plug-ins des Unternehmens Facebook Inc. eingebaut. Sie erkennen diese Buttons am klassischen Facebook-Logo, wie dem „Gefällt mir“-Button (die Hand mit erhobenem Daumen) oder an einer eindeutigen „Facebook Plug-in“-Kennzeichnung. Ein soziales Plug-in ist ein kleiner Teil von Facebook, der in unsere Seite integriert ist. Jedes Plug-in hat eine eigene Funktion. Die am meisten verwendeten Funktionen sind die bekannten “Gefällt mir”- und “Teilen”-Buttons.

Folgende soziale Plug-ins werden von Facebook angeboten:

*   “Speichern”-Button
*   “Gefällt mir”-Button, Teilen, Senden und Zitat
*   Seiten-Plug-in
*   Kommentare
*   Messenger-Plug-in
*   Eingebettete Beiträge und Videoplayer
*   Gruppen-Plug-in

Auf [https://developers.facebook.com/docs/plugins](https://developers.facebook.com/docs/plugins) erhalten Sie nähere Informationen, wie die einzelnen Plug-ins verwendet werden. Wir nützen die sozialen Plug-ins einerseits, um Ihnen ein besseres Usererlebnis auf unserer Seite zu bieten, andererseits weil Facebook dadurch unsere Werbeanzeigen optimieren kann.

Sofern Sie ein Facebook-Konto haben oder [facebook.com](https://www.facebook.com/) schon mal besucht haben, hat Facebook bereits mindestens ein Cookie in Ihrem Browser gesetzt. In diesem Fall sendet Ihr Browser über dieses Cookie Informationen an Facebook, sobald Sie unsere Seite besuchen bzw. mit sozialen Plug-ins (z.B. dem „Gefällt mir“-Button) interagieren.

Die erhaltenen Informationen werden innerhalb von 90 Tagen wieder gelöscht bzw. anonymisiert. Laut Facebook gehören zu diesen Daten Ihre IP-Adresse, welche Webseite Sie besucht haben, das Datum, die Uhrzeit und weitere Informationen, die Ihren Browser betreffen.

Um zu verhindern, dass Facebook während Ihres Besuches auf unserer Webseite viele Daten sammelt und mit den Facebook-Daten verbindet, müssen Sie sich während des Webseitenbesuchs von Facebook abmelden (ausloggen).

Falls Sie bei Facebook nicht angemeldet sind oder kein Facebook-Konto besitzen, sendet Ihr Browser weniger Informationen an Facebook, weil Sie weniger Facebook-Cookies haben. Dennoch können Daten wie beispielsweise Ihre IP-Adresse oder welche Webseite Sie besuchen an Facebook übertragen werden. Wir möchten noch ausdrücklich darauf hinweisen, dass wir über die genauen Inhalte der Daten nicht exakt Bescheid wissen. Wir versuchen aber Sie nach unserem aktuellen Kenntnisstand so gut als möglich über die Datenverarbeitung aufzuklären. Wie Facebook die Daten nutzt, können Sie auch in den Datenrichtline des Unternehmens unter [https://www.facebook.com/about/privacy/update](https://www.facebook.com/about/privacy/update) nachlesen.

Folgende Cookies werden in Ihrem Browser mindestens gesetzt, wenn Sie eine Webseite mit sozialen Plug-ins von Facebook besuchen:

**Name:** dpr  
**Wert:** keine Angabe  
**Verwendungszweck:** Dieses Cookie wird verwendet, damit die sozialen Plug-ins auf unserer Webseite funktionieren.  
**Ablaufdatum:** nach Sitzungsende

**Name:** fr  
**Wert:** 0jieyh4221148768c2GnlufEJ9..Bde09j…1.0.Bde09j  
**Verwendungszweck:** Auch das Cookie ist nötig, dass die Plug-ins einwandfrei funktionieren.  
**Ablaufdatum::** nach 3 Monaten

**Anmerkung:** Diese Cookies wurden nach einem Test gesetzt, auch wenn Sie nicht Facebook-Mitglied sind.

Sofern Sie bei Facebook angemeldet sind, können Sie Ihre Einstellungen für Werbeanzeigen unter [https://www.facebook.com/ads/preferences/?entry\_product=ad\_settings\_screen](https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen) selbst verändern. Falls Sie kein Facebook-User sind, können Sie auf [http://www.youronlinechoices.com/de/praferenzmanagement/](http://www.youronlinechoices.com/de/praferenzmanagement/?tid=221148768)grundsätzlich Ihre nutzungsbasierte Online-Werbung verwalten. Dort haben Sie die Möglichkeit, Anbieter zu deaktivieren bzw. zu aktivieren.

Wenn Sie mehr über den Datenschutz von Facebook erfahren wollen, empfehlen wir Ihnen die eigenen Datenrichtlinien des Unternehmens auf [https://www.facebook.com/policy.php](https://www.facebook.com/policy.php?tip=221148768).

Facebook Login Datenschutzerklärung
-----------------------------------

Wir haben auf unserer Seite das praktische Facebook Login integriert. So können Sie sich bei uns ganz einfach mit Ihrem Facebook-Konto einloggen, ohne ein weiteres Benutzerkonto anlegen zu müssen. Wenn Sie sich entscheiden, Ihre Registrierung über das Facebook Login zu machen, werden Sie auf das Social Media Network Facebook weitergeleitet. Dort erfolgt die Anmeldung über Ihre Facebook Nutzerdaten. Durch dieses Login-Verfahren werden Daten über Sie bzw. Ihr Userverhalten gespeichert und an Facebook übermittelt.

Um die Daten zu speichern, benutzt Facebook verschiedene Cookies. Im Folgenden zeigen wir Ihnen die wichtigsten Cookies, die in Ihrem Browser gesetzt werden bzw. schon bestehen, wenn Sie sich über das Facebook Login auf unserer Seite anmelden:

**Name:** fr  
**Wert:** 0jieyh4c2GnlufEJ9..Bde09j…1.0.Bde09j  
**Verwendungszweck:** Dieses Cookie wird verwendet, damit das soziale Plugin auf unserer Webseite bestmöglich funktioniert.  
**Ablaufdatum:** nach 3 Monaten

**Name:** datr  
**Wert:** 4Jh7XUA2221148768SEmPsSfzCOO4JFFl  
**Verwendungszweck:** Facebook setzt das “datr”-Cookie, wenn ein Webbrowser auf facebook.com zugreift, und das Cookie hilft, Anmeldeaktivitäten zu identifizieren und die Benutzer zu schützen.  
**Ablaufdatum:** nach 2 Jahren

**Name:** \_js\_datr  
**Wert:** deleted  
**Verwendungszweck:** Dieses Session-Cookie setzt Facebook zu Trackingzwecken, auch wenn Sie kein Facebook-Konto haben oder ausgeloggt sind.  
**Ablaufdatum:** nach Sitzungsende

**Anmerkung:** Die angeführten Cookies sind nur eine kleine Auswahl der Cookies, die Facebook zur Verfügung stehen. Weitere Cookies sind beispielsweise \_ fbp, sb oder wd. Eine vollständige Aufzählung ist nicht möglich, da Facebook über eine Vielzahl an Cookies verfügt und diese variabel einsetzt.

Der Facebook Login bietet Ihnen einerseits einen schnellen und einfachen Registrierungsprozess, andererseits haben wir so die Möglichkeit Daten mit Facebook zu teilen. Dadurch können wir unser Angebot und unsere Werbeaktionen besser an Ihre Interessen und Bedürfnisse anpassen. Daten, die wir auf diese Weise von Facebook erhalten, sind öffentliche Daten wie

*   Ihr Facebook-Name
*   Ihr Profilbild
*   eine hinterlegte E-Mail-Adresse
*   Freundeslisten
*   Buttons-Angaben (z.B. „Gefällt mir“-Button)
*   Geburtstagsdatum
*   Sprache
*   Wohnort

Im Gegenzug stellen wir Facebook Informationen über Ihre Aktivitäten auf unserer Webseite bereit. Das sind unter anderem Informationen über Ihr verwendetes Endgerät, welche Unterseiten Sie bei uns besuchen oder welche Produkte Sie bei uns erworben haben.

Durch die Verwendung von Facebook Login willigen Sie der Datenverarbeitung ein. Sie können diese Vereinbarung jederzeit widerrufen. Wenn Sie mehr Informationen über die Datenverarbeitung durch Facebook erfahren wollen, empfehlen wir Ihnen die Facebook-Datenschutzerklärung unter [https://de-de.facebook.com/policy.php](https://de-de.facebook.com/policy.php?tid=221148768).

Sofern Sie bei Facebook angemeldet sind, können Sie Ihre Einstellungen für Werbeanzeigen unter [https://www.facebook.com/ads/preferences/?entry\_product=ad\_settings\_screen](https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen) selbst verändern.

YouTube Abonnieren Button Datenschutzerklärung
----------------------------------------------

Wir haben auf unserer Webseite den YouTube Abonnieren Button (engl. „Subscribe-Button“) eingebaut. Sie erkennen den Button meist am klassischen YouTube-Logo. Das Logo zeigt vor rotem Hintergrund in weißer Schrift die Wörter „Abonnieren“ oder „YouTube“ und links davon das weiße „Play-Symbol“. Der Button kann aber auch in einem anderen Design dargestellt sein.

Unser YouTube-Kanal bietet Ihnen immer wieder lustige, interessante oder spannende Videos. Mit dem eingebauten „Abonnieren-Button“ können Sie unseren Kanal direkt von unserer Webseite aus abonnieren und müssen nicht eigens die YouTube-Webseite aufrufen. Wir wollen Ihnen somit den Zugang zu unserem umfassenden Content so einfach wie möglich machen. Bitte beachten Sie, dass YouTube dadurch Daten von Ihnen speichern und verarbeiten kann.

Wenn Sie auf unserer Seite einen eingebauten Abo-Button sehen, setzt YouTube – laut Google – mindestens ein Cookie. Dieses Cookie speichert Ihre IP-Adresse und unsere URL. Auch Informationen über Ihren Browser, Ihren ungefähren Standort und Ihre voreingestellte Sprache kann YouTube so erfahren. Bei unserem Test wurden folgende vier Cookies gesetzt, ohne bei YouTube angemeldet zu sein:

**Name:** YSC  
**Wert:** b9-CV6ojI5221148768Y  
**Verwendungszweck:** Dieses Cookie registriert eine eindeutige ID, um Statistiken des gesehenen Videos zu speichern.  
**Ablaufdatum:** nach Sitzungsende

**Name:** PREF  
**Wert:** f1=50000000  
**Verwendungszweck:** Dieses Cookie registriert ebenfalls Ihre eindeutige ID. Google bekommt über PREF Statistiken, wie Sie YouTube-Videos auf unserer Webseite verwenden.  
**Ablaufdatum:** nach 8 Monate

**Name:** GPS  
**Wert:** 1  
**Verwendungszweck:** Dieses Cookie registriert Ihre eindeutige ID auf mobilen Geräten, um den GPS-Standort zu tracken.  
**Ablaufdatum:** nach 30 Minuten

**Name:** VISITOR\_INFO1\_LIVE  
**Wert:** 22114876895Chz8bagyU  
**Verwendungszweck:** Dieses Cookie versucht die Bandbreite des Users auf unseren Webseiten (mit eingebautem YouTube-Video) zu schätzen.  
**Ablaufdatum:** nach 8 Monaten

**Anmerkung:** Diese Cookies wurden nach einem Test gesetzt und können nicht den Anspruch auf Vollständigkeit erheben.

Wenn Sie in Ihrem YouTube-Konto angemeldet sind, kann YouTube viele Ihrer Handlungen/Interaktionen auf unserer Webseite mit Hilfe von Cookies speichern und Ihrem YouTube-Konto zuordnen. YouTube bekommt dadurch zum Beispiel Informationen wie lange Sie auf unserer Seite surfen, welchen Browsertyp Sie verwenden, welche Bildschirmauflösung Sie bevorzugen oder welche Handlungen Sie ausführen.

YouTube verwendet diese Daten zum einen um die eigenen Dienstleistungen und Angebote zu verbessern, zum anderen um Analysen und Statistiken für Werbetreibende (die Google Ads verwenden) bereitzustellen.

Quelle: Erstellt mit dem [Datenschutz Generator](https://www.firmenwebseiten.at/datenschutz-generator/ "Datenschutz Generator von firmenwebseiten.at") von firmenwebseiten.at
